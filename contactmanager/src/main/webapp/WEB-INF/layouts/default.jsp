<%@page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<!DOCTYPE html>
<html lang="es">
    <head>
        <c:set var="url">${pageContext.request.requestURL}</c:set>
        <base href="${fn:substring(url, 0, fn:length(url) - fn:length(pageContext.request.requestURI))}${pageContext.request.contextPath}/" />

        <title><tiles:getAsString name="title"/></title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="keywords" content="contact manager, customer management, customer manager, shared address book, contact organizer, shared contact manager, contact management, business contacts, crm small business, address book online, business contact management" />
        <meta name="description" content="The simple and easy Customer Relationship Manager to create effective business relationships." />
        <link rel="shortcut icon" type="image/x-icon" href="https://www.mucontacts.com/images/favicon.ico"/>

        <link type="text/css" href="https://www.mucontacts.com/webfonts/Quicksand/stylesheet.css?jodd-unstaple" rel="stylesheet" />
        <link type="text/css" href="https://www.mucontacts.com/webfonts/Colaborate/stylesheet.css?jodd-unstaple" rel="stylesheet" />
        <link type="text/css" href="https://www.mucontacts.com/webfonts/OpenSans/stylesheet.css?jodd-unstaple" rel="stylesheet" />

        <link type="text/css" href="<c:url value="/css/addressbooks.css"/>" rel="stylesheet" />
        <link type="text/css" href="<c:url value="/css/banner.css"/>" rel="stylesheet" />
        <link type="text/css" href="<c:url value="/css/common.css"/>" rel="stylesheet" />
        <link type="text/css" href="<c:url value="/css/forms.css"/>" rel="stylesheet" />
        <link type="text/css" href="<c:url value="/css/header-footer.css"/>" rel="stylesheet" />
        <link type="text/css" href="<c:url value="/css/main.css"/>" rel="stylesheet" />
        <link type="text/css" href="<c:url value="/css/plans.css"/>" rel="stylesheet" />
        <link type="text/css" href="<c:url value="/css/referrals.css"/>" rel="stylesheet" />
        <link type="text/css" href="<c:url value="/css/signup.css"/>" rel="stylesheet" />
        <link type="text/css" href="<c:url value="/css/tooltips.css"/>" rel="stylesheet" />
        <link type="text/css" href="<c:url value="/js/lib/jquery-clearable-textfield/jquery.clearableTextField.css"/>" rel="stylesheet" />
        <link type="text/css" href="<c:url value="/js/lib/jquery-fancybox/jquery.fancybox-1.3.4.css"/>" rel="stylesheet" />
        <link type="text/css" href="<c:url value="/js/lib/jquery-freeow/freeow.css"/>" rel="stylesheet" />
        <link type="text/css" href="<c:url value="/js/lib/jquery-loading/jquery.loading.1.6.css"/>" rel="stylesheet" />
        <link type="text/css" href="<c:url value="/js/lib/jquery-tablesorter/css/style.css"/>" rel="stylesheet" />
        <link type="text/css" href="<c:url value="/js/lib/jquery-taghandler/jquery.taghandler.css"/>" rel="stylesheet" />
        <link type="text/css" href="<c:url value="/js/lib/jquery-ui/jquery-ui-1.8.23.custom.css"/>" rel="stylesheet" />

        <script src="<c:url value="/js/lib/jquery-1.11.1.min.js"/>"></script>
        <script src="<c:url value="/js/lib/jquery-migrate-1.2.1.min.js"/>"></script> <!-- needed by jquery-ba-bbq, it uses deprecated $.browser -->
        <script src="<c:url value="/js/lib/jquery-ba-bbq/jquery.ba-bbq.min.js"/>"></script>
        <script src="<c:url value="/js/lib/jquery-clearable-textfield/jquery.clearableTextField.min.js"/>"></script>
        <script src="<c:url value="/js/lib/jquery-cookie/jquery.cookie.js"/>"></script>
        <script src="<c:url value="/js/lib/jquery-cutetime/jquery.cuteTime.min.js"/>"></script>
        <script src="<c:url value="/js/lib/jquery-editable/jquery.jeditable.min.js"/>"></script>
        <script src="<c:url value="/js/lib/jquery-fancybox/jquery.easing-1.3.pack.js"/>"></script>
        <script src="<c:url value="/js/lib/jquery-fancybox/jquery.fancybox-1.3.4.pack.js"/>"></script>
        <script src="<c:url value="/js/lib/jquery-form/jquery.form.js"/>"></script>
        <script src="<c:url value="/js/lib/jquery-freeow/jquery.freeow.min.js"/>"></script>
        <script src="<c:url value="/js/lib/jquery-iautils/jquery.iautils.js"/>"></script>
        <script src="<c:url value="/js/lib/jquery-loading/jquery.loading.1.6.4.min.js"/>"></script>
        <script src="<c:url value="/js/lib/jquery-tablesorter/jquery.tablesorter.min.js"/>"></script>
        <script src="<c:url value="/js/lib/jquery-taghandler/jquery.taghandler.min.js"/>"></script>
        <script src="<c:url value="/js/lib/jquery-template/jquery.tmpl.min.js"/>"></script>
        <script src="<c:url value="/js/lib/jquery-ui/jquery-ui-1.8.23.custom.min.js"/>"></script>
        <script src="<c:url value="/js/lib/jquery-validator/jquery.validator.min.js"/>"></script>
        <script src="<c:url value="/js/lib/mousetrap/mousetrap.min.js"/>"></script>

        <script src="<c:url value="/js/app/mucontacts.js"/>"></script>
        <script src="<c:url value="/js/app/browser/browser.js"/>"></script>
        <script src="<c:url value="/js/app/form/form.js"/>"></script>
        <script src="<c:url value="/js/app/message/message.js"/>"></script>
        <script src="<c:url value="/js/app/search/search.js"/>"></script>
        <script src="<c:url value="/js/app/search/contacts/contacts.js"/>"></script>
        <script src="<c:url value="/js/app/service/service.js"/>"></script>
        <script src="<c:url value="/js/app/service/addressbook/addressbook.js"/>"></script>
        <script src="<c:url value="/js/app/service/addressbookinvitation/addressbookinvitation.js"/>"></script>
        <script src="<c:url value="/js/app/service/addressbookpermission/addressbookpermission.js"/>"></script>
        <script src="<c:url value="/js/app/service/comment/comment.js"/>"></script>
        <script src="<c:url value="/js/app/service/contact/contact.js"/>"></script>
        <script src="<c:url value="/js/app/service/tag/tag.js"/>"></script>
        <script src="<c:url value="/js/app/service/user/user.js"/>"></script>

        <tiles:importAttribute name="activeMenu" scope="page"/>

        <script type="text/javascript">
            $(document).ready(function() {
                //highlight active menu
                var activeMenu = "item_menu_" + "${activeMenu}";
                $("a." + activeMenu).addClass("activo");

                mucontacts.browser.displayUnsupportedBrowserMessage();
                mucontacts.message.displayDelayedMessage();

                /** Display spinner when executing ajax calls. */
                $.loading({
                    onAjax: true,
                    text: 'Working...',
                    delay: 200
                });

            });
        </script>

        <tiles:importAttribute name="showAnalytics" scope="page"/>
        <c:if test="${showAnalytics == 'true'}">
            <%@include file="/WEB-INF/jsp/snippets/analytics.jsp" %>
        </c:if>
    </head>
    <body>

        <div class="header">
            <div class="info anchoPagina">
                <div class="logo">
                    <a href="/mucontacts"><img alt="muContacts" src="<c:url value="/images/logo.png"/>" /></a>
                </div>
                <div class="menu">
                    <div class="welcome">
                        &nbsp;
                        <sec:authorize access="isAuthenticated()">
                            Welcome, ${SPRING_SECURITY_CONTEXT.authentication.name}
                        </sec:authorize>
                    </div>

                    <tiles:insertAttribute name="menu"/>
                </div>
            </div>
        </div>

        <div class="content">
            <tiles:insertAttribute name="body"/>
        </div>


        <div class="footer">
            <div class="info anchoPagina">
                <div class="logo"><img alt="muContacts" src="<c:url value="/images/logo-footer.png"/>" /></div>
                <div class="menu">
                    <tiles:insertAttribute name="menu"/>
                </div>
            </div>
        </div>

        <!-- jquery-freeow message container -->
        <div id="freeow" class="freeow freeow-top-right"></div>

        <tiles:importAttribute name="showFeedback" scope="page"/>
        <c:if test="${showFeedback == 'true'}">
            <%@include file="/WEB-INF/jsp/snippets/feedback.jsp" %>
        </c:if>
    </body>
</html>

