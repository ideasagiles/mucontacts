<%@ taglib prefix='c' uri='http://java.sun.com/jstl/core_rt' %>

<div class="banner">
    <div class="contenedor anchoPagina">
        <div class="info">
            <div class="line1"><strong>muContacts</strong> Referrals Center</div>
            <div class="line2">Invite your friends and enjoy awesome benefits!</div>
        </div>
    </div>
</div>

<div class="referrals anchoPagina">
    <div class="plan">
        <div class="info" id="info_level_4">
            <h1>Always-Free Plan Platinum</h1>
            7500 contacts / 2 addressbooks / 2 collaborators
        </div>
        <div class="counter" id="counter_level_4">20 referrals</div>
    </div>

    <div class="plan">
        <div class="info" id="info_level_3">
            <h1>Always-Free Plan Gold</h1>
            5000 contacts / 1 addressbook / 1 collaborator
        </div>
        <div class="counter" id="counter_level_3">10 referrals</div>
    </div>

    <div class="plan">
        <div class="info"  id="info_level_2">
            <h1>Always-Free Plan Silver</h1>
            2000 contacts / 1 addressbook / 1 collaborator
        </div>
        <div class="counter" id="counter_level_2">5 referrals</div>
    </div>

    <div class="plan">
        <div class="info" id="info_level_1">
            <h1>Always-Free Plan Bronce</h1>
            1000 contacts / 1 addressbook
        </div>
        <div class="counter" id="counter_level_1">1 referral</div>
    </div>

    <div class="plan">
        <div class="info" id="info_level_0">
            <h1>Always-Free Plan Basic</h1>
            500 contacts / 1 addressbook
        </div>
    </div>
</div>

<div class="part4-plans">
    <div class="contenedor anchoPagina">

        <div class="items"><img src="images/plans-faqs.png" alt="Faqs" height="66" width="960" /></div>

        <div class="faqstitle"><strong>About our referral program.</strong> Enjoy great benefits.</div>

        <div class="info">

            <div class="col1">
                <div class="titulo"><strong>&#149;</strong> How do I refer friends?</div>
                <div class="text">Send your unique referral link to your friends. When they sign up using your unique link, you earn a referral. It's that simple!</div>

                <div class="titulo"><strong>&#149;</strong> Where is my unique referral link?</div>
                <div class="text">You can find your unique referral link in your <a href="<c:url value="/user/account.html"/>">Account page</a>.</div>
            </div>
            <div class="col2">
                <div class="titulo"><strong>&#149;</strong> When is my referrals counter updated?</div>
                <div class="text">The referral counter is updated when your referral signs up using your unique referral code.</div>

                <div class="titulo"><strong>&#149;</strong> I want to contact you, do you have an email?</div>
                <div class="text">We'd love to hear from you! Just email us at <a href="mailto:support@mucontacts.com">support@mucontacts.com</a></div>
            </div>

        </div>

    </div>
</div>

<script src="<c:url value="/js/ui/plan/referrals.js"/>"></script>