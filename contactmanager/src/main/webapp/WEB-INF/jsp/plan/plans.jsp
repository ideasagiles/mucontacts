<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix='c' uri='http://java.sun.com/jstl/core_rt' %>

<fmt:setBundle basename="application"/>

<div class="banner"><!--start of BANNER-->
    <div class="contenedor anchoPagina">

        <div class="info">
            <div class="line1">Join <strong>muContacts</strong> today with a PRO account.</div>
            <div class="line2">You will enjoy all the features of your free account plus extra benefits!</div>
        </div>

    </div>
</div><!--end of BANNER-->

<div class="part1-plans"><!--start of PART 1 (needed for shadow of banner) -->
    &nbsp;
</div><!--end of PART 1-->

<div class="part2-plans"><!--start of PART 2-->
    <div class="contenedor anchoPagina">

        <div class="plans">

            <!--start of PLAN BEST DEAL-->
            <div class="col1">
                <div class="image"><img src="images/plans-bestdeal.png" alt="Best Deal" height="49" width="217" /></div>
                <div class="info">for small business with<br>growing relationships</div>
                <div class="separador"><img src="images/plans-separador2.png" alt="Separador" height="3" width="240" /></div>
                <div class="items">
                    <ul class="items">
                        <li><strong>&#149; 10</strong> addressbook</li>
                        <li><strong>&#149; 5000</strong> business contacts</li>
                        <li><strong>&#149; unlimited</strong> viewers</li>
                        <li><strong>&#149; 5</strong> collaborators</li>
                    </ul>
                </div>
                <div class="separador"><img src="images/plans-separador2.png" alt="Separador" height="3" width="240" /></div>
                <div class="price">only <strong>$19</strong> / month</div>
                <div class="button">
                    <!--a href="#"><img src="images/plans-button.png" alt="Choose Plan" height="41" width="158" /></a-->
                    <form action='<fmt:message key="paypal.post.url"/>' method="post">
                        <input type="hidden" name="cmd" value="_s-xclick">
                        <input type="hidden" name="hosted_button_id" value='<fmt:message key="paypal.button_id.pro_plan"/>'>
                        <input type="hidden" name="notify_url" value='<fmt:message key="paypal.notify.url"/>'/>
                        <input type="hidden" name="custom" value="username=<%= request.getUserPrincipal().getName()%>">
                        <%--    More info: https://merchant.paypal.com/us/cgi-bin/?cmd=_render-content&content_ID=developer/e_howto_html_Appx_websitestandard_htmlvariables --%>

                        <input type="hidden" name="return" value='<fmt:message key="paypal.return.url"/>' />
                        <input type="hidden" name="cancel_return" value='<fmt:message key="paypal.cancel.url"/>' />
                        <!-- input class="button-special" type="submit" value="Subscribe" / -->
                        <input type="image" class="button-special" src="images/plans-button.png" alt="Choose Plan" height="41" width="158" />
                    </form>
                </div>
            </div>
            <!--end of PLAN BEST DEAL-->

        </div>

    </div>
</div><!--end of PART 2-->

<div class="part3-plans dashed-separator"><!--start of PART 3-->

    <div class="contenedor anchoPagina">

        <div class="info">
            <div class="line1">Your data is safe and secure.</div>
            <div class="line2">Our servers are monitored 24x7, kept up-to-date with the latest security releases.</div>
        </div>

    </div>

</div><!--end of PART 3-->

<div class="part4-plans"><!--start of PART 4-->
    <div class="contenedor anchoPagina">

        <div class="items"><img src="images/plans-faqs.png" alt="Faqs" height="66" width="960" /></div>

        <div class="faqstitle"><strong>ANY QUESTIONS?</strong> We are always glad to help you.</div>

        <div class="info">

            <div class="col1">
                <div class="titulo"><strong>&#149;</strong> If you choose the FREE plan, can I upgrade later?</div>
                <div class="text">YES, You can upgrade anytime you want.</div>

                <div class="titulo"><strong>&#149;</strong> Is my data secure?</div>
                <div class="text">YES. All the content is transmited using strong encryption standards. Your contacts are always safe!</div>
            </div>
            <div class="col2">
                <div class="titulo"><strong>&#149;</strong> Can I pay using PAYPAL?</div>
                <div class="text">YES. We accept Paypal as payment method.</div>

                <div class="titulo"><strong>&#149;</strong> I want to contact you, do you have an email?</div>
                <div class="text">We'd love to hear from you! Just email us at <a href="mailto:support@mucontacts.com">support@mucontacts.com</a></div>
            </div>

        </div>

    </div>
</div><!--end of PART 4-->
