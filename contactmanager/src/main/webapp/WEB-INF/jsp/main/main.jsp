<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>

<div class="panel-wrapper">
    <div id="panel-window">
        <%@include file="/WEB-INF/jsp/main/welcome.jsp" %>
        <%@include file="/WEB-INF/jsp/main/browse.jsp" %>
        <%@include file="/WEB-INF/jsp/main/details.jsp" %>
        <%@include file="/WEB-INF/jsp/main/edit.jsp" %>
        <%@include file="/WEB-INF/jsp/main/keyboardShortcuts.jsp" %>
    </div>
</div>

<script src="<c:url value="/js/ui/main/main.js"/>"></script>
<script src="<c:url value="/js/ui/main/welcome.js"/>"></script>
<script src="<c:url value="/js/ui/main/browse.js"/>"></script>
<script src="<c:url value="/js/ui/main/details.js"/>"></script>
<script src="<c:url value="/js/ui/main/edit.js"/>"></script>
<script src="<c:url value="/js/ui/main/keyboardShortcuts.js"/>"></script>
