<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
<%-- Disabling EL in JSP for using Jquery Template. Alternative methods:
     http://www.ke-cai.net/2010/12/jquery-template-markup-and-jsp.html
--%>
<%@ page isELIgnored="true" %>

<div id="browse_panel" style="display: none" class="panel">

    <div class="banner">
        <div class="contenedor anchoPagina">
            <div class="left">
                <form id="browse_searchForm" class="search">
                    <input type="text" id="browse_q" placeholder="Search contacts and tags" value="" class="mousetrap"/>
                </form>
            </div>

            <div class="right">
                <div class="button">
                    <a id="browse_newContact" href="#" tabindex="2">
                        <img src="images/btn-newcontact.png" alt="New Contact" height="54" width="223" />
                    </a>
                </div>
            </div>
        </div>
    </div>

    <div class="contactsList anchoPagina">
        <div class="toolbar">
            <div class="addressBook">
                <h1>Filter by Address Book</h1>
                <ul id="browse_addressBooks"></ul>
            </div>

            <div class="tools">
                <h1>Tools</h1>
                <ul>
                    <a id="browse_emailList" href="#"><li>emails list from visible</li></a>
                    <a id="browse_exportToCSV" href="#"><li>export all contacts to a CSV file</li></a>
                    <a id="browse_importFromCSV" href="#"><li>import contacts from CSV file</li></a>
                    <a id="browse_showKeyboardShortcuts" href="#"><li>Show keyboard shortcuts</li></a>
                </ul>
            </div>


            <div class="tags">
                <h1>Filter by Tags</h1>
                <ul id="browse_tagListItems"></ul>
            </div>
        </div>

        <div id="browse_contactList" class="contacts"></div>
    </div>
</div>

<div id="browse_emailListInfo" style="display:none">
    <div class="email-list">
        <h1>Just copy-and-paste your emails</h1>
        <div id="browse_emailListContainer" class="emails"></div>
    </div>
</div>

<div id="browse_importFromCSVInfo" style="display:none">
    <div>
        <h1>Import contacts into your Adress Book</h1>
        <form class="browse_importFromCsvForm" method="post" action="<c:url value="/services/contact/import/csv"/>" enctype="multipart/form-data">
            <div class="row">
                <label>Import from: </label>
                <input type="radio" name="from" value="Outlook" checked="checked"/>Outlook (or Google exported as Outlook
                                                       <small><a href="http://support.google.com/mail/bin/answer.py?hl=en&answer=24911" target="__BLANK">-how?-</a></small>)
            </div>
            <div class="row">
                <label>CSV File: </label>
                <input class="browse_importFromCsvFile" name="file" type="file" required="required" /><br/>
            </div>
            <div class="row">
                <label>Import into: </label>
                <select id="browse_importFromCSVContainer" name="addressBookId"></select>
            </div>
            <div class="actions">
                <input type="submit" class="buttonAction" value="Upload CSV" />
                <br/><br/>
            </div>
        </form>
    </div>
</div>


<script id="browse_emailListTemplate" type="text/x-jquery-tmpl">
    {{if email}} ${email} <br/> {{/if}}
</script>

<script id="browse_importFromCSVTemplate" type="text/x-jquery-tmpl">
    <option value="${addressBook.id}">${addressBook.name}</option>
</script>

<script id="browse_addressBookTemplate" type="text/x-jquery-tmpl">
    <a href="#" class="browse_addressBookListItem ${$item.getStyleClass()}" data-id="${addressBook.id}"><li>${addressBook.name}</li></a>
</script>

<script id="browse_contactTemplate" type="text/x-jquery-tmpl">
    <a href="#" data-id="${id}" class="browse_contactListItem">
        <div class="browse_divContactListItem">
            <div class="itemIcon"><img src="${$item.getEmotionIconUrl()}" alt="icon" /></div>
            <div class="datos">
                <div class="nombre">${name}</div>
                <div class="profesion">${shortDescription}</div>
                <div class="email">${email}</div>
            </div>
            <div class="detailsIcon"><img src="<c:url value="/images/ico-flecha.png"/>" alt="follow"/></div>
        </div>
    </a>
</script>

<script id="browse_contactTemplateWelcome" type="text/x-jquery-tmpl">
    <div class="part3-plans">
        <div class="contenedor anchoPagina">
            <div class="info welcome">
                <div class="line1">Welcome to muContacts,<br/>the best tool to manage and grow<br/>your business relationships.</div>
                <div class="line2">Try <a href="#" id="browse_createFirstContact">creating your first contact</a> to start.</div>
            </div>
        </div>
    </div>
</script>

<script id="browse_tagList" type="text/x-jquery-tmpl">
    <a href="#" data-name="${name}" class="browse_tagItem"><li>${name}</li></a>
</script>
