<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
<%-- Disabling EL in JSP for using Jquery Template. Alternative methods:
     http://www.ke-cai.net/2010/12/jquery-template-markup-and-jsp.html
--%>
<%@ page isELIgnored="true" %>
<div id="keyboardShortcuts" style="display:none;">
    <h1>Keyboard Shortcuts</h1>
    <p>Press ? to display this refcard.</p>
    <table cellpadding="0">
        <tbody>
            <tr>
                <th></th>
                <th class="title">Browse</th>
            </tr>
            <tr>
                <td class="keys"><span>&lt;alt&gt;</span>
                                 <span class="separator">+</span>
                                 <span>n</span> or
                                 <span>&lt;command&gt;</span>
                                 <span class="separator">+</span>
                                 <span>n</span> :</td>
                <td class="description">Create new contact</td>
            </tr>
            <tr>
                <td class="keys"><span>&lt;alt&gt;</span>
                                 <span class="separator">+</span>
                                 <span>f</span> or
                                 <span>&lt;command&gt;</span>
                                 <span class="separator">+</span>
                                 <span>f</span> :</td>
                <td class="description">Find an existing contact</td>
            </tr>
            <tr>
                <td class="keys"><span>up</span> :</td>
                <td class="description">Select previous contact</td>
            </tr>
            <tr>
                <td class="keys"><span>down</span> :</td>
                <td class="description">Select next contact</td>
            </tr>
            <tr>
                <td class="keys"><span>Enter</span> :</td>
                <td class="description">Go To the description of the selected contact</td>
            </tr>
            <tr>
                <th></th>
                <th class="title">Edit</th>
            </tr>
            <tr>
                <td class="keys"><span>&lt;control&gt;</span>
                                 <span class="separator">+</span>
                                 <span>enter</span> or
                                 <span>&lt;command&gt;</span>
                                 <span class="separator">+</span>
                                 <span>enter</span> :</td>
                <td class="description">Save changes</td>
            </tr>
            <tr>
                <td class="keys"><span>&lt;alt&gt;</span>+<span>b</span> or
                                <span>&lt;command&gt;</span>
                                 <span class="separator">+</span>
                                 <span>b</span> :</td>
                <td class="description">Go to Browse</td>
            </tr>
            <tr>
                <th></th>
                <th class="title">Details</th>
            </tr>
            <tr>
                <td class="keys"><span>&lt;control&gt;</span>
                                 <span class="separator">+</span>
                                 <span>enter</span> or
                                 <span>&lt;command&gt;</span>
                                 <span class="separator">+</span>
                                 <span>enter</span> :</td>
                <td class="description">Submit comment</td>
            </tr>
            <tr>
                <td class="keys"><span>&lt;alt&gt;</span>
                                 <span class="separator">+</span>
                                 <span>n</span> or
                                 <span>&lt;command&gt;</span>
                                 <span class="separator">+</span>
                                 <span>n</span> :</td>
                <td class="description">Create new contact</td>
            </tr>
            <tr>
                <td class="keys"><span>&lt;alt&gt;</span>
                                 <span class="separator">+</span>
                                 <span>b</span> or
                                 <span>&lt;command&gt;</span>
                                 <span class="separator">+</span>
                                 <span>b</span> :</td>
                <td class="description">Go to Browse</td>
            </tr>
            <tr>
                <td class="keys"><span>&lt;alt&gt;</span>
                                 <span class="separator">+</span>
                                 <span>u</span> or
                                 <span>&lt;command&gt;</span>
                                 <span class="separator">+</span>
                                 <span>u</span>:</td>
                <td class="description">Update contact</td>
            </tr>
        </tbody>
    </table>
</div>
