<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
<%-- Disabling EL in JSP for using Jquery Template. Alternative methods:
     http://www.ke-cai.net/2010/12/jquery-template-markup-and-jsp.html
--%>
<%@ page isELIgnored="true" %>

<div id="details_panel" style="display: none" class="panel">
    <div class="banner">
        <div class="contenedor anchoPagina">

            <div class="left">
                <a id="details_browseButton" href="#">
                    <img src="images/btn-browsecontacts.png" alt="Browse Contacts" height="54" width="270" />
                </a>
            </div>

            <div class="right">
                <a id="details_newButton" href="#">
                    <img src="images/btn-newcontact.png" alt="New Contact" height="54" width="223" />
                </a>
            </div>
        </div>
    </div>


    <div id="details_contactDetails" class="contactInfo anchoPagina"></div>


    <div class="comments">
        <div id="details_commentDetails" class="contenedor anchoPagina"></div>
    </div>

    <div id="details_commentFormDetails" class="commentInput">
        <div class="contenedor anchoPagina">

            <div class="area">
                <form id="details_commentForm">
                    <input type="hidden" name="contactId" value=""/>
                    <input id="emotion" type="hidden" name="emotion" value=""/>
                    <div class="smiles">
                        <ul>
                            <li><img class="details_emotion" data-id="HAPPY" src="images/smilies/smile-happy-small.png" alt="Happy" /></li>
                            <li><img class="details_emotion" data-id="SAD" src="images/smilies/smile-sad-small.png" alt="Sad" /></li>
                            <li><img class="details_emotion" data-id="MAD" src="images/smilies/smile-mad-small.png" alt="Mad" /></li>
                            <li><img class="details_emotion" data-id="SCARED" src="images/smilies/smile-scared-small.png" alt="Scared" /></li>
                        </ul>
                    </div>
                    <div class="inputText">
                        <textarea id="details_commentText" name="text" rows="2" cols="20" maxlength="1000" required="required" class="mousetrap"></textarea>
                    </div>

                    <div class="button"><input type="image" src="images/btn-addcomment.png" alt="Add comment" height="54" width="183" /></div>
                </form>
            </div>
        </div>
    </div>
</div>



<script id="details_contactDetailsTemplate" type="text/x-jquery-tmpl">
    <div class="clearfix">
        <div class="left"><!--start of  LEFT-->
            <div class="itemIcon"><img src="<c:url value="/images/ico-persona.png"/>" alt="icon"/></div>
            <div class="details">
                <div class="nombre">${name}</div>
                {{if shortDescription}}
                <div class="profesion">
                    <p>${shortDescription}</p>
                </div>
                {{/if}}
                <div class="datos">
                    <ul>
                        {{if email}}<li><span class="black">Email:</span> ${email}</li>{{/if}}
                        {{if phone}}<li><span class="black">Phone:</span> ${phone}</li>{{/if}}
                        {{if organization}}<li><span class="black">Organization:</span> ${organization}</li>{{/if}}
                        {{if address}}<li><span class="black">Address:</span> ${address}</li>{{/if}}
                        {{if website}}<li><span class="black">Web:</span> <a href="${website}">${website}</a></li>{{/if}}
                    </ul>
                </div>
                {{if description}}
                <div class="comentarios">${description}</div>
                {{/if}}
            </div>
        </div><!--end of LEFT-->

        <div class="right"><!--start of RIGHT-->
            <div class="tags">
                <h1>Yours Tags</h1>
                <ul class="list">
                    {{each tags}}<li>${$value.name}</li>{{/each}}
                </ul>
            </div>

            <div class="updateContact">
                <a id="details_updateButton" href="#">
                    <img src="images/btn-updatecontact.png" alt="Update Contact" height="54" width="248" />
                </a>
            </div>

            <div class="deleteContact">
                <a id="details_deleteButton" href="#">
                    <img src="images/btn-deletecontact.png" alt="Delete Contact" height="37" width="171" />
                </a>
            </div>
        </div><!--end of RIGHT-->
    </div>
</script>

<script id="details_commentDetailsTemplate" type="text/x-jquery-tmpl">
    <div class="commentInfo">
        <div class="left">
            <img src="${$item.getIconUrl()}" alt="${emotion}" height="77" width="78" />
        </div>

        <div class="right">
            <div class="titulo">Posted by <span class="nombre">${details_formatPostedBy(postedBy)}</span> <span class="fecha timestamp">${details_formatTimestamp(creationTime)}</span></div>
            <div class="texto">${text}</div>
        </div>
    </div>
</script>
