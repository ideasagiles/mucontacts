<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
<%@ page import="com.ideasagiles.contactmanager.domain.AuthenticationProvider" %>
<%-- Disabling EL in JSP for using Jquery Template. Alternative methods:
     http://www.ke-cai.net/2010/12/jquery-template-markup-and-jsp.html
--%>
<%@ page isELIgnored="true" %>


<div class="mainContent anchoPagina center" id="subscription">
</div>

<div class="mainContent anchoPagina">
    <h1>Update your account information</h1>
    <form id="userForm" method="POST"></form>
</div>


<script id="userFormTemplate" type="text/x-jquery-tmpl">
    <input type="hidden" name="id" value="${id}"/>
    <div class="row">
        <label>Username: </label>
        <span>${username}</span>
    </div>
    <div class="row">
        <label>Email: </label>
        <input type="text" name="email" size="50" value="${email}" maxlength="50"/>
    </div>
    {{if authenticationProvider == '<%=AuthenticationProvider.LOCAL%>'}}
    <div class="row">
        <label>Password: </label>
        <input type="password" name="password" size="50" value="" maxlength="50"/>
    </div>
    <div class="row">
        <label>Retype-Password: </label>
        <input type="password" id="retypePassword" size="50" value="" maxlength="50"/>
    </div>
    {{/if}}
    <div class="row">
        <label>Real Name: </label>
        <input type="text" name="name" size="50" value="${name}" maxlength="100"/>
    </div>
    <div class="actions">
        <input id="submitButton" type="submit" class="buttonAction" value="Save changes"/>
        <span id="cancelAccountContainer"><a href="#" id="cancelAccount">I want to erase all my data and cancel my account.</a></span>
    </div>

</script>

<script id="subscriptionTemplate" type="text/x-jquery-tmpl">
    <h1>Your current subscriptions</h1>
    {{if validThrough}}
    <p>You have the ${plan.name}. This plan expires on ${validThroughText}</p>
    <p id="cancelSubscriptionContainer"><a href="#" id="cancelSubscription">I want to cancel my subscription.</a></p>
    {{else}}
    <p>You have the ${plan.name}. This plan is valid for life!</p>
    {{/if}}

    <p>${detailText}</p>

    {{if !validThrough}}
    <p>
        <a class="buttonAction narrow" href="<c:url value="/plan/plans.html"/>">Upgrade now!<span class="byline">see pricing & plans</span></a>
    </p>
    {{/if}}
    <p>

    <h1>Referral Program</h1>
    <p>
        Check your current status in the <a href="<c:url value="/plan/referrals.html"/>">Referrals Center</a>.
    </p>
    <p>
        Invite your friends to update your Plan with great extra benefits. Just send them this link:<br/><br/>
        <span class="highlightBox">${referralLink}</span>
    </p>
    {{if !validThrough}}
    {{/if}}
</script>

<script src="<c:url value="/js/ui/user/account.js"/>"></script>
