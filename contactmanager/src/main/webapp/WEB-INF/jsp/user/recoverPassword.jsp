<%@ taglib prefix='c' uri='http://java.sun.com/jstl/core_rt' %>

<div class="mainContent anchoPagina">
    <h1>Recover your password</h1>

    <form id="recoverPassword" method="POST">
        <div class="row">
            <label>Your email</label>
            <input type="email" value="" id="email" name="email" required="required" size="50" maxlength="50"/>
        </div>
        <div class="actions">
            <input id="submitButton" class="buttonAction" type="submit" value="Reset password"/>
        </div>
    </form>
</div>

<script src="<c:url value="/js/ui/user/recoverPassword.js"/>"></script>