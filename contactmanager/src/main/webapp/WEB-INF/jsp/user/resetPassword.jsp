<%@ taglib prefix='c' uri='http://java.sun.com/jstl/core_rt' %>

<div class="mainContent anchoPagina">
    <h1>Reset your password</h1>

    <form id="resetPassword" method="POST">
        <input type="hidden" id="email" name="email" value='<c:out value="${param['email']}"/>'/>
        <input type="hidden" id="token" name="token" value='<c:out value="${param['token']}"/>'/>
        <div class="row">
            <label>New password</label>
            <input type="password" value="" id="password" name="password"  required="required" size="50" maxlength="50"/>
        </div>
        <div class="row">
            <label>Retype password</label>
            <input type="password" value="" id="retypePassword" required="required" size="50" maxlength="50"/>
        </div>
        <div class="actions">
            <input id="submitButton" class="buttonAction" type="submit" value="Save changes"/>
        </div>
    </form>
</div>

<script src="<c:url value="/js/ui/user/resetPassword.js"/>"></script>
