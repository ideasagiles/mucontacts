<%@ taglib prefix='c' uri='http://java.sun.com/jstl/core_rt' %>

<div class="banner"><!--start of BANNER-->
    <div class="contenedor anchoPagina">
        <div class="title">Just one more step to join <strong>muContacts!</strong></div>
    </div>
</div><!--end of BANNER-->

<div class="part1"><!--start of PART 1-->
    <div class="contenedor anchoPagina">
        <div class="items">
            <div class="item1">
                <div class="titulo">About you</div>
                <form id="userSignup" method="POST">
                    <input type="hidden" id="token" name="token" value="${param.token}" />
                    <div class="row">
                        <label>Username:</label>
                        <input type='text' name='username' required="required" size="30" maxlength="50"/>
                    </div>
                    <div class="row">
                        <label>Email:</label>
                        <input type='email' name='email' size="30" maxlength="255"/>
                    </div>
                    <div class="row">
                        <label>Password:</label>
                        <input type='password' name='password' required="required" size="30" maxlength="50"/>
                    </div>
                    <div class="row">
                        <label>Retype password:</label>
                        <input type='password' id="retypePassword" required="required" size="30" maxlength="50"/>
                    </div>
                    <div class="button">
                        <input id="userSignupButton" type="image" src="<c:url value="/images/signup-button.png"/>" alt="Sign Up"/>
                        <br/>
                        <a href="<c:url value='/user/signin.html'/>">Sign in with your account</a>
                    </div>
                </form>
            </div>

            <div class="item2">
                <div class="titulo">Sign up with your Twitter or Facebook account</div>
                <div class="redessociales">
                    <div class="twitter">
                        <form id="tw_signin" action="<c:url value="/services/signin/twitter" />" method="POST">
                            <input type="image" src="<c:url value="/images/twitter-signup.png"/>" alt="Sign up with Twitter" />
                        </form>
                    </div>
                    <div class="facebook">
                        <form id="fb_signin" action="<c:url value="/services/signin/facebook" />" method="POST">
                            <input type="hidden" name="scope" value="email,offline_access"/>
                            <input type="image" src="<c:url value="/images/facebook-signup.png"/>" alt="Sign up with Facebook" />
                        </form>
                    </div>
                </div>

            </div>
            <br clear="all"/>
            <div class="item3">
                By joining, you are accepting the <a id="termsAndConditions" href="https://www.mucontacts.com/termsOfUse.html">Terms of Use</a>.
            </div>

        </div>
    </div>
</div><!--end of PART 1-->

<div class="part2 anchoPagina"><!--start of PART 2-->

    <div class="contenedor anchoPagina">

        <div class="info">
            Thanks for joining <strong>muContacts!</strong><br>
            Have fun while growing your relationships.
        </div>

    </div>

</div><!--end of PART 2-->

<div class="part3"><!--start of PART 3-->

    <div class="contenedor anchoPagina">

        <div class="info">
            I need help!<br>
            We�d love to hear from you! Just email us at<br>
            <a href="mailto:support@mucontacts.com">support@mucontacts.com</a>
        </div>

    </div>

</div><!--end of PART 3-->

<script src="<c:url value="/js/ui/user/signup.js"/>"></script>
