<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%-- Disabling EL in JSP for using Jquery Template. Alternative methods:
     http://www.ke-cai.net/2010/12/jquery-template-markup-and-jsp.html
--%>
<%@page isELIgnored="true" %>

<fmt:setBundle basename="projectInfo"/>
<div class="mainContent anchoPagina">
    <h1>Site Metrics</h1>
    <div id="weeklyGrowth" class="infoBox">
        <h3>Growth Rate</h3>
        <p>The weekly growth shows the percentage of new users since previous Sunday</p>
        <span></span>
    </div>
</div>

<div class="mainContent anchoPagina">
    <h1>Enabled users</h1>
    <table id="enabledUsersTable" class="list tablesorter">
        <thead>
            <tr>
                <th>Id</th>
                <th>Username</th>
                <th>Email</th>
                <th>Authentication</th>
                <th>Member since</th>
                <th>Last login</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody id="enabledUsers">
            <!-- initial empty rows needed by jquery-tablesort -->
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
        </tbody>
    </table>
</div>

<div class="mainContent anchoPagina">
    <h1>Disabled users</h1>
    <table id="disabledUsersTable" class="list tablesorter">
        <thead>
            <tr>
                <th>Id</th>
                <th>Username</th>
                <th>Email</th>
                <th>Authentication</th>
                <th>Member since</th>
                <th>Last login</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody id="disabledUsers">
            <!-- initial empty rows needed by jquery-tablesort -->
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
        </tbody>
    </table>
</div>

<div class="mainContent anchoPagina">
    <h1>Build information</h1>
    <p><strong>Build date: </strong><fmt:message key="project.build.date"/></p>
    <p><strong>Revision: </strong><fmt:message key="project.git.commit.id"/></p>
    <p><strong>Revision date: </strong><fmt:message key="project.git.commit.time"/></p>
</div>


<script id="enabledUserTemplate" type="text/x-jquery-tmpl">
    <tr>
        <td>${id}</td>
        <td>${username}</td>
        <td>${email}</td>
        <td>${authenticationProvider}</td>
        <td>${formatDate(memberSince)}</td>
        <td>${formatDate(lastSignIn)} (<span class="timestamp">${formatTimestamp(lastSignIn)}</span>)</td>
        <td><a class="admin_disableUser" href="#" data-id="${id}">Disable</a></td>
    </tr>
</script>

<script id="disabledUserTemplate" type="text/x-jquery-tmpl">
    <tr>
        <td>${id}</td>
        <td>${username}</td>
        <td>${email}</td>
        <td>${authenticationProvider}</td>
        <td>${formatDate(memberSince)}</td>
        <td>${formatDate(lastSignIn)} (<span class="timestamp">${formatTimestamp(lastSignIn)}</span>)</td>
        <td>
            <a class="admin_enableUser" href="#" data-id="${id}">Enable</a>
            &#8226 <a class="admin_deleteUser" href="#" data-id="${id}">DELETE</a>
        </td>
    </tr>
</script>

<script src="<c:url value="/js/app/service/admin/admin.js"/>"></script>

<script type="text/javascript">
    $(document).ready(function() {
        $("#enabledUsersTable").tablesorter();
        $("#disabledUsersTable").tablesorter();

        renderAllUsers();
        renderWeeklyGrowth();
    });

    function renderWeeklyGrowth() {
        mucontacts.service.admin.weeklyGrowthRate(function (growthRate) {
            growthRate = parseFloat(growthRate).toFixed(2);
            $("#weeklyGrowth span").empty();
            $("#weeklyGrowth span").html(growthRate + "%");
        });
    }

    function renderAllUsers() {
        mucontacts.service.admin.findAllUsers(function (users) {
            var enabledUsers = users.filter(function (item){
                return item.enabled;
            });
            var disabledUsers = users.filter(function (item){
                return !item.enabled;
            });

            //object used to format timestamp from User (needed by jquery timestamp plugin)
            var userDateFormater = {
                getLastSignInTimestamp: function () {
                    return new Date(this.data.lastSignIn).toString();
                }
            };

            $("#enabledUsers").empty();
            $("#disabledUsers").empty();
            $("#enabledUserTemplate").tmpl(enabledUsers, userDateFormater).appendTo("#enabledUsers");
            $("#disabledUserTemplate").tmpl(disabledUsers, userDateFormater).appendTo("#disabledUsers");

            $(".admin_disableUser").click(function () {
                var id = $(this).data("id");
                disableUser(id);
                return false;
            });

            $(".admin_enableUser").click(function () {
                var id = $(this).data("id");
                enableUser(id);
                return false;
            });

            $(".admin_deleteUser").click(function () {
                var id = $(this).data("id");
                if (confirm("Are you sure you want to FULLY DELETE this user? THERE IS NO ROLLBACK!")) {
                    deleteUserAccount(id);
                }
                return false;
            });

            //auto-refresh timestamps, in milliseconds
            $(".timestamp").cuteTime({
                refresh: 1000*60
            });

            //trigger update event for jquery-tablesort
            $("#enabledUsersTable").trigger("update");
            $("#disabledUsersTable").trigger("update");
        });
    }

    /** Formats the given date into a human readable string. */
    function formatDate(timeInMillis) {
        var date = new Date(timeInMillis);

        var year = date.getFullYear();
        var month = date.getMonth() + 1;
        var monthDay = date.getDate();
        if (monthDay < 10) {
            monthDay = "0" + monthDay;
        }
        if (month < 10) {
            month = "0" + month;
        }
        return year + "-" + month + "-" + monthDay;
    }

    function disableUser(userId) {
        mucontacts.service.admin.disableUser(userId, function() {
            mucontacts.browser.displayAcceptedMessage("User disabled", "The user is now disabled and can't login to the application.");
            renderAllUsers();
        });
    }

    function enableUser(userId) {
        mucontacts.service.admin.enableUser(userId, function() {
            mucontacts.browser.displayAcceptedMessage("User enabled", "The user is now enabled.");
            renderAllUsers();
        });
    }

    function deleteUserAccount(userId) {
        if (confirm("Are you sure you want to PERMANENTLY DELETE this user and ALL of his content?")) {
            mucontacts.service.admin.deleteUserAccount(userId, function() {
                mucontacts.browser.displayAcceptedMessage("User deleted", "The user has been deleted.");
                renderAllUsers();
            });
        }
    }

    /** Returns the timestamp as string as needed by jquery Timestamp plugin */
    function formatTimestamp(timeInMillis) {
        return new Date(timeInMillis).toString();
    }
</script>
