/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

$(document).ready(function () {
    loadUserIntoForm();
    loadSubscription();

    mucontacts.form.bindValidateSubmitForm($("#userForm"), function (form) {
        var password = $("input[name='password']");
        var retypePassword = $("input[id='retypePassword']");
        if (password.val().length > 0) {
            if (!mucontacts.form.validatePassword(password, retypePassword)) {
                return false;
            }
        }

        var user = form.serializeObject();
        mucontacts.service.user.update(user, onUpdateSuccess);
    });

    /*mucontacts.form.bindValidateSubmitForm($("#inviteUserForm"), function (form) {
        var invitationList = $("#invitations");
        if (invitationList.val().length === 0) {
            return false;
        }

        mucontacts.service.user.inviteUsersToOC(invitationList.val(), onInvitationSuccess);
    });*/


    $(document).on("click", "#cancelSubscription", function (e) {
        if (confirm("Are you sure you want to cancel your current subscription?")) {
            mucontacts.service.user.cancelSubscription(onCancelSubscriptionSuccess);
        }
        return false;
    });

    $(document).on("click", "#cancelAccount", function (e) {
        if (confirm("Are you sure you want to FULLY DELETE your account and ALL its data? This can not be undone!")) {
            mucontacts.service.user.cancelAccount(onCancelAccountSuccess);
        }
        return false;
    });

});


function onUpdateSuccess(data) {
    mucontacts.browser.displayAcceptedMessage("Account updated", "Your account has been updated.");
}

function onInvitationSuccess(data) {
    $("#invitations").val('');
    mucontacts.browser.displayAcceptedMessage("Invitations Sent", "Your invitations has been sent.");
}

function onCancelSubscriptionSuccess(data) {
    mucontacts.browser.displayAcceptedMessage("Subscription Canceled", "Your subscription has been canceled, this changed will be shown in the following 24 hours.");
    $("#cancelSubscriptionContainer").html("As requested, we will cancel your subscription during the following 24 hours. Thanks for using muContacts!");
}

function onCancelAccountSuccess(data) {
    mucontacts.browser.displayAcceptedMessage("Account Canceled", "Your account and all its data will be erased in the following 48 hours.");
    $("#cancelAccountContainer").html("As requested, we will erase your account and all its data during the following 48 hours. Please contact us if you want to cancel this request.");
}


/**
     * Loads a user into the User form.
     * This functions uses the request parameter "userId" to load a user
     * into the user form.
     */
function loadUserIntoForm() {
    mucontacts.service.user.currentUser(function (user) {
        renderForm(user);
    });
}

/**
 * Load the plan into the plan template
 */
function loadSubscription() {
    mucontacts.service.user.currentSubscription(function (subscription) {
        subscription.validThroughText = formatTimestampToDate(subscription.validThrough);
        subscription.referralLink = mucontacts.baseURI + "user/signup.html?referral_code=" + subscription.user.referralCode;
        subscription.detailText = createDetailText(subscription.plan.maxContacts, subscription.plan.maxAddressBooks, subscription.plan.maxReadOnlyInvitations, subscription.plan.maxWriteInvitations);
        $("#subscriptionTemplate").tmpl(subscription).appendTo("#subscription");
    });
}

/**
 * Get the detail text to use in the account page
 */
function createDetailText(maxContacts, maxAddressBooks, maxReadOnlyInvitations, maxWriteInvitations) {
    var text = "With this plan you can store ";
    text += basicPluralize(maxContacts, "contact");
    text += " and create ";
    text += basicPluralize(maxAddressBooks, "AddressBook");
    text += " (with ";
    text += basicPluralize(maxWriteInvitations, "collaborator");
    text += ").";
    return text;
}

/**
 * This function pluralize text for the detail text.
 */
function basicPluralize(amount, text) {
    if (amount == null) {
        return "unlimited " + text + "s";
    } else if (amount === 0) {
        return "no " + text + "s";
    } else if (amount === 1) {
        return "up to " + amount + " "+ text;
    } else {
        return "up to " + amount + " "+ text + "s";
    }
}

/**
 * Formats a unix timestamp (in seconds), returns a String.
 * Read more about date styles: http://en.wikipedia.org/wiki/Wikipedia:Manual_of_Style/Dates_and_numbers
 */
function formatTimestampToDate(timestamp) {
    var monthNames = new Array("January", "February", "March", "April", "May",
        "June", "July", "August", "September", "October", "November", "December");

    var date = new Date(timestamp);
    var day = date.getDate();
    var month = date.getMonth();
    var year = date.getFullYear();
    return monthNames[month] + " " + day + ", " + year;  //"June 9, 2012"
}

function renderForm(data) {
    $("#userFormTemplate").tmpl(data).appendTo("#userForm");
    $("input[name='name']").focus();
}
