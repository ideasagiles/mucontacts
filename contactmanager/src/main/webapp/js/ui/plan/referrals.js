/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

$(document).ready(function () {
    $("#counter_level_1").hide();
    $("#counter_level_2").hide();
    $("#counter_level_3").hide();
    $("#counter_level_4").hide();

    mucontacts.service.user.findReferralInfo(renderReferralInfo);

});

function renderReferralInfo(referralInfo) {
    var counterContainer;
    var referralCounter;
    var maxLevel = 0;  //max plan level achieved will be highlighted

    //render each referral counter
    for (var level = 1; level <= 4; level++) {
        counterContainer = $("#counter_level_" + level);
        referralCounter = referralInfo["referralsLeftForLevel" + level];
        if (referralCounter > 0) {
            counterContainer.html(referralCounterToString(referralCounter));
            counterContainer.show();
        } else {
            maxLevel = level;
        }
    }

    //If the user was referred, the maxLevel is level one
    if (referralInfo["referred"]) {
        maxLevel = 1;
        counterContainer = $("#counter_level_1");
        counterContainer.html("for<br/>free!");
        counterContainer.show();
    }

    //highlight current plan
    $("#info_level_" + maxLevel).addClass("active");
}

function referralCounterToString(counter) {
    var countText = counter;
    if (counter > 1) {
        countText += " referrals";
    } else {
        countText += " referral";
    }
    return countText;
}
