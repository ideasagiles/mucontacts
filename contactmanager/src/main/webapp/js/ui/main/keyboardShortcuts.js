/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
 * Prepare the Browse page to handle keyboard shortcuts.
 */
function browse_bindKeyShortcuts() {
    resetMousetrap();
    //TODO: This key combination is not working on FF.
    Mousetrap.bind(['alt+n', 'command+n'], function () {
        browse_goToEdit();
        return false;
    });
    Mousetrap.bind(['alt+f', 'command+f'], function() {
        main_scrollToTop();
        $('#browse_q').focus();
        return false;
    });
    Mousetrap.bind(['down'], function() {
        browse_moveSelectedContactCursor(1);
        return false;
    });
    Mousetrap.bind(['up'], function() {
        browse_moveSelectedContactCursor(-1);
        return false;
    });
    Mousetrap.bind(['enter'], function() {
        if (main_selectedContact) {
            browse_goToDetails(main_selectedContact.id);
        }
        return false;
    });
}

/**
 * Prepare the Detail page to handle keyboard shortcuts.
 */
function detail_bindKeyShortcuts() {
    resetMousetrap();
    Mousetrap.bind(['command+enter', 'ctrl+enter'], function() {
        $("#details_commentForm").submit();
        return false;
    });

    //TODO: This key combination is not working on FF.
    Mousetrap.bind(['alt+n', 'command+n'], function() {
        main_setSelectedContact(null);
        details_goToEdit();
        return false;
    });
    Mousetrap.bind(['alt+b', 'command+b'], function() {
        details_goToBrowse();
        return false;
    });

    //TODO: This key combination is not working on FF.
    Mousetrap.bind(['alt+u', 'command+u'], function(e) {
        //Instead of the function, call the event. If the user has no permission
        // then nothing will happens.
        e.preventDefault();
        $("#details_updateButton").click();
        return false
    });
}

/** Returns the contact id of the first selectable contact. */
function browse_getFirstSelectableContactId() {
    return $("#browse_contactList a:first").data("id");
}

/** Checks if the selected contact is visible in the contact list.
 * @return true if the contact is visible, false if it is not visible or it is
 * not setted.
 */
function browse_selectedContactIsVisible() {
    if (main_selectedContact) {
        var currentElement = browse_findElementByContactId(main_selectedContact.id);
        if (currentElement.length == 0) {
            return false;
        }
        return true;
    }
    return false;
}

/**
 * Moves the selected contact cursor to the previous or next contact.
 * @param direction if direction > 0 it moves the cursor to the next visible
 *        contact, if direction < 0 it moves the cursor to the previous
 *        visible contact, otherwise it does not move the cursor.
 */
function browse_moveSelectedContactCursor(direction) {
    var currentElement;
    var element;
    var contactId;

    if (!browse_selectedContactIsVisible()) {
        main_setSelectedContact(browse_getFirstSelectableContactId());
        direction = 0; //cancel cursor movement
    }

    if (main_selectedContact) {
        currentElement = browse_findElementByContactId(main_selectedContact.id);

        if (direction > 0) {
            element = currentElement.next();
        }
        else if (direction < 0) {
            element = currentElement.prev();
        }
        else {
            element = currentElement;
        }

        contactId = element.data("id");
        if (contactId) {
            main_setSelectedContact(contactId);
            browse_highlightSelectedContact();
            browse_scrollToSelectedContact();
        }
    }
}

/**
 * Highlights the current selected contact in the list.
 */
function browse_highlightSelectedContact() {
    browse_resetSelectMark();
    if (main_selectedContact) {
        var element = browse_findElementByContactId(main_selectedContact.id);
        element.find(".browse_divContactListItem").addClass("contactSelected");
    }
}

/**
 * Scrolls the viewport to the current selected contact.
 */
function browse_scrollToSelectedContact() {
    if (main_selectedContact) {
        var element = browse_findElementByContactId(main_selectedContact.id);

        //scroll to element if it is visible
        if (element.offset()) {
            //element.height() returns zero in Chrome because is not explicitily setted (ouch!).
            //Ask the height to its first children (it has the CSS height property setted).
            var bottom = element.offset().top + element.children().height();
            var offsetToContactDiv = bottom - $(window).height();
            $(window).scrollTop(offsetToContactDiv);
        }
    }
}

/**
* Searches the HTML element that has the given contact id as data.
*/
function browse_findElementByContactId(contactId) {
    return $("#browse_contactList").find("[data-id='" + contactId + "']");
}

/**
* Erases all selection marks from contacts list.
*/
function browse_resetSelectMark() {
    $(".contactSelected").removeClass("contactSelected");
}


/**
* Prepare the edit page to handle keyboard shortcuts.
*/
function edit_bindKeyShortcuts() {
    resetMousetrap();
    Mousetrap.bind(['command+enter', 'ctrl+enter'], function() {
        $("#edit_contactForm").submit();
        return false;
    });
    Mousetrap.bind(['alt+b', 'command+b'], function(e) {
        e.preventDefault();
        edit_goToBrowse();
        return false;
    });
}

function resetMousetrap() {
    Mousetrap.reset();
    Mousetrap.bind(['?'], function() {
        main_showKeyboardShortcutsHelp();
    });
}

function main_showKeyboardShortcutsHelp() {
    mucontacts.browser.displayModalMessage($("#keyboardShortcuts").html());
}
