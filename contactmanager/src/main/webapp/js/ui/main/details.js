/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

function details_ready() {
    mucontacts.form.bindValidateSubmitForm($("#details_commentForm"), function (form) {
        var comment = form.serializeObject();
        comment.contactId = main_selectedContact.id;
        mucontacts.service.comment.save(comment, details_onSaveCommentSuccess);
    });

    //bind actions
    $("#details_browseButton").click(function () {
        details_goToBrowse();
        return false;
    });
    $("#details_newButton").click(function () {
        main_setSelectedContact(null);
        details_goToEdit();
        return false;
    });
    $(".details_emotion").click(function () {
        var selectedEmotion = $(this);
        var idEmotion = selectedEmotion.data("id");
        $("#emotion").val(idEmotion);

        $(".details_emotion").removeClass("selected");
        selectedEmotion.addClass("selected");
    });
}

function details_showPanel() {
    $("#details_commentDetails").empty();

    browse_hidePanel();
    edit_hidePanel();
    $("#details_panel").show();

    details_renderContactDetails();
    details_renderCommentDetails();

    //select first emotion in comments
    $(".details_emotion:first").click();
    $("#details_commentText").focus();
    detail_bindKeyShortcuts();
}

function details_hidePanel() {
    $("#details_panel").hide()
}

function details_goToBrowse() {
    details_hidePanel();
    main_goToBrowse();
}

function details_goToEdit() {
    details_hidePanel();
    if (main_selectedContact) {
        main_goToEdit(main_selectedContact.id);
    } else {
        main_goToEdit(null);
    }
}

function details_renderContactDetails() {
    $("#details_contactDetails").empty();
    $("#details_contactDetailsTemplate").tmpl(main_selectedContact).appendTo("#details_contactDetails");
    details_renderActionButtons(main_selectedContact.addressBook.id);
}

function details_renderActionButtons(addressBookId) {
    //hide action buttons until permissions are checked
    $("#details_deleteButton").hide();
    $("#details_updateButton").hide();
    $("#details_commentFormDetails").hide();

    $.each(main_addressBookPermissions, function () {
        if (this.addressBook.id == addressBookId) {
            if (this.permission.indexOf("w") != -1) {
                //bind actions
                $("#details_deleteButton").click(function () {
                    details_onDeleteClick();
                    return false;
                });
                $("#details_updateButton").click(function () {
                    details_goToEdit();
                    return false;
                });

                $("#details_deleteButton").show();
                $("#details_updateButton").show();
                $("#details_commentFormDetails").show();
            }
        }
    });
}

function details_renderCommentDetails() {
    mucontacts.service.comment.findByContact(main_selectedContact.id, function (data) {
        $("#details_commentDetails").empty();
        $("#details_commentDetailsTemplate" ).tmpl(data, {
            getIconUrl: function () {
                var comment = this.data;
                return "images/smilies/smile-" + comment.emotion.toLowerCase() + ".png";
            }
        }).appendTo("#details_commentDetails");

        //auto-refresh timestamps, in milliseconds
        $(".timestamp").cuteTime({
            refresh: 1000*60
        });
    });
}

function details_onSaveCommentSuccess(data) {
    mucontacts.browser.displayAcceptedMessage("Comment saved", "Your comment was saved succesfully.");
    details_renderCommentDetails();
    $("#details_commentText").val("");
    $("#details_commentText").focus();
}

function details_onDeleteClick() {
    var id = main_selectedContact.id;
    if (confirm("Are you sure you want to PERMANENTLY DELETE this contact?")) {
        mucontacts.service.contact.destroy(id, function (data) {
            main_refreshContacts();
            mucontacts.browser.displayAcceptedMessage("Contact deleted", "The contact was successfully deleted.");
            $("#details_deleteButton").hide("fast");
            $("#details_updateButton").hide("fast");
            $("#details_commentFormDetails").hide("fast");
        });
    }
}

/** Returns the alias of a user to render. */
function details_formatPostedBy(user) {
    return user.name || user.username;
}

/** Returns the timestamp as string as needed by jquery Timestamp plugin */
function details_formatTimestamp(timeInMillis) {
    return new Date(timeInMillis).toString();
}