/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

function browse_ready() {
    //prepare search form
    $('#browse_q').clearableTextField();
    $('#browse_q').bind("keyup change paste cut", browse_throttle(browse_renderContactList, 100));

    //bind actions
    $("#browse_searchForm").submit(function () {
        browse_renderContactList();
        return false;
    });
    $("#browse_newContact").click(function () {
        browse_goToEdit();
        return false;
    });
    $(document).on("click", "a.browse_contactListItem", function () {
        var contactId = $(this).data("id");
        browse_goToDetails(contactId);
        return false;
    });
    $(document).on("click", ".browse_addressBookListItem", function () {
        var addressBookId = $(this).data("id");
        browse_toggleAddressBook(addressBookId);
        return false;
    });
    $(document).on("click", "#browse_createFirstContact", function () {
        browse_goToEdit();
        return false;
    });
    $(document).on("click", "a.browse_tagItem", function () {
        var tagName = $(this).data("name");
        browse_filterByTag(tagName);
    });
    $(document).on("click", "#browse_emailList", function () {
        browse_renderEmailList();
        return false;
    });
    $(document).on("click", "#browse_exportToCSV", function (e) {
        mucontacts.service.contact.exportAllToCsv(function () {
            mucontacts.browser.displayAcceptedMessage("Export finished", "All your contacts were exported.");
        });
        return false;
    });
    $(document).on("click", "#browse_importFromCSV", function(e) {
        browse_renderImportFromCSVModal();
        return false;
    });
    $(document).on("click", "#browse_showKeyboardShortcuts", function(e) {
        main_showKeyboardShortcutsHelp();
    });

    browse_renderAddressBooks();
    browse_renderContactList();
    browse_renderTagList();
}

function browse_showPanel() {
    details_hidePanel();
    edit_hidePanel();
    $("#browse_panel").show();

    browse_highlightSelectedContact();
    browse_scrollToSelectedContact();
    browse_bindKeyShortcuts();
}

function browse_hidePanel() {
    $("#browse_panel").hide();
}

function browse_goToDetails(contactId) {
    main_setSelectedContact(contactId);
    browse_hidePanel();
    main_goToDetails(contactId);
}

function browse_goToEdit() {
    main_setSelectedContact(null);
    browse_hidePanel();
    main_goToEdit();
}


function browse_renderEmailList() {
    var contacts = browse_filterContactsWithDefaults();

    $("#browse_emailListContainer").empty();
    $("#browse_emailListTemplate").tmpl(contacts).appendTo("#browse_emailListContainer");
    mucontacts.browser.displayModalMessage($("#browse_emailListInfo").html());
}

function browse_renderImportFromCSVModal() {
    mucontacts.service.addressbook.findAllWithMembers(function(addressBookWithMember) {
        $("#browse_importFromCSVContainer").empty();
        $("#browse_importFromCSVTemplate").tmpl(addressBookWithMember).appendTo("#browse_importFromCSVContainer");

        mucontacts.browser.displayModalMessage($("#browse_importFromCSVInfo").html(), function () {
            $(".browse_importFromCsvForm").ajaxForm({
                success: function(contactsCount) {
                    mucontacts.browser.displayAcceptedMessage("Contacts successfully imported!", "Great! " + contactsCount + " contacts were imported into your address book");
                },
                error: function() {
                    mucontacts.browser.displayErrorMessage("Contacts were not imported", "There was a problem importing your contacts. Are you sure it is a CSV file?");
                },
                complete: function() {
                    //clear file selection to avoid multiple submits
                    $(".browse_importFromCsvFile").val("");
                }
            });

        });

    });
}

function browse_renderAddressBooks() {
    $("#browse_addressBooks").empty();
    $("#browse_addressBookTemplate").tmpl(main_addressBookPermissions, {
        getStyleClass: function () {
            var addressBookPermission = this.data;
            if (addressBookPermission.isSelected) {
                return "selected";
            } else {
                return "unselected";
            }
        }
    }).appendTo("#browse_addressBooks");

    //round corners
    $("input[type='button'].browse_addressBookListItem:first").addClass("first");
    $("input[type='button'].browse_addressBookListItem:last").addClass("last");
}

/** Toggles the selected state of the given AddressBookId.
  */
function browse_toggleAddressBook(addressBookId) {
    //update AddressBook status in array
    for (var i=0; i<main_addressBookPermissions.length; i++) {
        var addressBookPermission = main_addressBookPermissions[i];
        if (addressBookPermission.addressBook.id == addressBookId) {
            addressBookPermission.isSelected = !addressBookPermission.isSelected;
            break;
        }
    }

    browse_renderAddressBooks();
    browse_renderContactList();
}

function browse_renderWelcomeMessage() {
    $("#browse_contactList").empty();
    $("#browse_contactTemplateWelcome").tmpl().appendTo("#browse_contactList");
}

/** Filters the contacts using the search query and the selected AddressBooks.
 */
function browse_filterContactsWithDefaults() {
    return mucontacts.search.contacts.filter(main_contacts, browse_getQueryStringAsWords(), browse_getSelectedAddressBooksIds());
}


/** Returns the current query string (from search form) as an array of words.
  * @return an array with 1 string in each position with no spaces, or an empty
  * array if the query string is empty.
  */
function browse_getQueryStringAsWords() {
    var wordArray = $.trim($("#browse_q").val()).split(/\s+/);
    if (wordArray[0].length === 0) {
        wordArray = [];
    }
    return wordArray;
}

function browse_renderContactList() {
    if (main_contacts.length <= 0) {
        //no contacts at all, display welcome message for first time users
        browse_renderWelcomeMessage();
        return;
    }

    var filteredContacts = browse_filterContactsWithDefaults();

    //render filtered contacts
    $("#browse_contactList").empty();
    $("#browse_contactTemplate").tmpl(filteredContacts, {
        getEmotionIconUrl: function () {
            return browse_getEmotionIconUrlForContactStatistics(this.data.statistics);
        }
    }).appendTo("#browse_contactList");

    browse_highlightSelectedContact();
}

/** Returns the emotion icon URL for the given contact statistics. */
function browse_getEmotionIconUrlForContactStatistics(statistics) {
    var orderedStats = browse_getEmotionStatisticsArray(statistics);

    if (orderedStats[0].count == 0) {
        //no stats yet
        return "images/ico-persona.png";
    }

    //we only show the most used emotion
    switch (orderedStats[0].emotion) {
        case "HAPPY": {
            return "images/smilies/smile-happy.png";
        }
        case "MAD": {
            return "images/smilies/smile-mad.png";
        }
        case "SAD": {
            return "images/smilies/smile-sad.png";
        }
        case "SCARED": {
            return "images/smilies/smile-scared.png";
        }
        default: {
            //unknown stat, render default icon
            return "images/ico-persona.png";
        }
    }
}

/** Creates an array of statistics from a contact, ordered by the emotion statistics count.
 *  @return an Array of objects (emotion, counter), ordered desc by counter.
 */
function browse_getEmotionStatisticsArray(statistics) {
    var stats = [];
    stats[0] = {
        emotion: "HAPPY",
        count: statistics.happyEmotionCount
    };
    stats[1] = {
        emotion: "MAD",
        count: statistics.madEmotionCount
    };
    stats[2] = {
        emotion: "SAD",
        count: statistics.sadEmotionCount
    };
    stats[3] = {
        emotion: "SCARED",
        count: statistics.scaredEmotionCount
    };

    function sortByCountDesc(stat1, stat2) {
        if (stat1.count > stat2.count) {
            return -1;
        }
        if (stat1.count < stat2.count) {
            return 1;
        }
        return 0;
    }

    stats.sort(sortByCountDesc);

    return stats;
}

function browse_renderTagList() {
    $("#browse_tagListItems").empty();
    $("#browse_tagList").tmpl(main_tags).appendTo("#browse_tagListItems");
}

function browse_filterByTag(tagName) {
    var searchQuery = $("#browse_q");

    if (searchQuery.val().indexOf(tagName) != -1) {
        browse_removeTagFromSearchQuery(searchQuery, tagName);
    } else {
        searchQuery.val(searchQuery.val() + tagName + " ");
    }

    searchQuery.trigger("keyup");
}

/**
  * Removes the given tagName from the given searchQuery texfield.
  * @param searchQuery a jQuery wrapped textfield.
  * @param tagName a String with the tagName to remove.
  */
function browse_removeTagFromSearchQuery(searchQuery, tagName) {
    if (searchQuery.val().indexOf(tagName) != -1) {
        searchQuery.val(searchQuery.val().replace(tagName + " ", ""));
        browse_removeTagFromSearchQuery(searchQuery, tagName);
    }
}

/**
 * Returns all the selected AddressBooks' ids.
 */
function browse_getSelectedAddressBooksIds() {
    var addressBookIds = [];
    for (var i=0; i<main_addressBookPermissions.length; i++) {
        var addressBookPermission = main_addressBookPermissions[i]
        if (addressBookPermission.isSelected) {
            addressBookIds.push(addressBookPermission.addressBook.id);
        }
    }
    return addressBookIds;
}

/**
  * Delays a function call a certain amount of time (in milliseconds). If
  * another call is made before the delay, the first call is canceled, the
  * timer is reseted and the new call is put on hold.
  * For example, its useful to "execute a search 2 seconds after the last keypress".
  * Read more: http://stackoverflow.com/questions/4364729/jquery-run-code-2-seconds-after-last-keypress
  *
  * @param f the function to invoke.
  * @param delay the delay in milliseconds. Default: 500.
  */
function browse_throttle(f, delay) {
    var timer = null;
    return function () {
        var context = this, args = arguments;
        clearTimeout(timer);
        timer = window.setTimeout(function (){
            f.apply(context, args);
        },
        delay || 500);
    };
}
