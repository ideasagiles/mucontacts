/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
 * Services for users.
 */
mucontacts.service.user = (function () {

    function save(user, onSuccessCallback) {
        function handleError(data) {
            if (data.status === 409) {
                mucontacts.browser.displayErrorMessage("User already exists", "The username is already in use. Please try recovering your password or using another username.");
            } else {
                mucontacts.service.genericHttpErrorHandler(data);
            }
        }
        var url = mucontacts.service.serviceBaseURI + "/user/create";
        $.postJSON(url, user, onSuccessCallback, handleError);
    }

    function update(user, onSuccessCallback) {
        $.postJSON(mucontacts.service.serviceBaseURI + "/user/update", user, onSuccessCallback, mucontacts.service.genericHttpErrorHandler);
    }

    function recoverPassword(email, onSuccessCallback) {
        var url = mucontacts.service.serviceBaseURI + "/user/" + email + "/recoverPassword";
        $.getJSON(url, onSuccessCallback, mucontacts.service.genericHttpErrorHandler);
    }

    function resetPassword(user, onSuccessCallback) {
        $.postJSON(mucontacts.service.serviceBaseURI + "/user/resetPassword", user, onSuccessCallback, mucontacts.service.genericHttpErrorHandler);
    }

    function currentUser(onSuccessCallback) {
        var url = mucontacts.service.serviceBaseURI + "/user/details";
        $.getJSON(url, onSuccessCallback, mucontacts.service.genericHttpErrorHandler);
    }

    function currentSubscription(onSuccessCallback) {
        var url = mucontacts.service.serviceBaseURI + "/user/currentSubscription";
        $.getJSON(url, onSuccessCallback, mucontacts.service.genericHttpErrorHandler);
    }

    /** Invites users to join the application.
     * @param emails a string with comma-separated emails.
     * @param onSuccessCallback callback function for onSucess event.
     */
    function inviteUsersToOC(emails, onSuccessCallback) {
        var url = mucontacts.service.serviceBaseURI + "/user/invite";
        $.postJSON(url, emails, onSuccessCallback, mucontacts.service.genericHttpErrorHandler);
    }

    function cancelSubscription(onSuccessCallback) {
        var url = mucontacts.service.serviceBaseURI + "/user/cancelSubscription";
        $.postJSON(url, null, onSuccessCallback, mucontacts.service.genericHttpErrorHandler);
    }

    function cancelAccount(onSuccessCallback) {
        var url = mucontacts.service.serviceBaseURI + "/user/cancelAccount";
        $.postJSON(url, null, onSuccessCallback, mucontacts.service.genericHttpErrorHandler);
    }

    function findReferralInfo(onSuccessCallback) {
        var url = mucontacts.service.serviceBaseURI + "/user/referralInfo";
        $.getJSON(url, onSuccessCallback, mucontacts.service.genericHttpErrorHandler);
    }

    return {
        save : save,
        update : update,
        recoverPassword : recoverPassword,
        resetPassword : resetPassword,
        currentUser : currentUser,
        inviteUsersToOC : inviteUsersToOC,
        currentSubscription: currentSubscription,
        cancelSubscription: cancelSubscription,
        cancelAccount: cancelAccount,
        findReferralInfo : findReferralInfo
    }

}());
