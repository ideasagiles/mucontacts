/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

mucontacts.service.admin = (function () {
    function findAllUsers(onSuccessCallback) {
        var url = mucontacts.service.serviceBaseURI + "/admin/users/all";
        $.getJSON(url, onSuccessCallback, mucontacts.service.genericHttpErrorHandler);
    }

    function enableUser(userId, onSuccessCallback) {
        var url = mucontacts.service.serviceBaseURI + "/admin/users/" + userId + "/enable";
        $.getJSON(url, onSuccessCallback, mucontacts.service.genericHttpErrorHandler);
    }

    function disableUser(userId, onSuccessCallback) {
        var url = mucontacts.service.serviceBaseURI + "/admin/users/" + userId + "/disable"
        $.getJSON(url, onSuccessCallback, mucontacts.service.genericHttpErrorHandler);
    }

    function deleteUserAccount(userId, onSuccessCallback) {
        var url = mucontacts.service.serviceBaseURI + "/admin/users/" + userId + "/delete";
        $.getJSON(url, onSuccessCallback, mucontacts.service.genericHttpErrorHandler);
    }
    
    function weeklyGrowthRate(onSuccessCallback) {
        var url = mucontacts.service.serviceBaseURI + "/admin/users/weeklyGrowthRate";
        $.getJSON(url, onSuccessCallback, mucontacts.service.genericHttpErrorHandler);
    }

    return {
        findAllUsers: findAllUsers,
        enableUser: enableUser,
        disableUser: disableUser,
        deleteUserAccount: deleteUserAccount,
        weeklyGrowthRate: weeklyGrowthRate
    }

}());



