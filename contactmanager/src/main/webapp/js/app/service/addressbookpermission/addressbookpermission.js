/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
 * Services for addressbooks permissions.
 */
mucontacts.service.addressbookpermission = (function () {

    function findAll(onSuccessCallback) {
        var url = mucontacts.service.serviceBaseURI + "/user/addressBookPermissions";
        $.getJSON(url, onSuccessCallback, mucontacts.service.genericHttpErrorHandler);
    }

    function destroy(id, onSuccessCallback) {
        $.getJSON(mucontacts.service.serviceBaseURI + "/addressBookPermission/delete/" + id, onSuccessCallback, mucontacts.service.genericHttpErrorHandler);
    }

    function update(addressBookPermissionId, newPermission, onSuccessCallback) {
        var url = mucontacts.service.serviceBaseURI + "/addressBookPermission/" + addressBookPermissionId + "/" + newPermission;
        $.getJSON(url, onSuccessCallback, mucontacts.service.genericHttpErrorHandler);
    }

    return {
        findAll : findAll,
        destroy : destroy,
        update : update
    }

}());
