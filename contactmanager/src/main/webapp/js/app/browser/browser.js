/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
 * Browser related utilities. This module helps displaying messages to the user.
 *
 */
mucontacts.browser = (function () {

    /**
     * Displays a critical error message to the user.
     * @param title the title for the message.
     * @param text the explanatory text of the message.
     */
    function displayCriticalMessage(title, text) {
        $("#freeow").freeow(title, text, {
            classes: ["lightgray", "critical"],
            autoHide: false,
            hideStyle: {
                opacity: 0,
                left: "400px"
            },
            showStyle: {
                opacity: 1,
                left: "0px"
            }
        });
    }

    /**
     * Displays an error message to the user.
     * @param title the title for the message.
     * @param text the explanatory text of the message.
     */
    function displayErrorMessage(title, text) {
        $("#freeow").freeow(title, text, {
            classes: ["lightgray", "critical"],
            autoHide: true,
            autoHideDelay: 8000,
            hideStyle: {
                opacity: 0,
                left: "400px"
            },
            showStyle: {
                opacity: 1,
                left: "0px"
            }
        });
    }

    /**
     * Displays a message informing an accepted operation.
     * @param title the title for the message.
     * @param text the explanatory text of the message.
     */
    function displayAcceptedMessage(title, text) {
        $("#freeow").freeow(title, text, {
            classes: ["lightgray", "accepted"],
            autoHide: true,
            hideStyle: {
                opacity: 0,
                left: "400px"
            },
            showStyle: {
                opacity: 1,
                left: "0px"
            }
        });
    }

    /**
     * Displays a message informing a rejected operation, posibly due to
     * validation problems.
     * @param title the title for the message.
     * @param text the explanatory text of the message.
     */
    function displayRejectedMessage(title, text) {
        $("#freeow").freeow(title, text, {
            classes: ["lightgray", "warning"],
            autoHide: true,
            autoHideDelay: 8000,
            hideStyle: {
                opacity: 0,
                left: "400px"
            },
            showStyle: {
                opacity: 1,
                left: "0px"
            }
        });
    }

    /**
     * Displays a message in a modal dialog.
     * @param html the text in html to show.
     * @param onCompleteCallback an optional callback to invoke once the content is displayed.
     */
    function displayModalMessage(html, onCompleteCallback) {
        $.fancybox({
            content : html,
            padding : 20,
            transitionIn : 'elastic',
            transitionOut : 'elastic',
            easingIn : 'easeOutBack',
            easingOut : 'easeInBack',
            overlayShow : true,
            overlayOpacity : 0.3,
            onComplete: onCompleteCallback
        });
    }

    /**
     * Checks if the web browser is unsupported by the application, and displays
     * a helpful message to the user.
     */
    function displayUnsupportedBrowserMessage() {
        var isBrowserSupported = true;
        if ($.browser.msie && parseInt($.browser.version) <= 6) {
            isBrowserSupported = false;
            window.location = 'https://www.mucontacts.com/browserNotSupported.html';
            return;
        }

        if (!isBrowserSupported) {
            displayCriticalMessage('Browser not supported', 'Your web browser is not supported. Please consider updating your browser to a newer version. We recommend Mozilla Firefox or Google Chrome to enjoy the best experience while using muContacts or surfing the Web!');
        }
    }


    return {
        displayCriticalMessage : displayCriticalMessage,
        displayErrorMessage : displayErrorMessage,
        displayAcceptedMessage : displayAcceptedMessage,
        displayRejectedMessage : displayRejectedMessage,
        displayModalMessage : displayModalMessage,
        displayUnsupportedBrowserMessage : displayUnsupportedBrowserMessage
    };

}());