/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
 * Public muContacts functions. This is the main module for all client-side
 * muContacts functions and services.
 */
var mucontacts = (function () {
    /** Returns the base URI for the application.
     */
    function baseURI() {
        if (document.baseURI) {
            return document.baseURI;
        }
        //IE does not support document.baseURI...
        var base = document.getElementsByTagName('base');
        if (base && base[0] && base[0].href) {
            return base[0].href;
        }
        //no base found in document, use relative URIs
        return "";
    }

    var BASE_URI = baseURI();

    return {
        baseURI : BASE_URI
    }
})();
