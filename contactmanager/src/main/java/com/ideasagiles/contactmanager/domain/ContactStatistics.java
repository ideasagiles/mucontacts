/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.contactmanager.domain;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * Usage statistics for a contact.
 */
@Embeddable
public class ContactStatistics implements Serializable {

    @Column(name="happy_emotion_count")
    private long happyEmotionCount;

    @Column(name="mad_emotion_count")
    private long madEmotionCount;

    @Column(name="sad_emotion_count")
    private long sadEmotionCount;

    @Column(name="scared_emotion_count")
    private long scaredEmotionCount;

    public long getHappyEmotionCount() {
        return happyEmotionCount;
    }

    public void setHappyEmotionCount(long happyEmotionCount) {
        this.happyEmotionCount = happyEmotionCount;
    }

    public long getMadEmotionCount() {
        return madEmotionCount;
    }

    public void setMadEmotionCount(long madEmotionCount) {
        this.madEmotionCount = madEmotionCount;
    }

    public long getSadEmotionCount() {
        return sadEmotionCount;
    }

    public void setSadEmotionCount(long sadEmotionCount) {
        this.sadEmotionCount = sadEmotionCount;
    }

    public long getScaredEmotionCount() {
        return scaredEmotionCount;
    }

    public void setScaredEmotionCount(long scaredEmotionCount) {
        this.scaredEmotionCount = scaredEmotionCount;
    }
}
