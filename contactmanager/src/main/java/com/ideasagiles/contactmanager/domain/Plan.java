/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.contactmanager.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.*;
import org.hibernate.validator.constraints.NotBlank;

/**
 * A commercial plan.
 */
@Entity
@Table(name = "plans")
public class Plan implements Serializable {

    /** The unique identifier */
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;

    @NotBlank
    @Column(name = "name")
    private String name;

    @Column(name = "max_contacts")
    private Integer maxContacts;

    @Column(name = "max_address_books")
    private Integer maxAddressBooks;

    @Column(name = "max_read_only_invitations")
    private Integer maxReadOnlyInvitations;

    @Column(name = "max_write_invitations")
    private Integer maxWriteInvitations;

    @Column(name = "amount")
    private BigDecimal amount;

    @Column(name = "duration_in_months")
    private Integer durationInMonths;

    @Column(name = "paypal_item_number")
    private String paypalItemNumber;

    @Column(name = "priority")
    private Integer priority;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getMaxAddressBooks() {
        return maxAddressBooks;
    }

    public void setMaxAddressBooks(Integer maxAddressBooks) {
        this.maxAddressBooks = maxAddressBooks;
    }

    public Integer getMaxContacts() {
        return maxContacts;
    }

    public void setMaxContacts(Integer maxContacts) {
        this.maxContacts = maxContacts;
    }

    public Integer getMaxReadOnlyInvitations() {
        return maxReadOnlyInvitations;
    }

    public void setMaxReadOnlyInvitations(Integer maxReadOnlyInvitations) {
        this.maxReadOnlyInvitations = maxReadOnlyInvitations;
    }

    public Integer getMaxWriteInvitations() {
        return maxWriteInvitations;
    }

    public void setMaxWriteInvitations(Integer maxWriteInvitations) {
        this.maxWriteInvitations = maxWriteInvitations;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Integer getDurationInMonths() {
        return durationInMonths;
    }

    public void setDurationInMonths(Integer durationInMonths) {
        this.durationInMonths = durationInMonths;
    }

    public String getPaypalItemNumber() {
        return paypalItemNumber;
    }

    public void setPaypalItemNumber(String paypalItemNumber) {
        this.paypalItemNumber = paypalItemNumber;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }
}
