/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.contactmanager.admin.controller;

import com.ideasagiles.contactmanager.admin.service.UserAdminService;
import com.ideasagiles.contactmanager.domain.User;
import java.util.Collection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Administration Controller for User resource.
 * @author Leito
 */
@Controller
public class UserAdminController {

    @Autowired
    private UserAdminService userAdminService;

    @RequestMapping(value = "/admin/users/all")
    public @ResponseBody Collection<User> findAllUsers() {
        return userAdminService.findAll();
    }

    @RequestMapping(value = "/admin/users/{id}/enable")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void enableUser(@PathVariable long id) {
        userAdminService.setUserEnabledStatus(id, true);
    }

    @RequestMapping(value = "/admin/users/{id}/disable")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void disableUser(@PathVariable long id) {
        userAdminService.setUserEnabledStatus(id, false);
    }

    @RequestMapping(value = "/admin/users/{id}/delete")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteUserAccount(@PathVariable long id) {
        userAdminService.deleteUserAccount(id);
    }

    @RequestMapping(value ="/admin/users/weeklyGrowthRate")
    public @ResponseBody double getWeeklyGrowthRate() {
        return userAdminService.getWeeklyGrowthRate();
    }

}
