/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.contactmanager.admin.dao;

import com.ideasagiles.contactmanager.domain.User;
import java.util.Collection;
import java.util.Date;

/**
 * Administration data access services for User.
 */
public interface UserAdminDao {

    /**
     * Finds all registered users of this application, from all authentication
     * providers, and in any status.
     *
     * @return a Collection with Users.
     */
    Collection<User> findAll();

    /**
     * Finds an user by its unique identifier.
     * @param id the unique identifier of an User.
     * @return the User with the given unique identifier.
     */
    User findById(long id);

    /**
     * Updates an user.
     * @param user the User to update.
     */
    void update(User user);

    /**
     * Deletes an User and all its dependencies. This method can not be undone.
     * @param id the unique identifier of an User to delete.
     */
    void deleteUserAccount(long userId);

    /**
     * Find new Users since a certain date.
     *
     * @param since Date since the new users should be searched
     * @return A collection of new users.
     */
    Collection<User> findNewUsersSince(Date since);

    /**
     * Return the count of the total users in the DB.
     *
     * @return an Integer representing the total count of users
     */
    long countAllUsers();

    /**
     * Count all users that joined before or on signupDate.
     * @param signupDate a member since Date until the users should be counted.
     * @return users count that joined before or on signupDate.
     */
    long countUsersUntilSignupDate(Date signupDate);

}
