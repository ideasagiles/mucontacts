/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.contactmanager.admin.service;

import com.ideasagiles.contactmanager.admin.dao.UserAdminDao;
import com.ideasagiles.contactmanager.domain.User;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Leito
 */
@Service
@PreAuthorize("hasRole('ADMIN')")
@Transactional
public class UserAdminServiceImpl implements UserAdminService {

    @Autowired
    private UserAdminDao userAdminDao;

    public void setUserAdminDao(UserAdminDao userAdminDao) {
        this.userAdminDao = userAdminDao;
    }

    @Override
    public Collection<User> findAll() {
        return userAdminDao.findAll();
    }

    @Override
    public void setUserEnabledStatus(long userId, boolean enabled) {
        User user = userAdminDao.findById(userId);
        user.setEnabled(enabled);
    }

    @Override
    public void deleteUserAccount(long userId) {
        User user = userAdminDao.findById(userId);
        if (user.getEnabled()) {
            throw new AccessDeniedException("User must be disabled to delete.");
        }
        userAdminDao.deleteUserAccount(userId);
    }

    @Override
    public double getWeeklyGrowthRate() {
        Date lastSunday = getLastSunday();
        long totalUsers = userAdminDao.countAllUsers();
        long totalUsersUntilLastSunday = userAdminDao.countUsersUntilSignupDate(lastSunday);
        long newUsers = totalUsers - totalUsersUntilLastSunday;

        double growthRate = (newUsers * 100.00 / totalUsers);

        return growthRate;
    }

    /**
     *  This method gets the previous Sunday.
     *  Read more: http://stackoverflow.com/questions/1319473/java-calendar-setcalendar-day-of-week-calendar-sunday-will-it-roll-backwards
     * @return the previous Sunday.
     */
    private Date getLastSunday() {
        Calendar lastSunday = new GregorianCalendar();
        int currentDayOfWeek = (lastSunday.get(Calendar.DAY_OF_WEEK) + 7 - lastSunday.getFirstDayOfWeek()) % 7;
        lastSunday.add(Calendar.DAY_OF_YEAR, -currentDayOfWeek);
        return lastSunday.getTime();
    }
}
