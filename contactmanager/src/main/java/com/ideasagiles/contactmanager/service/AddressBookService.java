/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.contactmanager.service;

import com.ideasagiles.contactmanager.domain.AddressBook;
import com.ideasagiles.contactmanager.domain.AddressBookInvitation;
import java.util.Collection;

/**
 * Class for AddressBook related services.
 *
 */
public interface AddressBookService {

    /** Finds the AddressBook with the given id.
     *
     * @param id the unique identifier of an AddressBook.
     * @return an AddressBook, or null if it does not exist.
     */
    AddressBook findById(long id);

    /** Saves a new AddressBook and assigns it to the logged in user with
     *  read/write/invite permissionss.
     *
     * @param addressBook the AddressBook to save.
     */
    void save(AddressBook addressBook);

    /** Updates the AddressBook. Only users with read/write/invite permissions
     * on AddressBook can update it (owners of AddressBook).
     *
     * @param addressBook the AddressBook to update.
     */
    void update(AddressBook addressBook);

    /** Invites an user to join an AddressBook.
     *
     * @param invitation the invitation to send.
     */
    void inviteUser(AddressBookInvitation invitation);

    /** Seaches for all AddressBookInvitations received and pending for the logged in user.
     *
     * @return a Collection of AddressBookInvitation.
     */
    Collection<AddressBookInvitation> getReceivedInvitationsForLoggedInUser();

    /** Seaches for all AddressBookInvitations sent by the logged in user.
     *
     * @return a Collection of AddressBookInvitation.
     */
    Collection<AddressBookInvitation> findInvitationByAddressBook(long addressBookId);

    /**
     * Accept an invitation
     *
     * @param invitatioIdn the Id of the AddressBookInvitation object.
     */
    void acceptInvitation(long invitationId);

    /**
     * Reject an invitation
     *
     * @param id  invitationId the Id of the AddressBookInvitation object.
     */
    public void rejectInvitation(long invitationId);

}
