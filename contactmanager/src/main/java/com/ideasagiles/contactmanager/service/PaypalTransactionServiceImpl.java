/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.contactmanager.service;

import com.ideasagiles.contactmanager.dao.PaypalTransactionDao;
import com.ideasagiles.contactmanager.dao.PlanDao;
import com.ideasagiles.contactmanager.dao.PrivateUserDao;
import com.ideasagiles.contactmanager.domain.PaypalSubscriptionPayment;
import com.ideasagiles.contactmanager.domain.Plan;
import com.ideasagiles.contactmanager.domain.PrivateUser;
import com.ideasagiles.contactmanager.domain.Subscription;
import com.ideasagiles.contactmanager.service.exception.PaymentTransactionException;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.Map;
import java.util.Set;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Leito
 */
@Transactional
public class PaypalTransactionServiceImpl implements PaypalTransactionService {

    private static Logger log = LoggerFactory.getLogger(PaypalTransactionServiceImpl.class);
    @Autowired
    private PaypalTransactionDao paypalTransactionDao;
    @Autowired
    private PlanDao planDao;
    @Autowired
    private SubscriptionService subscriptionService;
    @Autowired
    private PrivateUserDao privateUserDao;
    /**
     * Paypal validation URL. Used to post back a received request to validate
     * the transaction.
     */
    private URL validationUrl;
    /**
     * The Paypal email of the receiver account for a transaction. This is our
     * email registered in Paypal.
     */
    private String receiverEmail;

    public URL getValidationUrl() {
        return validationUrl;
    }

    public void setValidationUrl(URL validationUrl) {
        this.validationUrl = validationUrl;
    }

    public String getReceiverEmail() {
        return receiverEmail;
    }

    public void setReceiverEmail(String receiverEmail) {
        this.receiverEmail = receiverEmail;
    }

    /**
     * Checks if the transaction status from Paypal servers is Verified
     */
    private void checkStatusFromPaypal(String status) throws PaymentTransactionException {
        if (!status.equals("VERIFIED")) {
            throw new PaymentTransactionException("Paypal transaction validation failed. Status: " + status);
        }
    }

    /**
     * Checks if the paypal transaction has valid values
     */
    private void checkHasValidValues(PaypalSubscriptionPayment paypalTransaction) throws PaymentTransactionException {
        // check that paymentStatus=Completed or paymentStatus=Pending
        if (!paypalTransaction.isPaymentStatusCompleted() && !paypalTransaction.isPaymentStatusPending()) {
            throw new PaymentTransactionException("Payment transaction status is not 'Completed' nor 'Pending'. Payment status: " + paypalTransaction.getPaymentStatus());
        }
        if (paypalTransaction.getTransactionId() == null) {
            throw new PaymentTransactionException("Paypal transaction id is null.");
        }
    }

    /**
     * Check that receiverEmail is your Primary PayPal email
     */
    private void checkReceiverEmail(PaypalSubscriptionPayment paypalTransaction) throws PaymentTransactionException {
        if (!StringUtils.equals(paypalTransaction.getReceiverEmail(), receiverEmail)) {
            throw new PaymentTransactionException("Paypal receiver email: " + paypalTransaction.getReceiverEmail() + " does not match expected receiver email:" + receiverEmail);
        }
    }

    /**
     * Checks the given transaction was not already processed, and returns the
     * previous processed transaction.
     *
     * @return a local previous transaction, null if it does not exists.
     */
    private PaypalSubscriptionPayment checkPreviousTransaction(PaypalSubscriptionPayment paypalTransaction) throws PaymentTransactionException {
        PaypalSubscriptionPayment previousTransaction = paypalTransactionDao.findLastByTransactionId(paypalTransaction.getTransactionId());
        if (previousTransaction != null) {
            //ignore this transaction if it was already registered as Completed
            if (previousTransaction.isPaymentStatusCompleted()) {
                throw new PaymentTransactionException("Paypal transactionId: " + paypalTransaction.getTransactionId() + " was already processed as 'Completed', stored as PaypalTransaction.id=" + previousTransaction.getId());
            }
            //ignore this transaction if its already registered with the same status
            if (previousTransaction.getPaymentStatus().equals(paypalTransaction.getPaymentStatus())) {
                throw new PaymentTransactionException("Paypal transactionId: " + paypalTransaction.getTransactionId() + " was already processed with payment status '" + previousTransaction.getPaymentStatus() + "', stored as PaypalTransaction.id=" + previousTransaction.getId());
            }
        }
        return previousTransaction;
    }

    /**
     * Checks the Plan exists and returns it.
     *
     * @return the Plan for the given paypalTransaction.
     */
    private Plan checkPlan(PaypalSubscriptionPayment paypalTransaction) throws PaymentTransactionException {
        Plan plan = planDao.findByPaypalItemNumber(paypalTransaction.getItemNumber());
        if (plan == null) {
            throw new PaymentTransactionException("There is no Plan with this Paypal Item Number: " + paypalTransaction.getItemNumber());
        }
        return plan;
    }

    @Override
    public void processPayment(PaypalSubscriptionPayment paypalTransaction, Map<String, String[]> requestParams) throws PaymentTransactionException {
        String status = findTransactionStatusFromPaypal(requestParams);

        try {
            checkStatusFromPaypal(status);
            checkHasValidValues(paypalTransaction);
            checkReceiverEmail(paypalTransaction);
            PaypalSubscriptionPayment previousTransaction = checkPreviousTransaction(paypalTransaction);
            Plan plan = checkPlan(paypalTransaction);

            //TODO: check that paymentAmount/paymentCurrency are correct

            logTransaction(paypalTransaction, status);

            //create user subscription
            long subscriptionId;
            if (previousTransaction == null) {
                //no previous transaction registered, add a new subscription
                subscriptionId = addSubscriptionToUser(paypalTransaction, plan);
            } else {
                subscriptionId = previousTransaction.getSubscriptionId();
            }

            //save paypal transaction
            paypalTransaction.setSubscriptionId(subscriptionId);
            paypalTransactionDao.save(paypalTransaction);

        } catch (PaymentTransactionException ex) {
            logTransaction(paypalTransaction, status, ex);
            throw ex;
        } catch (Exception ex) {
            logTransaction(paypalTransaction, status, ex);
            throw new PaymentTransactionException("Unknown error while processing Paypal transaction.", ex);
        }
    }

    /**
     * Contacts Paypal validation system and obtains the transaction status.
     *
     * @param requestParams the parameters to send to Paypal.
     * @return the transaction status obtained from Paypal.
     * @throws PaymentTransactionException if the transaction is not valid.
     */
    private String findTransactionStatusFromPaypal(Map<String, String[]> requestParams) throws PaymentTransactionException {
        String status = "";
        try {
            // Prepare the response to Paypal
            String responseToPaypal = "cmd=_notify-validate";
            Set<String> paramNames = requestParams.keySet();
            for (String paramName : paramNames) {
                String paramValue = requestParams.get(paramName)[0];
                responseToPaypal = responseToPaypal + "&" + paramName + "=" + URLEncoder.encode(paramValue, "UTF-8");
            }

            // post back to PayPal system to validate
            URLConnection urlConnection = validationUrl.openConnection();
            urlConnection.setDoOutput(true);
            urlConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");

            try (PrintWriter writer = new PrintWriter(urlConnection.getOutputStream())) {
                writer.println(responseToPaypal);
            }

            try (BufferedReader in = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()))) {
                status = in.readLine();
            }

        } catch (MalformedURLException mue) {
            throw new PaymentTransactionException("Paypal validation system malformed URL", mue);
        } catch (IOException ie) {
            throw new PaymentTransactionException("Error contacting Paypal validation system.", ie);
        }

        return status;
    }

    private String toString(String status, PaypalSubscriptionPayment paypalTransaction) {
        StringBuilder msg = new StringBuilder();
        msg.append("====== Paypal Response: ").append(status).append(" ======\n");
        msg.append("Item name: ").append(paypalTransaction.getItemName()).append("\n");
        msg.append("Item number: ").append(paypalTransaction.getItemNumber()).append("\n");
        msg.append("Payment status: ").append(paypalTransaction.getPaymentStatus()).append("\n");
        msg.append("Payment amount: ").append(paypalTransaction.getPaymentAmount()).append("\n");
        msg.append("Payment currency:").append(paypalTransaction.getPaymentCurrency()).append("\n");
        msg.append("TX id: ").append(paypalTransaction.getTransactionId()).append("\n");
        msg.append("Receiver email: ").append(paypalTransaction.getReceiverEmail()).append("\n");
        msg.append("Payer email: ").append(paypalTransaction.getPayerEmail()).append("\n");
        msg.append("Custom value: ").append(paypalTransaction.getCustomValue()).append("\n");
        return msg.toString();
    }

    /**
     * Logs the paypal transaction.
     */
    private void logTransaction(PaypalSubscriptionPayment paypalTransaction, String status) {
        if (log.isDebugEnabled()) {
            log.debug(toString(status, paypalTransaction));
        }
    }

    /**
     * Logs the paypal transaction and the error it caused.
     */
    private void logTransaction(PaypalSubscriptionPayment paypalTransaction, String status, Throwable cause) {
        log.error(toString(status, paypalTransaction), cause);
    }

    /**
     * Creates a new subscription for the given user.
     */
    private long addSubscriptionToUser(PaypalSubscriptionPayment paypalTransaction, Plan plan) throws PaymentTransactionException {
        String username = paypalTransaction.getUsernameFromCustomValue();
        PrivateUser user = privateUserDao.findByUsername(username);
        if (user == null) {
            throw new PaymentTransactionException("Username from custom paypal value was not found: " + username);
        }
        Subscription subscription = subscriptionService.addSubscriptionToUser(plan.getId(), user.getId());
        return subscription.getId();
    }
}
