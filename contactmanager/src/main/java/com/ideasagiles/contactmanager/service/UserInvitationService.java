/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.contactmanager.service;

import com.ideasagiles.contactmanager.domain.User;
import com.ideasagiles.contactmanager.domain.UserInvitation;

/**
 *
 * @author gianu
 */
public interface UserInvitationService {
     /**
      * Send invitations to the users on the list.
      * If the user already exist on the list, he will be ignored.
      * If any of the email provided is no well-formed, it will be discarded.
      *
      * @param user The user sending the invitation.
      * @param invitations an array with email addressess.
      */
     void sendInvitations(String[] invitations);

     /**
      * Return an UserInvitation by a given token
      *
      * @param token the token of the userInvitation
      * @return the UserInvitation object, null if none was found.
      */
     UserInvitation findInvitationByToken(String token);

     /**
      * Accept an invitation and register the created user in the invitation object.
      *
      * @param token the token of the userInvitation
      * @param user The new Created user.
      */
     void acceptInvitation(String token, User user);
}
