/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.contactmanager.service;

import com.ideasagiles.contactmanager.dao.PrivateUserDao;
import com.ideasagiles.contactmanager.dao.ReferralDao;
import com.ideasagiles.contactmanager.domain.Plan;
import com.ideasagiles.contactmanager.domain.PrivateUser;
import com.ideasagiles.contactmanager.domain.Subscription;
import com.ideasagiles.contactmanager.vo.ReferralInfoVo;
import java.util.Random;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class ReferralServiceImpl implements ReferralService {

    private static final int REFERRALS_NEEDED_FOR_LEVEL_1 = 1;
    private static final int REFERRALS_NEEDED_FOR_LEVEL_2 = 5;
    private static final int REFERRALS_NEEDED_FOR_LEVEL_3 = 10;
    private static final int REFERRALS_NEEDED_FOR_LEVEL_4 = 20;


    /** The generated referral code length.  */
    private static final int REFERRAL_CODE_LENGTH = 6;

    /** Valid chars for a referral code. */
    private static final String[] CHARS = new String[]{"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d", "e", "f"};

    @Autowired
    private SubscriptionService subscriptionService;
    @Autowired
    private PlanService planService;
    @Autowired
    private ReferralDao referralDao;
    @Autowired
    private PrivateUserDao privateUserDao;

    @Override
    public String createReferralCode(long userId) {
        Random random = new Random();
        StringBuilder referralCode = new StringBuilder();
        referralCode.append(Long.toString(userId));
        while (referralCode.length() < REFERRAL_CODE_LENGTH) {
            int pos = random.nextInt(CHARS.length);
            referralCode.append(CHARS[pos]);
        }
        return referralCode.toString();
    }

    @PreAuthorize("isAuthenticated()")
    @Override
    public ReferralInfoVo findReferralInfoByUser(long userId) {
        Subscription subscription = subscriptionService.findActiveSubscriptionByUser(userId);
        if (subscription == null) {
            //user does not exist
            return null;
        }

        PrivateUser user = privateUserDao.findById(userId);

        ReferralInfoVo referralInfo = new ReferralInfoVo();
        referralInfo.setActivePlan(subscription.getPlan());
        referralInfo.setReferred(user.getReferrer() != null);

        long referralCount = referralDao.countReferrals(userId);
        referralInfo.setReferralsLeftForLevel1(referralsLeftForLevel(referralCount, REFERRALS_NEEDED_FOR_LEVEL_1));
        referralInfo.setReferralsLeftForLevel2(referralsLeftForLevel(referralCount, REFERRALS_NEEDED_FOR_LEVEL_2));
        referralInfo.setReferralsLeftForLevel3(referralsLeftForLevel(referralCount, REFERRALS_NEEDED_FOR_LEVEL_3));
        referralInfo.setReferralsLeftForLevel4(referralsLeftForLevel(referralCount, REFERRALS_NEEDED_FOR_LEVEL_4));

        return referralInfo;
    }

    /** Calculates how many referrals are left to achieve the needed ammount.
     * @return the referrals left amount, or zero if no more referrals are needed.
     */
    private long referralsLeftForLevel(long currentCount, int countNeeded) {
        long referralsLeft = countNeeded - currentCount;
        if (referralsLeft < 0) {
            referralsLeft = 0;
        }
        return referralsLeft;
    }

    @PreAuthorize("isAuthenticated()")
    @Override
    public void updateReferrals(long userId) {
        Subscription subscription = subscriptionService.findActiveSubscriptionByUser(userId);
        if (subscription == null) {
            return;
        }

        long referralCount = referralDao.countReferrals(userId);

        if (referralCount >= REFERRALS_NEEDED_FOR_LEVEL_4) {
            upgradeUserPlan(userId, subscription, planService.findLevelFourPlan());

        } else if (referralCount >= REFERRALS_NEEDED_FOR_LEVEL_3) {
            upgradeUserPlan(userId, subscription, planService.findLevelThreePlan());

        } else if (referralCount >= REFERRALS_NEEDED_FOR_LEVEL_2) {
            upgradeUserPlan(userId, subscription, planService.findLevelTwoPlan());

        } else if (referralCount >= REFERRALS_NEEDED_FOR_LEVEL_1) {
            upgradeUserPlan(userId, subscription, planService.findLevelOnePlan());
        }

    }

    private void upgradeUserPlan(long userId, Subscription currentSubscription, Plan plan) {
        if (!currentSubscription.getPlan().getId().equals(plan.getId())) {
            // Only upgrade plan if is a different one.
            subscriptionService.addSubscriptionToUser(plan.getId(), userId);
        }
    }

    @PreAuthorize("isAuthenticated()")
    @Override
    public void associateReferral(long referralUserId, long referredUserId) {
        PrivateUser referralUser = privateUserDao.findById(referralUserId);
        PrivateUser referredUser = privateUserDao.findById(referredUserId);

        // If one of the users is not found, do nothing.
        if (referralUser == null || referredUser == null) { return; }

        // If the referredUser was previously associated with another user, do nothing.
        if (referredUser.getReferrer() != null) { return; }

        referredUser.setReferrer(referralUser.toUser());
        Subscription subscription = subscriptionService.findActiveSubscriptionByUser(referredUserId);
        upgradeUserPlan(referredUserId, subscription, planService.findLevelOnePlan());
        privateUserDao.save(referredUser);
    }
}
