/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.contactmanager.service.io;

import com.ideasagiles.contactmanager.dao.ContactDao;
import com.ideasagiles.contactmanager.domain.Contact;
import java.io.IOException;
import java.util.Collection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author ldeseta
 */
public abstract class ContactImportServiceImpl {

    @Autowired
    private ContactDao contactDao;

    public void setContactDao(ContactDao contactDao) {
        this.contactDao = contactDao;
    }

    /**
     * Retrieves contacts information from external data, serialized using some
     * format.
     *
     * @param rawContacts serialized contacts in some format.
     * @return a Collection of Contacts.
     */
    protected abstract Collection<Contact> contactsFromExternalData(String rawContacts) throws IOException;

    /**
     * Save the contacts provided in the csv file.
     *
     * @param rawContacts serialized contacts in some format.
     * @param options options to save the contact
     *
     * @return a Collection of saved Contacts.
     */
    @PreAuthorize("isAuthenticated()")
    @Transactional
    public Collection<Contact> importContacts(String rawContacts, ContactImporterOptions options) throws IOException {
        Collection<Contact> contactsToSave = contactsFromExternalData(rawContacts);

        for (Contact contact : contactsToSave) {
            contact.setAddressBook(options.getAddressBook());
            contact.setCreationTime(options.getCreationTime());
            contactDao.save(contact);
        }

        return contactsToSave;
    }
}
