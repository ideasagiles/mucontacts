/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.contactmanager.service;

import com.ideasagiles.contactmanager.dao.AddressBookDao;
import com.ideasagiles.contactmanager.dao.CommentDao;
import com.ideasagiles.contactmanager.dao.ContactDao;
import com.ideasagiles.contactmanager.dao.TagDao;
import com.ideasagiles.contactmanager.domain.*;
import com.ideasagiles.contactmanager.service.exception.SubscriptionLimitException;
import java.util.Collection;
import java.util.Date;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Leito
 */
@Service
@Transactional
public class ContactServiceImpl implements ContactService {

    @Autowired
    private AddressBookPermissionService addressBookPermissionService;
    @Autowired
    private SubscriptionService subscriptionService;
    @Autowired
    private ContactDao contactDao;
    @Autowired
    private AddressBookDao addressBookDao;
    @Autowired
    private CommentDao commentDao;
    @Autowired
    private TagDao tagDao;

    @PreAuthorize("hasPermission(#contact.addressBook, 'rw')")
    @Override
    public void save(Contact contact) {
        verifySubscriptionLimitToAddContactToUser(contact);

        contact.setCreationTime(new Date());
        contact.setLastModificationTime(null);
        completeTags(contact);
        contact.setStatistics(new ContactStatistics());

        contactDao.save(contact);
    }

    @PreAuthorize("hasPermission(#contact, 'rw') and hasPermission(#contact.addressBook, 'rw')")
    @Override
    public void update(Contact contact) {
        //only verify subscription limit if it changes owner.
        User originalOwner = addressBookDao.findById(contact.getAddressBook().getId()).getOwner();
        User newOwner = addressBookDao.findByContact(contact.getId()).getOwner();
        if (!originalOwner.getId().equals(newOwner.getId())) {
            verifySubscriptionLimitToAddContactToUser(contact);
        }

        contact.setLastModificationTime(new Date());
        completeTags(contact);

        Contact originalContact = contactDao.findByIdDetached(contact.getId());
        contact.setStatistics(originalContact.getStatistics());

        tagDao.deleteByContact(contact.getId());
        contactDao.update(contact);
    }

    @PreAuthorize("hasPermission(#id, 'Contact', 'r')")
    @Override
    public Contact findById(long id) {
        return contactDao.findById(id);
    }

    @PreAuthorize("isAuthenticated()")
    @Override
    public Collection<Contact> findAll() {
        Collection<AddressBookPermission> addressBookPermissions = addressBookPermissionService.getAddressBookPermissionsForLoggedInUser();
        return contactDao.findAll(addressBookPermissions);
    }

    @PreAuthorize("hasPermission(#id, 'Contact', 'rw')")
    @Override
    public void delete(long id) {
        Contact contact = contactDao.findById(id);
        commentDao.deleteByContact(id);
        contactDao.delete(contact);
    }

    /**
     * Completes the tag information for this contact, for saving.
     */
    private void completeTags(Contact contact) {
        Collection<Tag> tags = contact.getTags();
        if (tags != null) {
            for (Tag tag : tags) {
                tag.setId(null);
                tag.setContact(contact);
            }
        }
    }


    /**
     * Verifies that the addressbook's owner of this contact has not reached his
     * subscription limit.
     *
     * @param contact the contact to verify.
     */
    private void verifySubscriptionLimitToAddContactToUser(Contact contact) {
        AddressBook addressBook = addressBookDao.findById(contact.getAddressBook().getId());
        if (!subscriptionService.canAddNewContactToUser(addressBook.getOwner().getId())) {
            throw new SubscriptionLimitException();
        }

        contact.setAddressBook(addressBook);
    }
}
