/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.contactmanager.service;

import com.ideasagiles.contactmanager.dao.MailDao;
import com.ideasagiles.contactmanager.dao.PlanDao;
import com.ideasagiles.contactmanager.dao.PrivateUserDao;
import com.ideasagiles.contactmanager.dao.SubscriptionDao;
import com.ideasagiles.contactmanager.domain.Plan;
import com.ideasagiles.contactmanager.domain.PrivateUser;
import com.ideasagiles.contactmanager.domain.Subscription;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author ldeseta
 */
@Service
@Transactional
public class SubscriptionServiceImpl implements SubscriptionService {

    private static Logger log = LoggerFactory.getLogger(SubscriptionServiceImpl.class);

    @Autowired
    private SubscriptionDao subscriptionDao;
    @Autowired
    private PrivateUserService privateUserService;
    @Autowired
    private PrivateUserDao privateUserDao;
    @Autowired
    private PlanDao planDao;
    @Autowired
    private MailDao mailDao;

    @PreAuthorize("isAuthenticated()")
    @Override
    public boolean canAddNewContactToUser(long userId) {
        Subscription subscription = subscriptionDao.findActiveSubscriptionByUser(userId, new Date());
        long count = subscriptionDao.countContactsByOwner(userId);
        Plan plan = subscription.getPlan();

        if (plan.getMaxContacts() == null || plan.getMaxContacts() > count) {
            return true;
        }

        return false;
    }

    @PreAuthorize("isAuthenticated()")
    @Override
    public boolean canAddNewAddressBookToUser(long userId) {
        Subscription subscription = subscriptionDao.findActiveSubscriptionByUser(userId, new Date());
        long count = subscriptionDao.countAddressBooksByOwner(userId);
        Plan plan = subscription.getPlan();

        if (plan.getMaxAddressBooks() == null || plan.getMaxAddressBooks() > count) {
            return true;
        }

        return false;
    }

    @PreAuthorize("isAuthenticated()")
    @Override
    public boolean canUpgradeToCollaborator(long userId) {
        Subscription subscription = subscriptionDao.findActiveSubscriptionByUser(userId, new Date());
        long count = subscriptionDao.countAddressBookPermissionsByUser(userId, "rw");
        count += subscriptionDao.countAddressBookPermissionsByUser(userId, "rwi");
        Plan plan = subscription.getPlan();

        if (plan.getMaxWriteInvitations() == null || plan.getMaxWriteInvitations() > count) {
            return true;
        }

        return false;
    }

    @Override
    public Subscription addSubscriptionToUser(long planId, long userId) {

        PrivateUser user = privateUserDao.findById(userId);
        if (user == null) {
            throw new IllegalArgumentException("userId was not found: " + userId);
        }
        Plan plan = planDao.findById(planId);
        if (plan == null) {
            throw new IllegalArgumentException("planId was not found: " + planId);
        }

        Subscription lastSubscription = subscriptionDao.findLastSubscriptionByUserPlan(planId, userId);

        Date now = new Date();
        Date since;
        if (lastSubscription == null || lastSubscription.getValidThrough() == null) {
            //there is no last subscription, or it does not expire, so start from today.
            since = now;
        }
        else {
            if (lastSubscription.getValidThrough().before(now)) {
                //last subscription is old, start from today
                since = now;
            }
            else {
                //last subscription is still valid, start new after that
                since = lastSubscription.getValidThrough();
            }
        }

        Date validThrough;
        if (plan.getDurationInMonths() != null) {
            //the plan has a month limit
            Calendar calendar = new GregorianCalendar();
            calendar.setTime(since);
            calendar.add(Calendar.MONTH, plan.getDurationInMonths());
            validThrough = calendar.getTime();
        }
        else {
            //the plan has no time limit
            validThrough = null;
        }

        Subscription subscription = new Subscription();
        subscription.setPlan(plan);
        subscription.setAmount(plan.getAmount());
        subscription.setUser(user.toUser());
        subscription.setSince(since);
        subscription.setValidThrough(validThrough);

        subscriptionDao.save(subscription);

        logSubscription(subscription);

        return subscription;
    }

    @PreAuthorize("isAuthenticated()")
    @Override
    public Subscription findActiveSubscriptionByUser(long userId) {
        return subscriptionDao.findActiveSubscriptionByUser(userId, new Date());
    }

    @PreAuthorize("isAuthenticated()")
    @Override
    public void cancelSubscription() {
        PrivateUser user = privateUserService.getLoggedInUser();
        String paypalUsername = subscriptionDao.findPaypalUsernameByUsername(user.getUsername());
        mailDao.sendSubscriptionCancel(user, paypalUsername);
    }

    private String toString(Subscription subscription) {
        StringBuilder msg = new StringBuilder();
        msg.append("====== New subscription created ======\n");
        msg.append("Subscripton Id: ").append(subscription.getId()).append("\n");
        msg.append("User Id: ").append(subscription.getUser().getId()).append("\n");
        msg.append("Username: ").append(subscription.getUser().getUsername()).append("\n");
        msg.append("Since:").append(subscription.getSince()).append("\n");
        msg.append("Valid Through: ").append(subscription.getValidThrough()).append("\n");
        msg.append("Amount: ").append(subscription.getAmount()).append("\n");
        msg.append("Plan Id: ").append(subscription.getPlan().getId()).append("\n");
        msg.append("Plan Name: ").append(subscription.getPlan().getName()).append("\n");
        msg.append("Plan Ammount: ").append(subscription.getPlan().getAmount()).append("\n");
        msg.append("Plan duration in months: ").append(subscription.getPlan().getDurationInMonths()).append("\n");
        msg.append("Plan Paypal item number: ").append(subscription.getPlan().getPaypalItemNumber()).append("\n");
        return msg.toString();
    }

    private void logSubscription(Subscription subscription) {
        if (log.isDebugEnabled()) {
            log.debug(toString(subscription));
        }
    }
}
