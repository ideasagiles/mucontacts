/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.contactmanager.service;

import com.ideasagiles.contactmanager.vo.TagVo;
import java.util.Collection;

/**
 * Services for Tags.
 */
public interface TagService {

    /**
     * Searches all used tags for the logged in user.
     * @return a Collection of Tags, empty if no tags were found.
     */
    Collection<TagVo> findAvailableTagsForLoggedInUser();
}
