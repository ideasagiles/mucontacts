/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.contactmanager.service;

import com.ideasagiles.contactmanager.vo.ReferralInfoVo;

/**
 *
 * @author Leito
 */
public interface ReferralService {
    /** Creates an unique and random referral code for the given userId.
     * Every time this method is called a different code may be returned.
     *
     * @param userId the id of an User.
     * @return an unique and random referral code.
     */
    String createReferralCode(long userId);

    /** Finds general information about the referral status of an user.
     * @userId the id of an user.
     * @return general information for the given user.
     */
    ReferralInfoVo findReferralInfoByUser(long userId);

    /**
     * Update, if applicable, the current plan of the referrer user.
     *
     * @param referrer The user referring another user
     */
    void updateReferrals(long userId);

    /**
     * Associate the referral user with the referred user
     *
     * @param referralUserId the Id of the user who send the referral
     * @param referredUserId the Id of the user referred
     */
    void associateReferral(long referralUserId, long referredUserId);

}
