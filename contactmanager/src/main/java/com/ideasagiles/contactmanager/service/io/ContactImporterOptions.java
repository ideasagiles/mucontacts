/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package com.ideasagiles.contactmanager.service.io;

import com.ideasagiles.contactmanager.domain.AddressBook;
import com.ideasagiles.contactmanager.domain.User;
import java.util.Date;

/**
 * Contact Importer Options.
 *
 * @author gianu
 */
public class ContactImporterOptions {
    private User owner;
    private AddressBook addressBook;
    private Date creationTime;

    public User getOwner() {
        return owner;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }

    public AddressBook getAddressBook() {
        return addressBook;
    }

    public void setAddressBook(AddressBook addressBook) {
        this.addressBook = addressBook;
    }

    public Date getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(Date creationTime) {
        this.creationTime = creationTime;
    }
}
