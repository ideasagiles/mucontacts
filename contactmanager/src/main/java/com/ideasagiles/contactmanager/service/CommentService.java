/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.contactmanager.service;

import com.ideasagiles.contactmanager.domain.Comment;
import java.util.Collection;

/**
 * Services for Comment domain model object.
 * A Comment represent an user opinion on a Contact.
 *
 * @author ldeseta
 */
public interface CommentService {

    /** Searches for all comments of a given contact.
     *
     * @param contactId the unique identifier of a contact.
     * @return a Collection of all comments for the given contact, empty if the
     * has no comments or the contact does not exist.
     */
    Collection<Comment> findByContact(long contactId);

    /** Creates a new comment.
     *
     * @param comment the comment to save.
     */
    void save(Comment comment);

}
