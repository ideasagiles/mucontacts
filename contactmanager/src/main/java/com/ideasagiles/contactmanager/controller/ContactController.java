/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.contactmanager.controller;

import com.ideasagiles.contactmanager.domain.Contact;
import com.ideasagiles.contactmanager.service.ContactService;
import java.util.Collection;
import javax.validation.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * This is the main Contact service class for the presentation, and handles
 * everything related with web data transformation.
 *
 * @author ldeseta
 */
@Controller
@RequestMapping(value="/contact")
public class ContactController {

    @Autowired
    private ContactService contactService;

    @ExceptionHandler(value=ConstraintViolationException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public void handleValidationExceptions() { }


    /** Creates a new Contact.
     * @param contact the contact to create.
     * @return a Map with the new created id.
     */
    @RequestMapping(value="/create")
    public @ResponseBody Contact create(@RequestBody Contact contact) {
        contactService.save(contact);
        return contact;
    }

    /** Updates the Contact.
     * @param contact the contact to update.
     * @return a Map with the new version of the contact.
     */
    @RequestMapping(value="/update")
    public @ResponseBody Contact update(@RequestBody Contact contact) {
        contactService.update(contact);
        return contact;
    }

    @RequestMapping(value="/all")
    public @ResponseBody Collection<Contact> findAll() {
        return contactService.findAll();
    }

    /** Deletes a user.
     *
     * @param id the id to delete.
     * @return the id that was deleted. If this method returns void, there is
     * a problem in Javascript reading the return value.
     */
    @RequestMapping(value="/delete/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable long id) {
        contactService.delete(id);
    }

}
