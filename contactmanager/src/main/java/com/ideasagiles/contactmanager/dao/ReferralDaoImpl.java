/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.contactmanager.dao;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author gianu
 */
@Repository
public class ReferralDaoImpl implements ReferralDao {

    private static final String COUNT_REFERRALS_QUERY = "select count(*) from PrivateUser u where u.referrer.id = :userId";
    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public long countReferrals(long id) {
        Query query = sessionFactory.getCurrentSession().createQuery(COUNT_REFERRALS_QUERY);
        query.setLong("userId", id);
        return (long) query.uniqueResult();
    }
}
