/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.contactmanager.dao;

import com.ideasagiles.contactmanager.domain.AddressBook;
import com.ideasagiles.contactmanager.domain.AddressBookInvitation;
import java.util.ArrayList;
import java.util.Collection;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Leito
 */
@Repository
public class AddressBookDaoImpl implements AddressBookDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public AddressBook findById(long id) {
        return (AddressBook) sessionFactory.getCurrentSession().get(AddressBook.class, id);
    }

    @Override
    public AddressBook findByContact(long contactId) {
        return (AddressBook) sessionFactory.getCurrentSession()
                .createQuery("select c.addressBook from Contact c where c.id = :id")
                .setLong("id", contactId)
                .uniqueResult();
    }

    @Override
    public void save(AddressBook addressBook) {
        sessionFactory.getCurrentSession().save(addressBook);
    }

    @Override
    public void update(AddressBook addressBook) {
        sessionFactory.getCurrentSession().update(addressBook);
    }

    @Override
    public AddressBook findByName(long userId, String name) {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(AddressBook.class);
        criteria.add(Restrictions.eq("owner.id", userId));
        criteria.add(Restrictions.ilike("name", name, MatchMode.EXACT));

        return (AddressBook) criteria.uniqueResult();
    }

    @Override
    public void save(AddressBookInvitation addressBookInvitation) {
        sessionFactory.getCurrentSession().save(addressBookInvitation);
    }

    @Override
    public AddressBookInvitation findInvitationByEmail(long addressBookId, String invitedUserEmail) {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(AddressBookInvitation.class);
        criteria.add(Restrictions.eq("addressBook.id", addressBookId));
        criteria.add(Restrictions.ilike("invitedUserEmail", invitedUserEmail, MatchMode.EXACT));
        return (AddressBookInvitation) criteria.uniqueResult();
    }

    @Override
    public Collection<AddressBookInvitation> findInvitationByEmail(String invitedUserEmail) {
        if (StringUtils.isBlank(invitedUserEmail)) {
            return new ArrayList();
        }
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(AddressBookInvitation.class);
        criteria.add(Restrictions.ilike("invitedUserEmail", invitedUserEmail, MatchMode.EXACT));
        return criteria.list();
    }

    @Override
    public AddressBookInvitation findInvitationById(long invitationId) {
        AddressBookInvitation invitation = (AddressBookInvitation) sessionFactory.getCurrentSession().get(AddressBookInvitation.class, invitationId);
        return invitation;
    }

    @Override
    public void delete(AddressBookInvitation addressBookInvitation) {
        sessionFactory.getCurrentSession().delete(addressBookInvitation);
        sessionFactory.getCurrentSession().flush();
    }

    @Override
    public Collection<AddressBookInvitation> findInvitationByAddressBook(long addressBookId) {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(AddressBookInvitation.class);
        criteria.add(Restrictions.eq("addressBook.id", addressBookId));
        return criteria.list();
    }
}
