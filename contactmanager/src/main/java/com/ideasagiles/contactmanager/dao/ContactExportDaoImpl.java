/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.contactmanager.dao;

import com.ideasagiles.contactmanager.domain.Contact;
import com.ideasagiles.contactmanager.domain.Tag;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.Arrays;
import java.util.Collection;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Repository;
import org.supercsv.io.CsvListWriter;
import org.supercsv.io.ICsvListWriter;
import org.supercsv.prefs.CsvPreference;

/**
 *
 * @author ldeseta
 */
@Repository
public class ContactExportDaoImpl implements ContactExportDao {

    @Override
    public void exportContactsToCSV(Collection<Contact> contacts, OutputStream out) throws IOException {
        try (BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(out));
                ICsvListWriter csvWriter = new CsvListWriter(writer, CsvPreference.STANDARD_PREFERENCE);) {

            String[] header = new String[]{
                "Name", "Short Description", "Description", "Address", "Email", "Phone", "Website", "Organization", "Tags"
            };

            csvWriter.writeHeader(header);

            for (Contact contact : contacts) {
                String[] line = new String[9];
                line[0] = StringUtils.defaultString(contact.getName());
                line[1] = StringUtils.defaultString(contact.getShortDescription());
                line[2] = StringUtils.defaultString(contact.getDescription());
                line[3] = StringUtils.defaultString(contact.getAddress());
                line[4] = StringUtils.defaultString(contact.getEmail());
                line[5] = StringUtils.defaultString(contact.getPhone());
                line[6] = StringUtils.defaultString(contact.getWebsite());
                line[7] = StringUtils.defaultString(contact.getOrganization());
                line[8] = getTagsAsString(contact.getTags());

                csvWriter.write(Arrays.asList(line));
            }
        }
    }

    private String getTagsAsString(Collection<Tag> tags) {
        StringBuilder tagString = new StringBuilder();
        for (Tag tag : tags) {
            tagString.append(StringUtils.defaultString(tag.getName()));
            tagString.append(",");
        }
        return tagString.toString();
    }
}
