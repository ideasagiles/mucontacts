/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.contactmanager.dao;

import com.ideasagiles.contactmanager.vo.TagVo;
import java.util.Collection;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Leito
 */
@Repository
public class TagDaoImpl implements TagDao {

    private static final String AVAILABLE_TAGS_QUERY = "select distinct new com.ideasagiles.contactmanager.vo.TagVo(t.name) from Tag t, AddressBookPermission p where t.contact.addressBook.id = p.addressBook.id and p.user.id = :userId order by t.name asc";
    private static final String DELETE_TAGS_BY_CONTACT_QUERY = "delete from Tag t where t.contact.id = :contactId";

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public Collection<TagVo> findAvailableTagsByUser(long userId) {
        Query query = sessionFactory.getCurrentSession().createQuery(AVAILABLE_TAGS_QUERY);
        query.setLong("userId", userId);
        return query.list();
    }

    @Override
    public int deleteByContact(long contactId) {
        Query query = sessionFactory.getCurrentSession().createQuery(DELETE_TAGS_BY_CONTACT_QUERY);
        query.setLong("contactId", contactId);
        return query.executeUpdate();
    }

}