/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.contactmanager.dao;

import com.ideasagiles.contactmanager.domain.AddressBookPermission;
import com.ideasagiles.contactmanager.domain.Contact;
import com.ideasagiles.contactmanager.domain.Emotion;
import java.util.Collection;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Leito
 */
@Repository
public class ContactDaoImpl implements ContactDao {

    private static final String INCREMENT_HAPPY_STATS_UPDATE = "update Contact c set c.statistics.happyEmotionCount = (c.statistics.happyEmotionCount + 1) where c.id = :id";
    private static final String INCREMENT_MAD_STATS_UPDATE = "update Contact c set c.statistics.madEmotionCount = (c.statistics.madEmotionCount + 1) where c.id = :id";
    private static final String INCREMENT_SAD_STATS_UPDATE = "update Contact c set c.statistics.sadEmotionCount = (c.statistics.sadEmotionCount + 1) where c.id = :id";
    private static final String INCREMENT_SCARED_STATS_UPDATE = "update Contact c set c.statistics.scaredEmotionCount = (c.statistics.scaredEmotionCount + 1) where c.id = :id";
    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public void save(Contact contact) {
        sessionFactory.getCurrentSession().save(contact);
    }

    @Override
    public void update(Contact contact) {
        sessionFactory.getCurrentSession().update(contact);
    }

    @Override
    public Contact findById(long id) {
        return (Contact) sessionFactory.getCurrentSession().get(Contact.class, id);
    }

    @Override
    public Contact findByIdDetached(long id) {
        Contact contact = findById(id);
        if (contact != null) {
            sessionFactory.getCurrentSession().evict(contact);
        }
        return contact;
    }

    @Override
    public Collection<Contact> findAll(Collection<AddressBookPermission> addressBookPermissions) {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Contact.class);

        Disjunction addressBookRestriction = Restrictions.disjunction();
        for (AddressBookPermission addressBookPermission : addressBookPermissions) {
            addressBookRestriction.add(Restrictions.eq("addressBook.id", addressBookPermission.getAddressBook().getId()));
        }
        criteria.add(addressBookRestriction);

        criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        criteria.addOrder(Order.asc("name"));
        return criteria.list();
    }

    @Override
    public void delete(Contact contact) {
        contact = (Contact) sessionFactory.getCurrentSession().get(Contact.class, contact.getId());
        sessionFactory.getCurrentSession().delete(contact);
    }

    @Override
    public void incrementEmotionStatistic(Emotion emotion, long contactId) {
        String updateQuery = getIncrementEmotionUpdate(emotion);

        int rows = sessionFactory.getCurrentSession().createQuery(updateQuery).setLong("id", contactId).executeUpdate();

        if (rows != 1) {
            throw new IllegalArgumentException("Expected 1 row affected, but was: " + rows);
        }
    }

    /**
     * Returns the increment emotion update HSQL for the given emotion.
     */
    private String getIncrementEmotionUpdate(Emotion emotion) {
        switch (emotion) {
            case HAPPY:
                return INCREMENT_HAPPY_STATS_UPDATE;
            case MAD:
                return INCREMENT_MAD_STATS_UPDATE;
            case SAD:
                return INCREMENT_SAD_STATS_UPDATE;
            case SCARED:
                return INCREMENT_SCARED_STATS_UPDATE;
            default:
                throw new IllegalArgumentException("Unknown emotion: " + emotion);
        }
    }

}
