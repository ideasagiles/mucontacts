/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.contactmanager.dao;

/**
 *
 * @author gianu
 */
public interface ReferralDao {

    /**
     * Returns the referral count for a given user.
     *
     * @param id The Id of the user.
     * @return the total count of referrals.
     */
    long countReferrals(long id);

}
