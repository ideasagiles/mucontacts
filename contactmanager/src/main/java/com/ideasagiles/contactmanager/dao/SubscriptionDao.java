/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.contactmanager.dao;

import com.ideasagiles.contactmanager.domain.Subscription;
import java.util.Date;

/**
 * Data access methods for Subscription.
 */
public interface SubscriptionDao {

    /** Saves a subscription.
     *
     * @param subscription the Subscription to save.
     */
    void save(Subscription subscription);

    /** Counts how many contacts a user has in all the address books he owns.
     *
     * @param userId the unique identifier of a user.
     * @return the contacts count, zero if the user does not exist or has no contacts yet.
     */
    long countContactsByOwner(long userId);

    /** Counts how many AddressBooks a user has.
     *
     * @param userId the unique identifier of a user.
     * @return the addressbooks count, zero if the user does not exist or has no addressbooks yet.
     */
    long countAddressBooksByOwner(long userId);

    /** Count how many AddressBookPermissions an user has with the given permission.
     *
     * @param userId the unique identifier of a user.
     * @param permission the permission to look for ("r", "rw", etc.).
     * @return the AddressBookPermission count, zero if the user does not exist.
     */
    long countAddressBookPermissionsByUser(long userId, String permission);

    /** Finds the active subscription of the given user. This returns the
     * highests priority subscription the user has. .
     *
     * @param userId the unique identifier of a user.
     * @param date the date the evaluate the active subscription.
     * @return the highest priority Subscription of the given user, or null if none is found.
     */
    Subscription findActiveSubscriptionByUser(long userId, Date date);

    /**
     * Finds the last (highest validThrough field) Subscription for the given
     * plan and user.
     *
     * @param planId The Id of the plan.
     * @param userId The Id of the user.
     * @return a Subscription which is the last subscription for the given plan/user
     * (highest validThrough field), or null if no subscription matches the criteria.
     */
    Subscription findLastSubscriptionByUserPlan(long planId, long userId);

    /**
     * Finds the Paypal username for a given user.
     *
     * @param username The Username of the user.
     * @return the Paypal Username
     */
    String findPaypalUsernameByUsername(String username);
}
