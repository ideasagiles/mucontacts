/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.contactmanager.dao;

import com.ideasagiles.contactmanager.domain.PasswordRecoveryToken;
import com.ideasagiles.contactmanager.domain.PrivateUser;
import com.ideasagiles.contactmanager.domain.User;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author gianu
 */
@Repository("PrivateUserDaoImpl")
public class PrivateUserDaoImpl implements PrivateUserDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public void save(PrivateUser user) {
        sessionFactory.getCurrentSession().save(user);
        sessionFactory.getCurrentSession().flush();
    }

    @Override
    public PrivateUser findByEmail(String email) {
        if (StringUtils.isBlank(email)) {
            return null;
        }
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(PrivateUser.class);
        criteria.add(Restrictions.ilike("email", email, MatchMode.EXACT));
        return (PrivateUser) criteria.uniqueResult();
    }

    @Override
    public PrivateUser findByUsername(String username) {
        if (StringUtils.isBlank(username)) {
            return null;
        }
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(PrivateUser.class);
        criteria.add(Restrictions.ilike("username", username, MatchMode.EXACT));
        return (PrivateUser) criteria.uniqueResult();
    }

    @Override
    public PrivateUser findById(long id) {
        return (PrivateUser) sessionFactory.getCurrentSession().get(PrivateUser.class, id);
    }

    @Override
    public void update(PrivateUser user) {
        sessionFactory.getCurrentSession().update(user);
    }

    @Override
    public User findByToken(String token) {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(PasswordRecoveryToken.class);
        criteria.add(Restrictions.eq("token", token));

        PasswordRecoveryToken userToken = (PasswordRecoveryToken) criteria.uniqueResult();
        if (userToken == null) { return null;}

        return userToken.getUser().toUser();
    }

    @Override
    public User findByReferralCode(String referralCode) {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(User.class);
        criteria.add(Restrictions.eq("referralCode", referralCode));
        return (User) criteria.uniqueResult();
    }
}
