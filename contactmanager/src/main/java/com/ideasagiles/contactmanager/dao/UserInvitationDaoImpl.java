/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.contactmanager.dao;

import com.ideasagiles.contactmanager.domain.UserInvitation;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author gianu
 */
@Repository("UserInvitationDaoImpl")
public class UserInvitationDaoImpl implements UserInvitationDao {
    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public void save(UserInvitation userInvitation) {
        sessionFactory.getCurrentSession().save(userInvitation);
    }

    @Override
    public UserInvitation findInvitationByEmail(String email) {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(UserInvitation.class);
        criteria.add(Restrictions.eq("email", email));

        return (UserInvitation) criteria.uniqueResult();
    }

    @Override
    public UserInvitation findInvitationByToken(String token) {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(UserInvitation.class);
        criteria.add(Restrictions.eq("token", token));

        return (UserInvitation) criteria.uniqueResult();
    }

    @Override
    public void update(UserInvitation userInvitation) {
        sessionFactory.getCurrentSession().update(userInvitation);
    }
}
