/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.contactmanager.dao;

import com.ideasagiles.contactmanager.domain.PaypalSubscriptionPayment;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Leito
 */
@Repository
public class PaypalTransactionDaoImpl implements PaypalTransactionDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public PaypalSubscriptionPayment findLastByTransactionId(String transactionId) {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(PaypalSubscriptionPayment.class);
        criteria.add(Restrictions.eq("transactionId", transactionId))
                .addOrder(Order.desc("id"))
                .setMaxResults(1);
        return (PaypalSubscriptionPayment) criteria.uniqueResult();
    }

    @Override
    public void save(PaypalSubscriptionPayment paypalTx) {
        sessionFactory.getCurrentSession().save(paypalTx);
    }

}
