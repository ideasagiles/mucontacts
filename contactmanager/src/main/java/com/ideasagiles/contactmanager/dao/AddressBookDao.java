/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.contactmanager.dao;

import com.ideasagiles.contactmanager.domain.AddressBook;
import com.ideasagiles.contactmanager.domain.AddressBookInvitation;
import java.util.Collection;

/**
 * DAO that manages the AddressBook domain object.
 */
public interface AddressBookDao {

    /** Finds the AddressBook with the given id.
     *
     * @param id the unique identifier of an AddressBook.
     * @return an AddressBook, or null if it does not exist.
     */
    AddressBook findById(long id);

    /** Finds the AddressBook of the given contact.
     *
     * @param contactId
     * @return the AddressBook of the given contact.
     */
    AddressBook findByContact(long contactId);

    /** Saves an AddressBook.
     *
     * @param addressBook the AddressBook to save.
     */
    void save(AddressBook addressBook);

    /**
     * Updates an AddressBook.
     * @param addressBook the AddressBook to update.
     */
    void update(AddressBook addressBook);

    /** Saves an AddressBookInvitation.
     *
     * @param addressBookInvitation the invitation to save
     */
    void save(AddressBookInvitation addressBookInvitation);

    /** Finds the AddressBook owned by the given user that has the given name.
     *
     * @param userId the owner of the AddressBook.
     * @param name the name of an AddressBook (the search is case insensitive).
     * @return an AddressBook, or null if there are no matches.
     */
    AddressBook findByName(long userId, String name);

    /** Finds an AddressBookInvitation for a given AddressBook and email.
     *
     * @param addressBookId the AddressBook to search the invitation.
     * @param invitedUserEmail the email of the invited user.
     * @return an AddressBookInvitation, or null if there are no matches.
     */
    AddressBookInvitation findInvitationByEmail(long addressBookId, String invitedUserEmail);

    /** Finds all AddressBookInvitation for the given email.
     *
     * @param invitedUserEmail the email of the invited user.
     * @return a Collection of AddressBookInvitation.
     */
    Collection<AddressBookInvitation> findInvitationByEmail(String invitedUserEmail);

    /**
     * Find the invitation for the given id
     *
     * @param invitationId the Id of the invitation
     * @return An AddressBookInvitation or a Null object if the invitationId is invalid.
     */
    AddressBookInvitation findInvitationById(long invitationId);

    /**
     * Delete the addressBookInvitation object
     *
     * @param addressBookInvitation the address book invitation to be deleted.
     */
    void delete(AddressBookInvitation addressBookInvitation);

    /**
     * Find the invitations sent for the given AddressBook.
     * @param addressBookId the unique identifier of the AddressBook.
     * @return a Collection of AddressBookInvitaiton.
     */
    Collection<AddressBookInvitation> findInvitationByAddressBook(long addressBookId);

}
