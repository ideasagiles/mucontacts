/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

/* Patch 1 for schema.
*
*  Changes:
*     - Added "relevance" column to address_books_permissions to allow ordering.
*/

alter table address_books_permissions add column relevance integer default 0 not null;


/* Patch 2 for schema.
*
*  Changes:
*     - Added "plans" table.
*     - Added "subscriptions" table.
*/

create table plans (
    id bigint auto_increment primary key,
    name varchar(100) not null,
    max_contacts int,
    max_address_books int,
    max_read_only_invitations int,
    max_write_invitations int
);

create table subscriptions (
    id bigint auto_increment primary key,
    since timestamp not null,
    valid_through timestamp,
    id_user bigint not null,
    id_plan bigint not null
);

alter table subscriptions add constraint fk_subscriptions_users foreign key(id_user) references users(id);
alter table subscriptions add constraint fk_subscriptions_plans foreign key(id_plan) references plans(id);


/*
 * Basic configuration.
 * The following statements are basic configuration needed by the application.
 */
insert into plans (id, name, max_contacts, max_address_books, max_read_only_invitations, max_write_invitations) values (1, 'Free default plan', 50, 1, 3, 0);
insert into plans (id, name, max_contacts, max_address_books, max_read_only_invitations, max_write_invitations) values (9000, 'Unlimited Plan', null, null, null, null);


/*
 * Test data statements.
 * The following statements are for test porpuses. This can be ommited.
 */
insert into plans (id, name, max_address_books, max_contacts, max_read_only_invitations, max_write_invitations) values (8000, 'Very Restricted Test Plan', 2, 2, 2, 2);

insert into subscriptions (id, since, valid_through, id_user, id_plan) values (1, NOW(), null, 1, 9000);
insert into subscriptions (id, since, valid_through, id_user, id_plan) values (2, NOW(), null, 2, 9000);
insert into subscriptions (id, since, valid_through, id_user, id_plan) values (3, NOW(), null, 3, 8000);



/* Patch 3 for schema.
*
*  Changes:
*     - Added "UserConnection" table, used by Spring Social (org.springframework.social.connect.jdbc.JdbcUsersConnectionRepository)
*     - Table "users" column "password" now allows nulls to allow external providers accounts.
*     - Table "users" column "email" has incresed size and now allows nulls.
*     - Added column "username" to table "users".
*     - Added column "authentication_provider" to table "users".
*/

create table UserConnection (userId varchar(255) not null,
	providerId varchar(255) not null,
	providerUserId varchar(255),
	rank int not null,
	displayName varchar(255),
	profileUrl varchar(512),
	imageUrl varchar(512),
	accessToken varchar(255) not null,
	secret varchar(255),
	refreshToken varchar(255),
	expireTime bigint,
	primary key (userId, providerId, providerUserId));
create unique index UserConnectionRank on UserConnection(userId, providerId, rank);

alter table users modify column password varchar(50) null;
alter table users modify column email varchar(255) null;
alter table users add column username varchar(50) unique;
alter table users add column authentication_provider varchar(15) not null;
update users set authentication_provider = 'ONCONTACTS';



/*
 * Test data statements.
 * The following statements are for test porpuses. This can be ommited.
 */
update users set username = 'gianu' where id = 1;
update users set username = 'leito' where id = 2;
update users set username = 'zim' where id = 3;


/* Patch 4 for schema.
*
*  Changes:
*     - Added column "organization" to table "contacts".
*/

alter table contacts add column organization varchar(255);



/* Patch 5 for schema.
*
*  Changes:
*     - Added column "amount" to table "plans".
*     - Added column "paypal_item_number" to table "plans".
*     - Added column "duration_in_months" to table "plans".
*     - Added column "priority" to table "plans".
*     - Added column "amount" to table "subscriptions".
*     - Added table "paypal_transactions".
*/

alter table plans add column amount numeric(6,2) not null;
update plans set amount = 0;

alter table plans add column paypal_item_number varchar(255) unique;

alter table plans add column duration_in_months tinyint;

alter table subscriptions add column amount numeric(6,2) not null;
update subscriptions set amount = 0;

alter table plans add column priority int not null;
update plans set priority = 0 where id = 1;
update plans set priority = 99999 where id = 9000;

create table paypal_subscription_payments (
    id bigint auto_increment primary key,
    transaction_id varchar(255),
    item_name varchar(255),
    item_number varchar(255),
    payment_status varchar(255),
    payment_amount numeric(6,2),
    payment_currency varchar(255),
    receiver_email varchar(255),
    payer_email varchar(255),
    custom_value varchar(255),
    date_received datetime not null,
    id_subscription bigint
);

alter table paypal_subscription_payments add constraint fk_paypal_subscription_payments_subscriptions foreign key(id_subscription) references subscriptions(id);

/* Replace timestamp datatype for datetime.
 * timestamp is limited to year 2038 and does not allow nulls (it defaults to current date).
*/
alter table address_books modify column date_created datetime not null;
alter table contacts modify column date_created datetime;
alter table contacts modify column date_modified datetime;
alter table comments modify column date_created datetime;
alter table users modify column member_since datetime not null;
alter table subscriptions modify column since datetime not null;
alter table subscriptions modify column valid_through datetime;

/* After changing timestamp to datetime, force all current subscriptions
 * valid_through to null, because it contains invalid data.
*/
update subscriptions set valid_through = null;


/* Patch 6 for schema.
*
*  Changes:
*     - changed timestamp datatype to datetime.
*     - added table user_invitations.
*/


/* Replace timestamp datatype for datetime.
 * timestamp is limited to year 2038 and does not allow nulls (it defaults to current date).
*/
alter table address_books_invitations modify column date_created datetime not null;
alter table password_recovery_tokens modify column creation_date datetime not null;

create table user_invitations (
    id bigint auto_increment primary key,
    id_inviter_user bigint not null,
    email varchar(255) not null,
    token varchar(50) not null,
    id_invited_user bigint,
    creation_time datetime not null,
    accepted_time datetime,
    constraint fk_user_invitations_inviter foreign key(id_inviter_user) references users(id),
    constraint fk_user_invitations_invited foreign key(id_invited_user) references users(id)
);



/* Patch 7 for schema.
*
*  Changes:
*     - Add Pro Plan to Plans Table
*/


insert into plans (id, name, max_contacts, max_address_books, max_read_only_invitations, max_write_invitations, amount, paypal_item_number, duration_in_months, priority) values (2, 'Pro Plan', 5000, 10, null, 5, 19, 'OC_PRO_PLAN   ', 1, 2);


/* Patch 8 for schema.
*
*  Changes:
*     - Add last_login column to table users
*/

alter table users add column last_sign_in datetime;



/* Patch 9 for schema.
*
*  Changes:
*     - Added column "happy_emotion_count" to table "contacts".
*     - Added column "mad_emotion_count" to table "contacts".
*     - Added column "sad_emotion_count" to table "contacts".
*     - Added column "scared_emotion_count" to table "contacts".
*     - Deleted table "emotions".
*     - Renamed column "id_emotion" to "emotion" in table "comments".
*     - Renamed users authentication_provider, from value "ONCONTACTS" to value "LOCAL".
*     - Renamed OC_PRO_PLAN to MC_PRO_PLAN.
*/

alter table contacts add column happy_emotion_count integer default 0;
alter table contacts add column mad_emotion_count integer default 0;
alter table contacts add column sad_emotion_count integer default 0;
alter table contacts add column scared_emotion_count integer default 0;


/* Update current statistics */
update contacts c set happy_emotion_count = (select count(*) from comments where id_emotion = 'happy' and id_contact = c.id);
update contacts c set mad_emotion_count = (select count(*) from comments where id_emotion = 'mad' and id_contact = c.id);
update contacts c set sad_emotion_count = (select count(*) from comments where id_emotion = 'sad' and id_contact = c.id);
update contacts c set scared_emotion_count = (select count(*) from comments where id_emotion = 'scared' and id_contact = c.id);


/* Remove table "emotions" */
alter table comments drop foreign key fk_comments_emotions;
alter table comments drop index fk_comments_emotions;
drop table emotions;

/* Emotions are now all uppercase (to match enum) */
update comments set id_emotion = UPPER(id_emotion);

/* rename id_emotion to emotion */
alter table comments change id_emotion emotion varchar(10) not null;

/* rename authentication provider */
update users set authentication_provider = 'LOCAL' where authentication_provider = 'ONCONTACTS';

/* Renamed OC_PRO_PLAN to PRO PLAN NAME */
update plans set paypal_item_number = 'MC_PRO_PLAN' where id = 2;



/* Patch 10 for schema.
*
*  Changes:
*     - Created index for column "transaction_id" on table "paypal_subscription_payments".
*     - Added column "payment_status_details" to table "paypal_subscription_payments".
*/

create index ix_paypal_subscription_payments_transaction_id on paypal_subscription_payments(transaction_id);

alter table paypal_subscription_payments add column payment_status_details varchar(255);


/* Patch 11 for schema.
*
*  Changes:
*     - Table "contacts_tags" dropped.
*     - Table "tags" refactored.
*/

alter table tags rename to tags_old;

create table tags (
    id bigint auto_increment primary key,
    id_contact bigint not null,
    name varchar(50) not null
);
create unique index ix_tags_name_id_contact on tags(name, id_contact);
alter table tags add constraint fk_tags_contacts foreign key(id_contact) references contacts(id);

insert into tags (id_contact, name) (select c.id_contact, t.name from contacts_tags c, tags_old t where c.id_tag = t.id);

drop table contacts_tags;
drop table tags_old;


/* Patch 12 for schema.
*
*  Changes:
*     - Updated plans limits.
*     - Added new plans for referrals.
*     - Added column referral_code to user.
*     - Added column referred_by_user_id.
*/

/* Free Plan */
update plans set max_contacts = 500 where id = 1;
update plans set max_read_only_invitations = null where id = 1;

/* Pro Plan */
update plans set max_contacts = 10000 where id = 2;
update plans set priority = 100 where id = 2;

/* new Always-Free plans for referrals */
insert into plans (id, name, amount, priority, max_contacts, max_address_books, max_write_invitations) values (3, 'Bronce Free plan',   0, 1, 1000, 1, 0);
insert into plans (id, name, amount, priority, max_contacts, max_address_books, max_write_invitations) values (4, 'Silver Free plan',   0, 2, 2000, 1, 1);
insert into plans (id, name, amount, priority, max_contacts, max_address_books, max_write_invitations) values (5, 'Gold Free plan',     0, 3, 5000, 1, 1);
insert into plans (id, name, amount, priority, max_contacts, max_address_books, max_write_invitations) values (6, 'Platinum Free plan', 0, 4, 7500, 2, 2);

/* Referral code for Users table */
alter table users add column referral_code varchar(20) unique;

alter table users add column referred_by_user_id bigint;
alter table users add constraint fk_users_referred_user foreign key(referred_by_user_id) references users(id);

update users set referral_code = concat(id, substring(MD5(id),1,5)) where id >= 0 and id < 10;
update users set referral_code = concat(id, substring(MD5(id),1,4)) where id >= 10 and id < 100;

