/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

/* Schema generation script for MySQL.
*  This script contains the following statements in order:
*     1) DROP statements.
*     2) CREATE statements.
*     3) INSERT statements with test data.
*/

/*
 * Create schema statement.
 *
 */
drop table if exists comments;
drop table if exists contacts_tags;
drop table if exists tags;
drop table if exists contacts;
drop table if exists address_books_invitations;
drop table if exists address_books_permissions;
drop table if exists address_books;
drop table if exists permissions;
drop table if exists emotions;
drop table if exists authorities;
drop table if exists password_recovery_tokens;
drop table if exists paypal_subscription_payments;
drop table if exists subscriptions;
drop table if exists plans;
drop table if exists user_invitations;
drop table if exists users;
drop table if exists UserConnection;

create table address_books (
    id bigint auto_increment primary key,
    name varchar(100) not null,
    date_created timestamp not null,
    id_owner_user bigint not null
);

create table address_books_invitations (
    id bigint auto_increment primary key,
    invited_user_email varchar(100) not null,
    id_address_book bigint not null,
    id_permission varchar(5) not null,
    sent_by_user bigint not null,
    date_created timestamp not null
);

create table address_books_permissions (
    id bigint auto_increment primary key,
    id_user bigint not null,
    id_address_book bigint not null,
    id_permission varchar(5) not null
);

create table permissions (
    id varchar(5) primary key
);

create table contacts (
    id bigint auto_increment primary key,
    name varchar(100) not null,
    short_description varchar(200),
    email varchar(100),
    phone varchar(100),
    website varchar(200),
    address varchar(200),
    description varchar(1000),
    date_created timestamp,
    date_modified timestamp,
    id_address_book bigint not null,
    version bigint default 0
);

create table comments (
    id bigint auto_increment primary key,
    text varchar(1000) not null,
    id_emotion varchar(10) not null,
    date_created timestamp,
    posted_by_user bigint not null,
    id_contact bigint not null
);

create table emotions (
    id varchar(10) primary key
);

create table contacts_tags (
    id_contact bigint,
    id_tag bigint
);

create table tags (
    id bigint auto_increment primary key,
    name varchar(50) unique not null
);

create table users (
      id bigint auto_increment primary key,
      email varchar(50) unique not null,
      password varchar(50) not null,
      enabled boolean not null,
      name varchar(100),
      member_since timestamp not null default CURRENT_TIMESTAMP,
      version numeric default 0
);

create table authorities (
      id bigint auto_increment primary key,
      userId bigint not null,
      authority varchar(50) not null,
      constraint fk_authorities_users foreign key(userId) references users(id)
);

create table password_recovery_tokens (
    id bigint auto_increment primary key,
    user_id bigint not null,
    token varchar(255) not null,
    creation_date timestamp default CURRENT_TIMESTAMP,
    version numeric default 0,
    constraint fk_password_recovery_tokens_users foreign key(user_id) references users(id)
);

create unique index ix_auth_email on authorities (userId,authority);

create unique index ix_address_books on address_books(id_owner_user,name);

create unique index ix_address_books_invitations on address_books_invitations(invited_user_email,id_address_book);
alter table address_books_invitations add constraint fk_address_books_invitations_address_books foreign key(id_address_book) references address_books(id);
alter table address_books_invitations add constraint fk_address_books_invitations_permissions foreign key(id_permission) references permissions(id);
alter table address_books_invitations add constraint fk_address_books_invitations_users foreign key(sent_by_user) references users(id);

create unique index ix_address_books_permissions on address_books_permissions(id_user,id_address_book);
alter table address_books_permissions add constraint fk_address_books_permissions_users foreign key(id_user) references users(id);
alter table address_books_permissions add constraint fk_address_books_permissions_permissions foreign key(id_permission) references permissions(id);

alter table comments add constraint fk_comments_emotions foreign key(id_emotion) references emotions(id);
alter table comments add constraint fk_comments_contacts foreign key(id_contact) references contacts(id);
alter table comments add constraint fk_comments_users foreign key(posted_by_user) references users(id);

alter table contacts add constraint fk_contacts_address_books foreign key(id_address_book) references address_books(id);

create unique index ix_contacts_tags on contacts_tags(id_contact,id_tag);
alter table contacts_tags add constraint fk_contacts_tags_contacts foreign key(id_contact) references contacts(id);
alter table contacts_tags add constraint fk_contacts_tags_tags foreign key(id_tag) references tags(id);



/*
 * Basic configuration.
 * The following statements are basic configuration needed by the application.
 *
 */
insert into emotions (id) values ('happy');
insert into emotions (id) values ('sad');
insert into emotions (id) values ('mad');
insert into emotions (id) values ('scared');

/** r: read, w: write, i: invite */
insert into permissions (id) values ('r');
insert into permissions (id) values ('rw');
insert into permissions (id) values ('rwi');


/*
 * Test data statements.
 * The following statements are for test porpuses. This can be ommited.
 *
 */
insert into users (id, email, password, name, enabled) values (1,'gianu@ideasagiles.com', MD5('gianu'), null, 1);
insert into users (id, email, password, name, enabled) values (2,'leito@ideasagiles.com', MD5('leito'), 'Leito', 1);
insert into users (id, email, password, name, enabled) values (3,'zim@ideasagiles.com', MD5('zim'), 'Invader Zim', 1);

insert into authorities (id, userId, authority) values (1, 1, 'USER');
insert into authorities (id, userId, authority) values (2, 2, 'USER');
insert into authorities (id, userId, authority) values (3, 3, 'USER');

insert into address_books (id, id_owner_user, name, date_created) values (1, 1, 'Gianu''s Address Book', NOW());
insert into address_books (id, id_owner_user, name, date_created) values (2, 2, 'Leito''s Address Book', NOW());
insert into address_books (id, id_owner_user, name, date_created) values (3, 3, 'World Domination Army', NOW());
insert into address_books (id, id_owner_user, name, date_created) values (4, 3, 'Friends from Earth', NOW());

insert into address_books_permissions (id, id_user, id_address_book, id_permission) values (1, 1, 1, 'rwi');
insert into address_books_permissions (id, id_user, id_address_book, id_permission) values (2, 2, 2, 'rwi');
insert into address_books_permissions (id, id_user, id_address_book, id_permission) values (3, 1, 2, 'rw');
insert into address_books_permissions (id, id_user, id_address_book, id_permission) values (4, 2, 1, 'rw');
insert into address_books_permissions (id, id_user, id_address_book, id_permission) values (5, 3, 3, 'rwi');
insert into address_books_permissions (id, id_user, id_address_book, id_permission) values (6, 3, 4, 'rwi');

insert into contacts(id, id_address_book, name, short_description, email) values (1, 1, 'Razputin "Raz" Aquato', 'The main character!', 'raz@ideasagiles.com');
insert into contacts(id, id_address_book, name, short_description, email) values (2, 1, 'Morceau Oleander', 'Old and grumpy, it has a military background', 'oleander@ideasagiles.com');
insert into contacts(id, id_address_book, name, short_description, email) values (3, 1, 'Sasha Nein', 'a weird kind of detective... X Files anyone?', 'sein222@gmail.com');
insert into contacts(id, id_address_book, name, short_description, email) values (4, 1, 'Milla Vodello', null, 'millavodello@yahoo.com');
insert into contacts(id, id_address_book, name, short_description, email) values (5, 1, 'Ford Cruller', 'i think it was Sasha''s partner','ford.cruller@ciudad.com.ar');
insert into contacts(id, id_address_book, name, short_description, email) values (6, 2, 'Lili Zanotto', null, 'lzanotto@gmail.com');
insert into contacts(id, id_address_book, name, short_description, email) values (7, 2, 'Dogen Boole', 'strange guy', 'dogenb@yahoo.com.ar');
insert into contacts(id, id_address_book, name, short_description, email) values (8, 2, 'Fred Bonaparte', 'likes playing games', 'fredbonaparte733@bigfoot.com');
insert into contacts(id, id_address_book, name, short_description, email) values (9, 2, 'Dr. Loboto', 'stay away from him!', 'dr.loboto@myrealbox.com');

/** For test purposes, even id_contact values have no comments */
insert into comments(id, id_contact, text, date_created, posted_by_user, id_emotion) values (1, 1, 'Que grande Raz! Un personaje único, sin dudas', NOW(), 1, 'happy');
insert into comments(id, id_contact, text, date_created, posted_by_user, id_emotion) values (2, 1, 'Ah, y además tiene buena onda, aunque parezca un poco freak.', date_sub(NOW(),interval 2 hour), 2, 'sad');
insert into comments(id, id_contact, text, date_created, posted_by_user, id_emotion) values (3, 3, 'Un comentario cualquiera.', date_sub(NOW(),interval 60 day), 1, 'mad');
insert into comments(id, id_contact, text, date_created, posted_by_user, id_emotion) values (4, 3, '¿Y esta quién era?', date_sub(NOW(),interval 1 day), 2, 'scared');
insert into comments(id, id_contact, text, date_created, posted_by_user, id_emotion) values (5, 5, 'No recuerdo si era malo o bueno...', date_sub(NOW(),interval 8 day), 1, 'happy');
insert into comments(id, id_contact, text, date_created, posted_by_user, id_emotion) values (6, 7, 'Nombre gracioso tiene este :)', date_sub(NOW(),interval 2 day), 2, 'sad');
insert into comments(id, id_contact, text, date_created, posted_by_user, id_emotion) values (7, 7, 'Pero macanudo!', date_sub(NOW(),interval 20 day), 1, 'mad');
insert into comments(id, id_contact, text, date_created, posted_by_user, id_emotion) values (8, 7, '... ponele. ¿Era el chico que tenía el sombrero de aluminio?', date_sub(NOW(),interval 40 day), 2, 'scared');
insert into comments(id, id_contact, text, date_created, posted_by_user, id_emotion) values (9, 7, 'Tan facil como buscarlo en la wikipedia!', date_sub(NOW(),interval 600 day), 1, 'happy');

/** For test purposes, even id_contact values have no tags */
insert into tags(id, name) values (1, 'niceguy');
insert into tags(id, name) values (2, 'people');
insert into tags(id, name) values (3, 'business');
insert into tags(id, name) values (4, 'friend');
insert into tags(id, name) values (5, 'company');

insert into contacts_tags (id_contact, id_tag) values (1, 1);
insert into contacts_tags (id_contact, id_tag) values (1, 2);
insert into contacts_tags (id_contact, id_tag) values (1, 3);
insert into contacts_tags (id_contact, id_tag) values (1, 4);
insert into contacts_tags (id_contact, id_tag) values (1, 5);
insert into contacts_tags (id_contact, id_tag) values (3, 2);
insert into contacts_tags (id_contact, id_tag) values (3, 5);
insert into contacts_tags (id_contact, id_tag) values (5, 5);
insert into contacts_tags (id_contact, id_tag) values (7, 1);
insert into contacts_tags (id_contact, id_tag) values (7, 4);
insert into contacts_tags (id_contact, id_tag) values (7, 5);
insert into contacts_tags (id_contact, id_tag) values (9, 2);
insert into contacts_tags (id_contact, id_tag) values (9, 4);

insert into address_books_invitations (id, invited_user_email, id_address_book, id_permission, sent_by_user, date_created) values (1, 'gianu@ideasagiles.com', 3, 'r', 2, NOW());
insert into address_books_invitations (id, invited_user_email, id_address_book, id_permission, sent_by_user, date_created) values (2, 'gianu@ideasagiles.com', 4, 'rw', 2, NOW());
insert into address_books_invitations (id, invited_user_email, id_address_book, id_permission, sent_by_user, date_created) values (3, 'leito@ideasagiles.com', 3, 'r', 1, NOW());
insert into address_books_invitations (id, invited_user_email, id_address_book, id_permission, sent_by_user, date_created) values (4, 'leito@ideasagiles.com', 4, 'rw', 1, NOW());
