/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.contactmanager;

import javax.sql.DataSource;
import org.hibernate.SessionFactory;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.init.DataSourceInitializer;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Utility base class for creating tests that use a Database.
 * This class prepares a Spring test context and a test database.
 * If a test method is annotated with @DirtiesDatabase the database will be
 * rebuilt after the method finishs.
 * It also performs a user logout after each test method, to clean the security
 * context if it was used.
 *
 * @author Leito
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
    "classpath:datasource-test.xml",
    "classpath:applicationContext.xml",
    "classpath:payments.xml",
    "classpath:security.xml",
    "classpath:social.xml",
    "classpath:mail-test.xml"
})
public abstract class AbstractDatabaseTest implements DatabaseRebuilder {

    @Rule
    public DirtiesDatabaseRule dirtiesDatabaseRule = new DirtiesDatabaseRule(this);

    @Autowired
    private DataSourceInitializer dataSourceInitializer;

    @Autowired
    protected SessionFactory sessionFactory;

    @Autowired
    private DataSource dataSource;

    protected JdbcTemplate jdbcTemplate;

    /** Creates and populates the application database.
     */
    @Override
    public void rebuildDatabase() {
        //this initializer is meant to be run after sprint set all its properties.
        //here we force the execution of all scripts.
        dataSourceInitializer.afterPropertiesSet();
    }

    @Before
    public void prepareDatabaseConnection() {
        jdbcTemplate = new JdbcTemplate(dataSource);
    }

    /** Logs out all users to clean the context
     */
    @After
    public void logout() {
        SecurityTestUtils.logout();
    }
}
