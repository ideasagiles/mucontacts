/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.contactmanager.dao;

import com.ideasagiles.contactmanager.AbstractDatabaseTest;
import com.ideasagiles.contactmanager.domain.AuthenticationProvider;
import com.ideasagiles.contactmanager.domain.PrivateUser;
import com.ideasagiles.contactmanager.domain.User;
import org.hibernate.TransientObjectException;
import static org.junit.Assert.*;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.transaction.annotation.Transactional;

/**
 * Test for the UserDao Class
 *
 * @author gianu
 */
@Transactional
public class PrivateUserDaoTest extends AbstractDatabaseTest{

    @Autowired
    private PrivateUserDao userDao;

    @Test
    public void save_withValidUser_savesUser() {
        PrivateUser user = new PrivateUser();
        user.setUsername("raz-test");
        user.setEmail("raz-test@ideasagiles.com");
        user.setPassword(new Md5PasswordEncoder().encodePassword("raztest", null));
        user.setAuthenticationProvider(AuthenticationProvider.LOCAL);
        user.setEnabled(true);

        userDao.save(user);
        sessionFactory.getCurrentSession().flush();

        assertNotNull(user.getId());
    }

    @Test
    public void update_withValidUser_changesVersion() {
        Long id = 1L;

        PrivateUser user = userDao.findById(1L);
        user.setName("Raz with new name");
        Long oldVersion = user.getVersion();

        userDao.update(user);
        sessionFactory.getCurrentSession().flush();

        assertEquals(id, user.getId());
        assertTrue(user.getVersion() > oldVersion);
    }

    @Test(expected = TransientObjectException.class)
    public void updateUser_withNullId_throwsException() {
        PrivateUser user = new PrivateUser();
        user.setId(null);

        userDao.update(user);
        sessionFactory.getCurrentSession().flush();

        fail("An exception should have been thrown");
    }

    @Test
    public void findByEmail_withExistingUser_returnsUser() {
        PrivateUser user = userDao.findByEmail("GIANU@ideasagiles.com");

        assertNotNull(user);
        assertNotNull(user.getId());
        assertEquals("gianu@ideasagiles.com", user.getEmail());
    }

    @Test
    public void findByEmail_notExistingUser_returnsNull() {
        PrivateUser user = userDao.findByEmail("zzzxxxyyy");

        assertNull(user);
    }

    @Test
    public void findByEmail_nullEmail_returnsNull() {
        PrivateUser user = userDao.findByEmail(null);
        assertNull(user);
    }

    @Test
    public void findByEmail_emptyEmail_returnsNull() {
        PrivateUser user = userDao.findByEmail("");
        assertNull(user);
    }

    @Test
    public void findByUsername_withExistingUser_returnsUser() {
        PrivateUser user = userDao.findByUsername("LEITO");

        assertNotNull(user);
        assertNotNull(user.getId());
        assertEquals("leito", user.getUsername());
    }

    @Test
    public void findByUsername_notExistingUser_returnsNull() {
        PrivateUser user = userDao.findByUsername("zzzxxxyyy");

        assertNull(user);
    }

    @Test
    public void findByUsername_nullUsername_returnsNull() {
        PrivateUser user = userDao.findByUsername(null);

        assertNull(user);
    }

    @Test
    public void findById_existingUser_returnsUser() {
        Long id = 1L;
        PrivateUser user = userDao.findById(1L);
        assertNotNull(user);
        assertEquals(id, user.getId());
    }

    @Test
    public void findById_notExistingUser_returnsNull() {
        PrivateUser user = userDao.findById(-9999L);
        assertNull(user);
    }

    @Test
    public void findByToken_withExistingToken_returnsUser() {
        final String token = "abcdefghijklmnopq";
        final Long userId = 3L;

        User user = userDao.findByToken(token);

        assertNotNull(user);
        assertEquals(userId, user.getId());
    }

    @Test
    public void findByToken_withNoExistingToken_returnsNull() {
        final String token="thisTokenDoesNotExist";

        User user = userDao.findByToken(token);

        assertNull(user);
    }

    @Test
    public void findByToken_withNullToken_returnsNull() {
        User user = userDao.findByToken(null);

        assertNull(user);
    }

    @Test
    public void findByReferralCode_withExistingCode_returnsUser() {
        final Long userId = 1L;
        final String username = "gianu";
        User user = userDao.findByReferralCode("refcode_1"); //gianu

        assertNotNull(user);
        assertEquals(userId, user.getId());
        assertEquals(username, user.getUsername());
    }

    @Test
    public void findByReferralCode_withNoExistingCode_returnsNull() {
        User user = userDao.findByReferralCode("invalid_referral");

        assertNull(user);
    }

    @Test
    public void findByReferralCode_withNullCode_returnsNull() {
        User user = userDao.findByReferralCode(null);

        assertNull(user);
    }
}