/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.contactmanager.dao;

import com.ideasagiles.contactmanager.AbstractDatabaseTest;
import static org.junit.Assert.*;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author gianu
 */
@Transactional
public class ReferralDaoTest extends AbstractDatabaseTest {

    @Autowired
    private ReferralDao referralDao;

    @Test
    public void countReferral_withValidUserWithNoReferral_returnsCount() {
        long count = referralDao.countReferrals(1L);
        assertEquals(0, count);

    }

    @Test
    public void countReferral_withValidUserWithOneReferral_returnsCount() {
        long count = referralDao.countReferrals(2L);
        assertEquals(1, count);
    }

    @Test
    public void countReferral_withInvalidUser_returnsZero() {
        long count = referralDao.countReferrals(12999L);
        assertEquals(0, count);
    }
}
