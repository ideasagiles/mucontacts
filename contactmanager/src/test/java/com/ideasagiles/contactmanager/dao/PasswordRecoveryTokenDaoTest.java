/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.contactmanager.dao;

import com.ideasagiles.contactmanager.AbstractDatabaseTest;
import com.ideasagiles.contactmanager.domain.PasswordRecoveryToken;
import com.ideasagiles.contactmanager.domain.PrivateUser;
import java.util.Date;
import static org.junit.Assert.*;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author gianu
 */
@Transactional
public class PasswordRecoveryTokenDaoTest extends AbstractDatabaseTest{

    @Autowired
    private PrivateUserDao userDao;
    @Autowired
    private PasswordRecoveryTokenDao passwordRecoveryTokenDao;

    @Test
    public void save_existingUser_returnToken() {
        PrivateUser user = userDao.findByEmail("gianu@ideasagiles.com");

        PasswordRecoveryToken userToken = new PasswordRecoveryToken();
        userToken.setUser(user);
        userToken.setCreationDate(new Date());
        userToken.setToken("abcdefghijk");

        passwordRecoveryTokenDao.save(userToken);

        assertNotNull(userToken.getId());
    }

    @Test
    public void findByUser_existingUserWithToken_returnToken() {
        Long userId = 3L;

        PasswordRecoveryToken userToken = passwordRecoveryTokenDao.findByUser(userId);

        assertNotNull(userToken);
        assertNotNull(userToken.getToken());
        assertEquals(userId, userToken.getUser().getId());
    }

    @Test
    public void findByUser_existingUserWithoutToken_returnNull() {
        Long userId = 1L;

        PasswordRecoveryToken userToken = passwordRecoveryTokenDao.findByUser(userId);

        assertNull(userToken);
    }

    @Test
    public void findByUser_unexistingUser_returnNull() {
        Long userId = 190L;

        PasswordRecoveryToken userToken = passwordRecoveryTokenDao.findByUser(userId);

        assertNull(userToken);
    }

    @Test
    public void findByToken_existingToken_returnUserToken() {
        String token = "abcdefghijklmnopq";

        PasswordRecoveryToken userToken = passwordRecoveryTokenDao.findByToken(token);

        assertNotNull(userToken);
        assertNotNull(userToken.getToken());
        assertEquals(token, userToken.getToken());
    }

    @Test
    public void findByToken_unexistingToken_returnNull() {
        String token = "unexisting_token";

        PasswordRecoveryToken userToken = passwordRecoveryTokenDao.findByToken(token);

        assertNull(userToken);
    }

    @Test
    public void findByToken_nullToken_returnNull() {
        String token = null;

        PasswordRecoveryToken userToken = passwordRecoveryTokenDao.findByToken(token);

        assertNull(userToken);
    }

}