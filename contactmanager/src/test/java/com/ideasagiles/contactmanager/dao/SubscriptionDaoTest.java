/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.contactmanager.dao;

import com.ideasagiles.contactmanager.AbstractDatabaseTest;
import com.ideasagiles.contactmanager.SecurityTestUtils;
import com.ideasagiles.contactmanager.domain.Plan;
import com.ideasagiles.contactmanager.domain.Subscription;
import com.ideasagiles.contactmanager.domain.User;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import javax.validation.ConstraintViolationException;
import static org.junit.Assert.*;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

/**
 * Test cases for SubscriptionDao.
 */
@Transactional
public class SubscriptionDaoTest extends AbstractDatabaseTest {

    @Autowired
    private SubscriptionDao subscriptionDao;

    @Test
    public void save_withValidSubscription_savesTheSubscripton() {
        Plan plan = new Plan();
        plan.setId(1L);
        User user = new User();
        user.setId(1L);
        user.setVersion(1L);

        Subscription subscription = new Subscription();
        subscription.setPlan(plan);
        subscription.setSince(new Date());
        subscription.setUser(user);
        subscription.setValidThrough(null);
        subscription.setAmount(new BigDecimal("139.90"));

        subscriptionDao.save(subscription);

        assertNotNull(subscription.getId());
    }

    @Test(expected=ConstraintViolationException.class)
    public void save_withInvalidSubscription_throwsException() {
        Subscription subscription = new Subscription();

        subscriptionDao.save(subscription);

        fail("An exception should have been thrown.");
    }

    @Test
    public void countContactsByOwner_withExistingUser_returnsCount() {
        long count = subscriptionDao.countContactsByOwner(SecurityTestUtils.USER_WITH_STANDARD_ROLES_ID);
        assertTrue(count > 0);
    }

    @Test
    public void countContactsByOwner_withNotExistingUser_returnsZero() {
        long count = subscriptionDao.countContactsByOwner(Long.MIN_VALUE);
        assertEquals(0, count);
    }

    @Test
    public void countAddressBooksByOwner_withExistingUser_returnsCount() {
        long count = subscriptionDao.countAddressBooksByOwner(SecurityTestUtils.USER_WITH_STANDARD_ROLES_ID);
        assertTrue(count > 0);
    }

    @Test
    public void countAddressBooksByOwner_withNotExistingUser_returnsZero() {
        long count = subscriptionDao.countAddressBooksByOwner(Long.MIN_VALUE);
        assertEquals(0, count);
    }

    @Test
    public void countAddressBookPermissionsByUser_withExistingUserAndPermission_returnsCount() {
        long count = subscriptionDao.countAddressBookPermissionsByUser(SecurityTestUtils.USER_WITH_STANDARD_ROLES_ID, "rw");
        assertTrue(count > 0);
    }

    @Test
    public void countAddressBookPermissionsByUser_withExistingUserAndNotExistingPermission_returnsZero() {
        long count = subscriptionDao.countAddressBookPermissionsByUser(SecurityTestUtils.USER_WITH_STANDARD_ROLES_ID, "doesnotexist");
        assertEquals(0, count);
    }

    @Test
    public void countAddressBookPermissionsByUser_withExistingUserAndNullPermission_returnsZero() {
        long count = subscriptionDao.countAddressBookPermissionsByUser(SecurityTestUtils.USER_WITH_STANDARD_ROLES_ID, null);
        assertEquals(0, count);
    }

    @Test
    public void countAddressBookPermissionsByUser_withNotExistingUser_returnsZero() {
        long count = subscriptionDao.countAddressBookPermissionsByUser(Long.MIN_VALUE, "rw");
        assertEquals(0, count);
    }


    @Test
    public void findActiveSubscriptionByUser_withUserWithOneSubscription_returnsSubscription() {
        Long userId = SecurityTestUtils.USER_WITH_STANDARD_ROLES_ID;

        Subscription subscription = subscriptionDao.findActiveSubscriptionByUser(userId, new Date());

        assertNotNull(subscription);
        assertEquals(userId, subscription.getUser().getId());
    }

    @Test
    public void findActiveSubscriptionByUser_withUserWithExpiredPaidSubscription_returnsFreeSubscription() {
        Long userId = 1L;

        Subscription subscription = subscriptionDao.findActiveSubscriptionByUser(userId, new Date());

        assertNotNull(subscription);
        assertEquals(userId, subscription.getUser().getId());
        assertEquals(new Long(1), subscription.getPlan().getId());
    }

    @Test
    public void findActiveSubscriptionByUser_withUserWithTwoActivePaidSubscriptionsToday_returnsNonFreeSubscription() {
        Long userId = SecurityTestUtils.USER_WITH_RESTRICTED_PLAN_ID;

        Subscription subscription = subscriptionDao.findActiveSubscriptionByUser(userId, new Date());

        assertNotNull(subscription);
        assertEquals(userId, subscription.getUser().getId());
        assertEquals(90000, subscription.getPlan().getId().longValue());
    }

    @Test
    public void findActiveSubscriptionByUser_withUserWithManyActiveSubscriptions_returnsHighestPriorityPaidSubscription() {
        Long userId = SecurityTestUtils.USER_WITH_RESTRICTED_PLAN_ID;

        Calendar cal = new GregorianCalendar();
        cal.add(Calendar.DAY_OF_MONTH, -10);
        Date tenDaysAgo = cal.getTime();

        Subscription subscription = subscriptionDao.findActiveSubscriptionByUser(userId, tenDaysAgo);

        assertNotNull(subscription);
        assertEquals(userId, subscription.getUser().getId());
        assertEquals(90001, subscription.getPlan().getId().longValue());
    }

    @Test
    public void findActiveSubscriptionByUser_withUserWithSubscriptionCreatedJustNow_returnsSubscription() {
        Long userId = 3L;

        Subscription subscription = subscriptionDao.findActiveSubscriptionByUser(userId, new Date());

        assertNotNull(subscription);
        assertEquals(userId, subscription.getUser().getId());
    }


    @Test
    public void findActiveSubscriptionByUser_withNotExistingUser_returnsNull() {
        Subscription subscription = subscriptionDao.findActiveSubscriptionByUser(Long.MIN_VALUE, new Date());
        assertNull(subscription);
    }

    @Test
    public void findActiveSubscriptionByUser_withNullDate_returnsNull() {
        Subscription subscription = subscriptionDao.findActiveSubscriptionByUser(1L, null);
        assertNull(subscription);
    }


    @Test
    public void findLastSubscriptionByPlan_withExistingPlanId_returnsSubscription() {
        long userId = 6L;
        long planId = 90000L;

        Subscription subscription = subscriptionDao.findLastSubscriptionByUserPlan(planId, userId);

        assertNotNull(subscription);
        assertEquals(userId, subscription.getUser().getId().longValue());
        assertEquals(planId, subscription.getPlan().getId().longValue());
    }

    @Test
    public void findLastSubscriptionByPlan_withExpiredSubscription_returnsLastExpiredSubscription() {
        long userId = 6L;
        long planId = 90001L;

        Subscription subscription = subscriptionDao.findLastSubscriptionByUserPlan(planId, userId);

        assertNotNull(subscription);
        assertEquals(planId, subscription.getPlan().getId().longValue());
    }

    @Test
    public void findLastSubscriptionByPlan_withNotExistingPlan_returnsNull() {
        long userId = 6L;
        long planId = Long.MIN_VALUE;

        Subscription subscription = subscriptionDao.findLastSubscriptionByUserPlan(planId, userId);

        assertNull(subscription);
    }

    @Test
    public void findLastSubscriptionByPlan_withNotExistingUser_returnsNull() {
        long userId = Long.MIN_VALUE;
        long planId = 90000L;

        Subscription subscription = subscriptionDao.findLastSubscriptionByUserPlan(planId, userId);

        assertNull(subscription);
    }

    @Test
    public void findPaypalUsernameByUsername_withExistingUser_returnsPaypalUsername() {
        String username = "gianu";
        String paypalUsername = subscriptionDao.findPaypalUsernameByUsername(username);
        assertEquals("gianu@ideasagiles.com", paypalUsername);
    }

    @Test
    public void findPaypalUsernameByUsername_withNotExistingUser_returnsNull() {
        String paypalUsername = subscriptionDao.findPaypalUsernameByUsername("pepe_soriano");
        assertNull(paypalUsername);
    }

}
