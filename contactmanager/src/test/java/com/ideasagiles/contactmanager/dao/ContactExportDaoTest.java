/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.contactmanager.dao;

import com.ideasagiles.contactmanager.AbstractDatabaseTest;
import com.ideasagiles.contactmanager.domain.Contact;
import com.ideasagiles.contactmanager.domain.Tag;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.LineNumberReader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Collection;
import org.junit.After;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

/**
 * Test cases for ContactExportDao.
 */
@Transactional
public class ContactExportDaoTest extends AbstractDatabaseTest {

    @Autowired
    private ContactExportDao contactExportDao;
    private ByteArrayOutputStream out;

    @Before
    public void openDefaultOutputStream() {
        out = new ByteArrayOutputStream();
    }

    @After
    public void closeDefaultOutputStream() throws IOException {
        out.close();
    }

    private Collection<Contact> createTestContactCollection() {
        Collection<Tag> tags = new ArrayList<>();
        Tag t1 = new Tag();
        t1.setName("first tag");
        tags.add(t1);

        Tag t2 = new Tag();
        t2.setName("second tag");
        tags.add(t2);

        Collection<Contact> contacts = new ArrayList<>();
        Contact c1 = new Contact();
        c1.setName("Test subject #1: Ñoqui");
        c1.setAddress("Some address 123");
        c1.setDescription("A brief description.\nAnother line");
        c1.setEmail("my.email@some.domain.com.ar");
        c1.setPhone("(011) 3343 9973");
        c1.setWebsite("http://www.ideasagiles.com");
        c1.setTags(tags);
        contacts.add(c1);

        Contact c2 = new Contact();
        c2.setName("Test subject #2: Voilá");
        c2.setAddress("Some address 123");
        c2.setDescription("A brief description.\nAnother line");
        c2.setEmail("my.email@some.domain.com.ar");
        c2.setPhone("(011) 3343 9973");
        c2.setWebsite("http://www.ideasagiles.com");
        c2.setTags(tags);
        contacts.add(c2);

        Contact c3 = new Contact();
        c3.setName("Test subject #3: ¡Great!");
        c3.setAddress("Some address 123");
        c3.setDescription("A brief description.\nAnother line");
        c3.setEmail("my.email@some.domain.com.ar");
        c3.setPhone("(011) 3343 9973");
        c3.setWebsite("http://www.ideasagiles.com");
        c3.setTags(tags);
        contacts.add(c3);

        return contacts;
    }

    @Test(expected = NullPointerException.class)
    public void exportContactsToCSV_nullContacts_throwsException() throws IOException {
        contactExportDao.exportContactsToCSV(null, out);
    }

    @Test
    public void exportContactsToCSV_emptyContacts_writesHeaderLine() throws IOException {
        contactExportDao.exportContactsToCSV(new ArrayList(), out);

        String csv = out.toString();
        assertNotNull(csv);
        assertTrue(csv.length() > 0);

        StringReader reader = new StringReader(csv);
        LineNumberReader lnr = new LineNumberReader(reader);
        int lineCount = 0;
        while (lnr.readLine() != null) {
            lineCount++;
        }

        // We have an additional header line
        assertEquals(1, lineCount);
    }

    @Test
    public void exportContactsToCSV_withContacts_writesCsv() throws IOException {
        Collection<Contact> contacts = createTestContactCollection();

        contactExportDao.exportContactsToCSV(contacts, out);

        String csv = out.toString();
        assertNotNull(csv);
        assertTrue(csv.length() > 0);

        StringReader reader = new StringReader(csv);
        LineNumberReader lnr = new LineNumberReader(reader);
        int lineCount = 0;
        while (lnr.readLine() != null) {
            lineCount++;
        }

        // We have an additional header line, and extra carriege return characters
        assertTrue(lineCount > contacts.size());
    }
}
