/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.contactmanager.dao;

import com.ideasagiles.contactmanager.AbstractDatabaseTest;
import com.ideasagiles.contactmanager.SecurityTestUtils;
import com.ideasagiles.contactmanager.domain.AddressBook;
import com.ideasagiles.contactmanager.domain.AddressBookInvitation;
import com.ideasagiles.contactmanager.domain.User;
import java.util.Collection;
import java.util.Date;
import javax.validation.ConstraintViolationException;
import org.hibernate.StaleStateException;
import org.hibernate.TransientObjectException;
import static org.junit.Assert.*;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.jdbc.JdbcTestUtils;
import org.springframework.transaction.annotation.Transactional;

/**
 * Test cases for AddressBookDao.
 */
@Transactional
public class AddressBookDaoTest extends AbstractDatabaseTest {

    @Autowired
    private AddressBookDao addressBookDao;

    /** Creates a valid AddressBook for saving (with no id).
     */
    private AddressBook getValidAddressBook() {
        User user = new User();
        user.setId(3L);
        user.setVersion(0L);
        AddressBook addressBook = new AddressBook();
        addressBook.setCreationTime(new Date());
        addressBook.setName("Test address book");
        addressBook.setOwner(user);
        return addressBook;
    }

    @Test
    public void findById_withExistingId_returnsAddressBook() {
        long id = 3L;
        AddressBook addressBook = addressBookDao.findById(id);
        assertNotNull(addressBook);
        assertEquals(id, addressBook.getId().longValue());
    }

    @Test
    public void findById_withNotExistingId_returnsNull() {
        AddressBook addressBook = addressBookDao.findById(Long.MIN_VALUE);
        assertNull(addressBook);
    }

    @Test
    public void save_withValidAddressBook_savesTheAddressBook() {
        AddressBook addressBook = getValidAddressBook();

        addressBookDao.save(addressBook);
        sessionFactory.getCurrentSession().flush();

        assertNotNull(addressBook.getId());
    }

    @Test
    public void save_withInvalidAddressBook_throwsException() {
        AddressBook addressBook = new AddressBook();
        addressBook.setName(""); //empty not allowed
        addressBook.setCreationTime(null); //null not allowed
        addressBook.setOwner(null); //null not allowed

        try {
            addressBookDao.save(addressBook);
            sessionFactory.getCurrentSession().flush();
            fail("An exception shold have been thrown.");
        } catch (ConstraintViolationException ex) {
            assertTrue(ex.getConstraintViolations().size() == 3);
        }
    }

    @Test
    public void update_withValidExistingAddressBook_updatesTheAddressBook() {
        AddressBook addressBook = getValidAddressBook();
        addressBook.setId(1L);
        addressBook.setName("A new name for this addressbook");

        addressBookDao.update(addressBook);
        sessionFactory.getCurrentSession().flush();

        assertNotNull(addressBook.getId());
    }

    @Test(expected=StaleStateException.class)
    public void update_withNotExistingId_throwsException() {
        AddressBook addressBook = getValidAddressBook();
        addressBook.setId(Long.MIN_VALUE);

        addressBookDao.update(addressBook);
        sessionFactory.getCurrentSession().flush();

        fail("An exception should have been thrown.");
    }

    @Test(expected=TransientObjectException.class)
    public void update_withNullId_throwsException() {
        AddressBook addressBook = getValidAddressBook();
        addressBook.setId(null);

        addressBookDao.update(addressBook);
        sessionFactory.getCurrentSession().flush();

        fail("An exception should have been thrown.");
    }

    @Test
    public void update_withInvalidAddressBook_throwsException() {
        AddressBook addressBook = getValidAddressBook();
        addressBook.setId(1L);
        addressBook.setName(""); //empty not allowed
        addressBook.setCreationTime(null); //null not allowed
        addressBook.setOwner(null); //null not allowed

        try {
            addressBookDao.update(addressBook);
            sessionFactory.getCurrentSession().flush();
            fail("An exception shold have been thrown.");
        } catch (ConstraintViolationException ex) {
            assertTrue(ex.getConstraintViolations().size() == 3);
        }
    }

    @Test
    public void findByContact_withExistingContact_returnsAddressBook() {
        AddressBook addressBook = addressBookDao.findByContact(1L);
        assertNotNull(addressBook);
    }

    @Test
    public void findByContact_withNotExistingContact_returnsNull() {
        AddressBook addressBook = addressBookDao.findByContact(Long.MIN_VALUE);
        assertNull(addressBook);
    }

    @Test
    public void findByName_withExistingNameCaseInsensitive_returnsAddressBook() {
        long userId = SecurityTestUtils.USER_WITH_STANDARD_ROLES_ID;
        String name = "test_user@ideasagiles.com ADDreSS BoOk";

        AddressBook addressBook = addressBookDao.findByName(userId, name);

        assertNotNull(addressBook);
        assertEquals(name.toLowerCase(), addressBook.getName().toLowerCase());
    }

    @Test
    public void findByName_withNotExistingName_returnsNull() {
        long userId = SecurityTestUtils.USER_WITH_STANDARD_ROLES_ID;
        String name = "This name should not exist.";

        AddressBook addressBook = addressBookDao.findByName(userId, name);

        assertNull(addressBook);
    }

    @Test
    public void saveInvitation_validInvitation_savesInvitation() {
        User user = new User();
        user.setId(3L);
        user.setVersion(0L); //it needs to be set... why?
        AddressBook addressBook = new AddressBook();
        addressBook.setId(3L);

        AddressBookInvitation invitation = new AddressBookInvitation();
        invitation.setAddressBook(addressBook);
        invitation.setInvitedUserEmail("zim@ideasagiles.com");
        invitation.setCreationTime(new Date());
        invitation.setSentByUser(user);
        invitation.setPermission("rw");

        addressBookDao.save(invitation);

        assertNotNull(invitation.getId());
    }

    @Test(expected = ConstraintViolationException.class)
    public void saveInvitation_invalidEmail_throwsException() {
        AddressBookInvitation invitation = new AddressBookInvitation();
        invitation.setCreationTime(new Date());
        invitation.setInvitedUserEmail("notanemail.com");

        addressBookDao.save(invitation);
        fail("An exception should have been thrown.");
    }

    @Test
    public void findAddressBookInvitationByEmailAndAddressBook_exists_returnsInvitation() {
        long addressBookId = 3L;
        String invitedUserEmail = "notREGisteredUSER@ideasagiles.COM"; //seach should be case insensitive

        AddressBookInvitation invitation = addressBookDao.findInvitationByEmail(addressBookId, invitedUserEmail);

        assertNotNull(invitation);
        assertEquals(addressBookId, invitation.getAddressBook().getId().longValue());
        assertEquals(invitedUserEmail.toLowerCase(), invitation.getInvitedUserEmail().toLowerCase());
    }

    @Test
    public void findAddressBookInvitationByEmailAndAddressBook_doesntExists_returnsNull() {
        long addressBookId = 3L;
        String invitedUserEmail = "isnotinvitedyet@somedomain.com";

        AddressBookInvitation invitation = addressBookDao.findInvitationByEmail(addressBookId, invitedUserEmail);

        assertNull(invitation);
    }

    @Test
    public void findAddressBookInvitationByEmail_exists_returnsInvitations() {
        String invitedUserEmail = SecurityTestUtils.USER_WITH_STANDARD_ROLES_EMAIL;

        Collection<AddressBookInvitation> invitations = addressBookDao.findInvitationByEmail(invitedUserEmail);

        assertFalse(invitations.isEmpty());
    }

    @Test
    public void findAddressBookInvitationByEmail_doesntExists_returnsEmptyCollection() {
        String invitedUserEmail = "isnotinvitedyet@somedomain.com";

        Collection<AddressBookInvitation> invitations = addressBookDao.findInvitationByEmail(invitedUserEmail);

        assertTrue(invitations.isEmpty());
    }

    @Test
    public void findAddressBookInvitationByEmail_nullEmail_returnsEmptyCollection() {
        Collection<AddressBookInvitation> invitations = addressBookDao.findInvitationByEmail(null);

        assertTrue(invitations.isEmpty());
    }

    @Test
    public void findInvitationById_existingId_returnsInvitation() {
        Long invitationId = 2L;
        AddressBookInvitation invitation = addressBookDao.findInvitationById(invitationId);

        assertNotNull(invitation);
    }

    @Test
    public void findInvitationById_nonExistingId_returnsNul() {
        Long invitationId = 190L;
        AddressBookInvitation invitation = addressBookDao.findInvitationById(invitationId);

        assertNull(invitation);
    }

    @Test
    public void deleteInvitation_existintInvitation_deleteInvitation() {
        Long invitationId = 2L;
        AddressBookInvitation invitation = addressBookDao.findInvitationById(invitationId);

        int beforeCount = JdbcTestUtils.countRowsInTable(jdbcTemplate, "address_books_invitations");
        addressBookDao.delete(invitation);
        int afterCount = JdbcTestUtils.countRowsInTable(jdbcTemplate, "address_books_invitations");

        assertEquals(beforeCount - 1, afterCount);
    }

    @Test
    public void findInvitationByAddressBook_hasInvitations_returnsCollection() {
        Long addressBookId = 3L;
        Collection<AddressBookInvitation> invitations = addressBookDao.findInvitationByAddressBook(addressBookId);
        assertNotNull(invitations);
        assertTrue(invitations.size() > 0);
        for (AddressBookInvitation invitation : invitations) {
            assertEquals(addressBookId, invitation.getAddressBook().getId());
        }
    }

    @Test
    public void findInvitationByAddressBook_hasNoInvitations_returnsEmptyCollection() {
        Long addressBookId = Long.MIN_VALUE;
        Collection<AddressBookInvitation> invitations = addressBookDao.findInvitationByAddressBook(addressBookId);
        assertNotNull(invitations);
        assertTrue(invitations.isEmpty());
    }
}
