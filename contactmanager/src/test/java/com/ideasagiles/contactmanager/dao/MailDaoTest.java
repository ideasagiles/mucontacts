/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.contactmanager.dao;

import com.ideasagiles.contactmanager.AbstractDatabaseAndSmtpServerTest;
import com.ideasagiles.contactmanager.domain.*;
import java.io.IOException;
import java.util.Date;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import static org.junit.Assert.*;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailSendException;
import org.springframework.transaction.annotation.Transactional;
import org.subethamail.wiser.WiserMessage;

/**
 * Test cases for MailDao.
 *
 * @author Leito
 */
@Transactional
public class MailDaoTest extends AbstractDatabaseAndSmtpServerTest {

    @Autowired
    private MailDao mailDao;

    private PasswordRecoveryToken getValidUserToken() {
        PrivateUser user = new PrivateUser();
        user.setName("Leito");
        user.setEmail("ldeseta@gmail.com");
        PasswordRecoveryToken userToken = new PasswordRecoveryToken();
        userToken.setToken("abcdefghijklmnopqrstuvwxyz");
        userToken.setUser(user);
        return userToken;
    }

    private AddressBookInvitation getValidAddressBookInvitation() {
        User user = new User();
        user.setEmail("leito@ideasagiles.com");
        user.setName("Leito");
        AddressBook addressBook = new AddressBook();
        addressBook.setId(3L);
        addressBook.setName("A test address book");
        addressBook.setOwner(user);
        AddressBookInvitation invitation = new AddressBookInvitation();
        invitation.setAddressBook(addressBook);
        invitation.setInvitedUserEmail("gianu@ideasagiles.com");
        return invitation;
    }

    private UserInvitation getValidInvitation() {
        User user = new User();
        user.setEmail("gianu@ideasagiles.com");
        user.setName("GiaNU");
        UserInvitation userInvitation = new UserInvitation();
        userInvitation.setEmail("invasorzim@ideasagiles.com");
        userInvitation.setInviter(user);
        userInvitation.setToken("123456789");
        userInvitation.setCreationTime(new Date());

        return userInvitation;
    }

    private PrivateUser getValidPrivateUser() {
        PrivateUser user = new PrivateUser();
        user.setId(3L);
        user.setEmail("gianu@ideasagiles.com");
        user.setName("Sergio Rafael Gianazza");
        user.setUsername("GiaNU");
        return user;
    }

    @Test
    public void sendPasswordRecoveryEmail_withValidToken_sendsEmail() throws IOException, MessagingException {
        PasswordRecoveryToken userToken = getValidUserToken();
        mailDao.sendPasswordRecoveryEmail(userToken);

        assertEquals(1, smtpServer.getMessages().size());
        MimeMessage mail = ((WiserMessage) smtpServer.getMessages().get(0)).getMimeMessage();
        assertNotNull(mail.getSubject());
        assertTrue(mail.getContent().toString().indexOf(userToken.getUser().getEmail()) != -1);
        assertTrue(mail.getContent().toString().indexOf(userToken.getToken()) != -1);
    }

    @Test(expected = MailSendException.class)
    public void sendPasswordRecoveryEmail_withServerDown_throwsMailException() {
        smtpServer.stop();

        PasswordRecoveryToken userToken = getValidUserToken();
        mailDao.sendPasswordRecoveryEmail(userToken);
        fail("An exception should have been thrown.");
    }

    @Test
    public void sendInvitationToAddressBookEmail_validInvitationUserNameSet_sendsEmail() throws MessagingException, IOException {
        AddressBookInvitation invitation = getValidAddressBookInvitation();

        mailDao.sendInvitationToAddressBookEmail(invitation);
        assertEquals(1, smtpServer.getMessages().size());
        MimeMessage mail = ((WiserMessage) smtpServer.getMessages().get(0)).getMimeMessage();
        assertNotNull(mail.getSubject());
        assertTrue(mail.getContent().toString().indexOf(invitation.getAddressBook().getName()) != -1);
        assertTrue(mail.getContent().toString().indexOf(invitation.getAddressBook().getOwner().getName()) != -1);
    }

    @Test
    public void sendInvitationToAddressBookEmail_validInvitationUserNameNotSet_sendsEmail() throws MessagingException, IOException {
        AddressBookInvitation invitation = getValidAddressBookInvitation();
        invitation.getAddressBook().getOwner().setName(null);

        mailDao.sendInvitationToAddressBookEmail(invitation);
        assertEquals(1, smtpServer.getMessages().size());
        MimeMessage mail = ((WiserMessage) smtpServer.getMessages().get(0)).getMimeMessage();
        assertNotNull(mail.getSubject());
        assertTrue(mail.getContent().toString().indexOf(invitation.getAddressBook().getName()) != -1);
        assertTrue(mail.getContent().toString().indexOf(invitation.getAddressBook().getOwner().getEmail()) != -1);
    }

    @Test
    public void sendInvitationToOC_validInvitationObject_sendsEmail() throws MessagingException, IOException {
        UserInvitation invitation = getValidInvitation();

        mailDao.sendInvitationToOCEmail(invitation);
        assertEquals(1, smtpServer.getMessages().size());
        MimeMessage mail = ((WiserMessage) smtpServer.getMessages().get(0)).getMimeMessage();
        assertNotNull(mail.getSubject());
        assertTrue(mail.getContent().toString().indexOf(invitation.getToken()) != -1);
        assertTrue(mail.getContent().toString().indexOf(invitation.getInviter().getName()) != -1);
    }

    @Test
    public void sendSubscriptionCancelMail_ValidObjects_sendsEmail() throws MessagingException, IOException {
        PrivateUser user = getValidPrivateUser();
        String paypalUsername = "valid-paypal-username";

        mailDao.sendSubscriptionCancel(user, paypalUsername);
        assertEquals(1, smtpServer.getMessages().size());
        MimeMessage mail = ((WiserMessage) smtpServer.getMessages().get(0)).getMimeMessage();
        assertNotNull(mail.getSubject());
        assertTrue(mail.getContent().toString().indexOf(paypalUsername) != -1);
        assertTrue(mail.getContent().toString().indexOf(user.getEmail()) != -1);
    }

    @Test
    public void sendAccountCancelMail_ValidObjects_sendsEmail() throws MessagingException, IOException {
        PrivateUser user = getValidPrivateUser();

        mailDao.sendAccountCancel(user);
        assertEquals(1, smtpServer.getMessages().size());
        MimeMessage mail = ((WiserMessage) smtpServer.getMessages().get(0)).getMimeMessage();
        assertNotNull(mail.getSubject());
        assertTrue(mail.getContent().toString().indexOf(user.getId().toString()) != -1);
        assertTrue(mail.getContent().toString().indexOf(user.getUsername()) != -1);
        assertTrue(mail.getContent().toString().indexOf(user.getEmail()) != -1);
    }
}
