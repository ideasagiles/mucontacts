/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.contactmanager;

import org.junit.rules.TestRule;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;

/**
 * Method rule for JUnit that rebuilds the database if the method under test
 * is annotated with @DirtiesDatabase. The rebuild is executed after the method
 * is executed.
 */
public class DirtiesDatabaseRule implements TestRule {

    private DatabaseRebuilder rebuilder;

    public DirtiesDatabaseRule(DatabaseRebuilder rebuilder) {
        this.rebuilder = rebuilder;
    }

    @Override
    public Statement apply(final Statement base, final Description description) {
        return new Statement() {
            @Override
            public void evaluate() throws Throwable {
                try {
                    base.evaluate();
                } finally {
                    DirtiesDatabase dirtiesDatabase = description.getAnnotation(DirtiesDatabase.class);
                    if (dirtiesDatabase != null) {
                        rebuilder.rebuildDatabase();
                    }
                }
            }
        };
    }
}
