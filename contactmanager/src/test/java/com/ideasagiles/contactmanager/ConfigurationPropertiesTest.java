/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.contactmanager;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import org.junit.Before;
import org.junit.Test;
import org.springframework.core.io.support.PropertiesLoaderUtils;

/**
 * Tests the configuration files in the application.
 * This test class tests different configuration files and tries to check for
 * common problems in the different environment configurations.
 */
public class ConfigurationPropertiesTest {
    /** Directory that contains private configuration for different environments */
    private static final String MUCONTACTS_PRIVATE_CONF_DIR = "../../mucontacts_private/conf/";

    private Map<String, Properties> applicationProperties;

    @Before
    public void setUp() throws IOException {
        applicationProperties = new HashMap();
        applicationProperties.put("application.properties", PropertiesLoaderUtils.loadAllProperties("application.properties"));
        addPrivatePropertiesIfExist(applicationProperties, "application.dev.properties");
        addPrivatePropertiesIfExist(applicationProperties, "application.test.properties");
        addPrivatePropertiesIfExist(applicationProperties, "application.prod.properties");
    }

    /** Adds a private configuration properties file, if exists. */
    private void addPrivatePropertiesIfExist(Map applicationProperties, String propertiesFileName) throws IOException {
        try(FileReader reader = new FileReader(MUCONTACTS_PRIVATE_CONF_DIR + propertiesFileName)) {
            Properties properties = new Properties();
            properties.load(reader);
            applicationProperties.put(propertiesFileName, properties);
        } catch (FileNotFoundException ex) {
            //file not found, ignore
        }
    }

    /**
     * Checks that all configuration files for the application has the exact
     * same keys, in all files.
     * It iterates through each configuration file for different environments
     * and checks against the other files for the existance of all the keys.
     * It also checks that each key has a value assigned.
     */
    @Test
    public void checkAllPropertiesKeysAreSetInAllPropertiesFiles() {
        Set<String> names = applicationProperties.keySet();
        for (String currentName : names) {
            Set<String> keys = applicationProperties.get(currentName).stringPropertyNames();
            for (String key : keys) {
                for (String propertiesName : names) {
                    Properties properties = applicationProperties.get(propertiesName);
                    String value = properties.getProperty(key);
                    assertNotNull("Property " + key + " was not found in " + propertiesName, value);
                    assertFalse("Property " + key + " has no value assigned in " + propertiesName, value.isEmpty());
                }
            }
        }
    }
}
