/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.contactmanager.admin.service;

import com.ideasagiles.contactmanager.AbstractDatabaseTest;
import com.ideasagiles.contactmanager.DirtiesDatabase;
import com.ideasagiles.contactmanager.SecurityTestUtils;
import com.ideasagiles.contactmanager.domain.User;
import com.ideasagiles.contactmanager.service.PrivateUserService;
import java.util.Collection;
import static org.junit.Assert.*;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.test.jdbc.JdbcTestUtils;

/**
 * Test class for UserAdminService.
 */
public class UserAdminServiceTest extends AbstractDatabaseTest {
    @Autowired
    private UserAdminService userAdminService;
    @Autowired
    private PrivateUserService privateUserService;


    @Test
    public void findAll_adminIsLoggedIn_returnsAllUsers() {
        SecurityTestUtils.loginUserWithAdminRoles();

        Collection<User> users = userAdminService.findAll();

        int userCount = JdbcTestUtils.countRowsInTable(jdbcTemplate, "users");

        assertNotNull(users);
        assertEquals(userCount, users.size());
    }

    @Test(expected=AccessDeniedException.class)
    public void findAll_adminIsNotLoggedIn_throwsSecurityException() {
        SecurityTestUtils.loginUserWithStandardRoles();

        userAdminService.findAll();
        fail("An exception should have been thrown.");
    }

    @Test(expected=AuthenticationCredentialsNotFoundException.class)
    public void findAll_userIsNotLoggedIn_throwsSecurityException() {
        userAdminService.findAll();
        fail("An exception should have been thrown.");
    }

    @Test
    @DirtiesDatabase
    public void setUserEnabledStatus_adminIsLoggedIn_changesUserStatus() {
        SecurityTestUtils.loginUserWithAdminRoles();

        userAdminService.setUserEnabledStatus(SecurityTestUtils.USER_DISABLED_ID, true);

        User user = privateUserService.findByEmail(SecurityTestUtils.USER_DISABLED_EMAIL);
        assertNotNull(user);
        assertTrue(user.getEnabled());
    }

    @Test(expected=AccessDeniedException.class)
    public void setUserEnabledStatus_userIsLoggedIn_throwsSecurityException() {
        SecurityTestUtils.loginUserWithStandardRoles();
        userAdminService.setUserEnabledStatus(SecurityTestUtils.USER_DISABLED_ID, true);
        fail("An exception should have been thrown.");
    }

    @Test(expected=AuthenticationCredentialsNotFoundException.class)
    public void setUserEnabledStatus_userIsNotLoggedIn_throwsSecurityException() {
        userAdminService.setUserEnabledStatus(SecurityTestUtils.USER_DISABLED_ID, true);
        fail("An exception should have been thrown.");
    }

    @Test
    @DirtiesDatabase
    public void deleteUserAccount_existingDisabledUser_deletesUserAccount() {
        SecurityTestUtils.loginUserWithAdminRoles();

        int beforeUserCount = JdbcTestUtils.countRowsInTable(jdbcTemplate, "users");
        int beforeAuthoritiesCount = JdbcTestUtils.countRowsInTable(jdbcTemplate, "authorities");
        int beforeAddressBookCount = JdbcTestUtils.countRowsInTable(jdbcTemplate, "address_books");
        int beforeSubscriptionsCount = JdbcTestUtils.countRowsInTable(jdbcTemplate, "subscriptions");
        userAdminService.deleteUserAccount(SecurityTestUtils.USER_DISABLED_ID);
        int afterUserCount = JdbcTestUtils.countRowsInTable(jdbcTemplate, "users");
        int afterAuthoritiesCount = JdbcTestUtils.countRowsInTable(jdbcTemplate, "authorities");
        int afterAddressBookCount = JdbcTestUtils.countRowsInTable(jdbcTemplate, "address_books");
        int afterSubscriptionsCount = JdbcTestUtils.countRowsInTable(jdbcTemplate, "subscriptions");

        assertEquals("There should be 1 less user", beforeUserCount-1, afterUserCount);
        assertTrue("There should be less authorities", beforeAuthoritiesCount > afterAuthoritiesCount);
        assertTrue("There should be less address books", beforeAddressBookCount > afterAddressBookCount);
        assertTrue("There should be less subscriptions", beforeSubscriptionsCount > afterSubscriptionsCount);
    }

    @Test(expected=AccessDeniedException.class)
    public void deleteUserAccount_existingEnabledUser_throwsException() {
        SecurityTestUtils.loginUserWithAdminRoles();
        userAdminService.deleteUserAccount(SecurityTestUtils.USER_WITH_STANDARD_ROLES_ID);
        fail("An exception should have been thrown.");
    }


    @Test(expected=AuthenticationCredentialsNotFoundException.class)
    public void deleteUserAccount_userIsNotLoggedIn_throwsException() {
        userAdminService.deleteUserAccount(SecurityTestUtils.USER_DISABLED_ID);
        fail("An exception should have been thrown.");
    }

    @Test(expected=AccessDeniedException.class)
    public void deleteUserAccount_adminIsNotLoggedIn_throwsException() {
        SecurityTestUtils.loginUserWithStandardRoles();
        userAdminService.deleteUserAccount(SecurityTestUtils.USER_DISABLED_ID);
        fail("An exception should have been thrown.");
    }
    
    @Test
    public void getWeeklyGrowthRate_adminIsLoggedIn_getGrowthRate() {
        SecurityTestUtils.loginUserWithAdminRoles();
        double growthRate = userAdminService.getWeeklyGrowthRate();
        assertEquals("The growth rate should be 3.49", 3.49, growthRate, 0.01);
    }
    
    @Test(expected=AccessDeniedException.class)
    public void getWeeklyGrowthRate_adminIsNotLoggedIn_throwsException() {
        SecurityTestUtils.loginUserWithStandardRoles();
        userAdminService.getWeeklyGrowthRate();
        fail("An exception should have been thrown.");
    }
}
