/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.contactmanager.security;

import com.ideasagiles.contactmanager.AbstractDatabaseTest;
import com.ideasagiles.contactmanager.SecurityTestUtils;
import com.ideasagiles.contactmanager.domain.PrivateUser;
import com.ideasagiles.contactmanager.service.PrivateUserService;
import java.util.Date;
import static org.junit.Assert.*;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.transaction.annotation.Transactional;

/**
 * Tests for AccountIdAsPrincipalSigninService class.
 *
 * @author Leito
 */
@Transactional
public class AccountIdAsPrincipalSigninServiceTest extends AbstractDatabaseTest {

    @Autowired
    private AccountIdAsPrincipalSigninService accountIdAsPrincipalSigninService;

    @Autowired
    private PrivateUserService privateUserService;


    @Test
    public void signIn_withExistingUser_signInUser() {
        String userId = SecurityTestUtils.USER_WITH_STANDARD_ROLES_ID.toString();
        Date now = new Date();

        accountIdAsPrincipalSigninService.signIn(userId, null, null);

        PrivateUser user = privateUserService.getLoggedInUser();
        assertNotNull(user);
        assertEquals(userId, user.getId().toString());
        assertNotNull(user.getLastSignIn());
        assertTrue(user.getLastSignIn().getTime() >= now.getTime());
    }

    @Test(expected=AccessDeniedException.class)
    public void signIn_withNotExistingUser_throwsException() {
        String userId = "-99999";
        accountIdAsPrincipalSigninService.signIn(userId, null, null);
        fail("An exception should have been thrown.");
    }

    @Test(expected=NumberFormatException.class)
    public void signIn_withNotNumericUserId_throwsException() {
        String userId = "not-a-number";
        accountIdAsPrincipalSigninService.signIn(userId, null, null);
        fail("An exception should have been thrown.");
    }

}
