/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.contactmanager.security;

import com.ideasagiles.contactmanager.AbstractDatabaseTest;
import com.ideasagiles.contactmanager.SecurityTestUtils;
import com.ideasagiles.contactmanager.dao.PrivateUserDao;
import com.ideasagiles.contactmanager.domain.PrivateUser;
import java.util.Date;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Test;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.event.AuthenticationSuccessEvent;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.transaction.annotation.Transactional;

/**
 * Tests for AuthenticationSuccessEventListener.
 * @author ldeseta
 */
public class AuthenticationSuccessEventListenerTest extends AbstractDatabaseTest {

    @Autowired
    private AuthenticationSuccessEventListener instance;

    @Autowired
    private PrivateUserDao privateUserDao;

    private AuthenticationSuccessEvent event;


    @Before
    public void setup() {
        UserDetails userDetails = mock(UserDetails.class);
        when(userDetails.getUsername()).thenReturn(SecurityTestUtils.USER_WITH_STANDARD_ROLES_USERNAME);

        Authentication authentication = mock(Authentication.class);
        when(authentication.getPrincipal()).thenReturn(userDetails);

        event = mock(AuthenticationSuccessEvent.class);
        when(event.getAuthentication()).thenReturn(authentication);
    }

    @Test
    @Transactional
    public void onApplicationEvent_userExists_updatesLastSignIn() {
        Date now = new Date();

        instance.onApplicationEvent(event);

        PrivateUser user = privateUserDao.findByUsername(SecurityTestUtils.USER_WITH_STANDARD_ROLES_USERNAME);
        assertNotNull(user.getLastSignIn());
        assertTrue(user.getLastSignIn().getTime() >= now.getTime());
    }

}
