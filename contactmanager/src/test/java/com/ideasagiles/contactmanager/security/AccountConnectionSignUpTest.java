/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.contactmanager.security;

import com.ideasagiles.contactmanager.AbstractDatabaseTest;
import com.ideasagiles.contactmanager.dao.PrivateUserDao;
import com.ideasagiles.contactmanager.domain.AuthenticationProvider;
import com.ideasagiles.contactmanager.domain.PrivateUser;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.social.connect.Connection;
import org.springframework.social.connect.ConnectionData;
import org.springframework.social.connect.UserProfile;
import org.springframework.social.twitter.api.Twitter;
import org.springframework.transaction.annotation.Transactional;

/**
 * Tests for AccountConnectionSignUp class.
 *
 * @author Leito
 */
@Transactional
public class AccountConnectionSignUpTest extends AbstractDatabaseTest {

    @Autowired
    private AccountConnectionSignUp accountConnectionSignUp;

    @Autowired
    private PrivateUserDao privateUserDao;

    private Connection<Twitter> providerConnection;
    private UserProfile providerUserProfile;;
    private ConnectionData providerConnectionData;

    @Before
    public void setup() {
        providerConnection = mock(Connection.class);
        providerUserProfile = mock(UserProfile.class);
        providerConnectionData = mock(ConnectionData.class);

        when(providerUserProfile.getEmail()).thenReturn(null); //Twitter API doest not return email
        when(providerUserProfile.getName()).thenReturn("Ideas Agiles");
        when(providerUserProfile.getEmail()).thenReturn("contacto@ideasagiles.com");

        when(providerConnection.createData()).thenReturn(providerConnectionData);
        when(providerConnection.fetchUserProfile()).thenReturn(providerUserProfile);
    }

    @Test
    public void execute_withTwitterUserDoesNotExist_createsNewUserAccount() {
        String username = "ideasagiles";
        when(providerUserProfile.getUsername()).thenReturn(username);
        when(providerConnectionData.getProviderId()).thenReturn("twitter");

        String localUserId = accountConnectionSignUp.execute(providerConnection);

        assertNotNull(localUserId);

        PrivateUser user = privateUserDao.findById(Long.parseLong(localUserId));

        assertNotNull(user);
        assertEquals(AuthenticationProvider.TWITTER, user.getAuthenticationProvider());
        assertEquals(user.getName(), providerUserProfile.getName());
        assertEquals("twitter:" + username, user.getUsername());
        assertNotNull("User Id should be a positive number", user.getId());
        assertNotNull("MemberSince shouldn't be null", user.getMemberSince());
        assertNotNull("Version shouln't be null", user.getVersion());
        assertTrue("The user should be enabled", user.getEnabled());
        assertNull("Pasword should be forced to null", user.getPassword());
    }

    @Test
    public void execute_facebookReturnsOnlyUsername_createsNewUserAccount() {
        when(providerUserProfile.getUsername()).thenReturn("invader.zim");
        when(providerUserProfile.getEmail()).thenReturn(null);
        when(providerUserProfile.getName()).thenReturn(null);
        when(providerConnectionData.getProviderId()).thenReturn("facebook");

        String localUserId = accountConnectionSignUp.execute(providerConnection);

        assertNotNull(localUserId);

        PrivateUser user = privateUserDao.findById(Long.parseLong(localUserId));

        assertNotNull(user);
        assertEquals(AuthenticationProvider.FACEBOOK, user.getAuthenticationProvider());
        assertEquals(user.getName(), providerUserProfile.getName());
        assertEquals("facebook:" + providerUserProfile.getUsername(), user.getUsername());
        assertNotNull("User Id should be a positive number", user.getId());
        assertNotNull("MemberSince shouldn't be null", user.getMemberSince());
        assertNotNull("Version shouln't be null", user.getVersion());
        assertTrue("The user should be enabled", user.getEnabled());
        assertNull("Pasword should be forced to null", user.getPassword());
    }

    @Test
    public void execute_facebookReturnsOnlyEmail_createsNewUserAccount() {
        when(providerUserProfile.getUsername()).thenReturn(null);
        when(providerUserProfile.getName()).thenReturn(null);
        when(providerConnectionData.getProviderId()).thenReturn("facebook");

        String localUserId = accountConnectionSignUp.execute(providerConnection);

        assertNotNull(localUserId);

        PrivateUser user = privateUserDao.findById(Long.parseLong(localUserId));

        assertNotNull(user);
        assertEquals(AuthenticationProvider.FACEBOOK, user.getAuthenticationProvider());
        assertEquals(user.getName(), providerUserProfile.getName());
        assertEquals("facebook:" + providerUserProfile.getEmail(), user.getUsername());
        assertNotNull("User Id should be a positive number", user.getId());
        assertNotNull("MemberSince shouldn't be null", user.getMemberSince());
        assertNotNull("Version shouln't be null", user.getVersion());
        assertTrue("The user should be enabled", user.getEnabled());
        assertNull("Pasword should be forced to null", user.getPassword());
    }

    @Test
    public void execute_facebookReturnsOnlyName_createsNewUserAccountWithNotName() {
        when(providerUserProfile.getUsername()).thenReturn(null);
        when(providerUserProfile.getEmail()).thenReturn(null);
        when(providerUserProfile.getName()).thenReturn("Invader Zim");
        when(providerConnectionData.getProviderId()).thenReturn("facebook");

        String localUserId = accountConnectionSignUp.execute(providerConnection);

        assertNotNull(localUserId);

        PrivateUser user = privateUserDao.findById(Long.parseLong(localUserId));

        assertNotNull(user);
        assertEquals(AuthenticationProvider.FACEBOOK, user.getAuthenticationProvider());
        assertEquals(user.getName(), providerUserProfile.getName());
        assertEquals("facebook:" + providerUserProfile.getName(), user.getUsername());
        assertNotNull("User Id should be a positive number", user.getId());
        assertNotNull("MemberSince shouldn't be null", user.getMemberSince());
        assertNotNull("Version shouln't be null", user.getVersion());
        assertTrue("The user should be enabled", user.getEnabled());
        assertNull("Pasword should be forced to null", user.getPassword());
    }

    @Test
    public void execute_facebookReturnsNullEmailAndNullName_createsNewUserAccountWithDefaultUsername() {
        when(providerUserProfile.getUsername()).thenReturn(null);
        when(providerUserProfile.getEmail()).thenReturn(null);
        when(providerUserProfile.getName()).thenReturn(null);
        when(providerConnectionData.getProviderId()).thenReturn("facebook");

        String localUserId = accountConnectionSignUp.execute(providerConnection);

        assertNotNull(localUserId);

        PrivateUser user = privateUserDao.findById(Long.parseLong(localUserId));

        assertNotNull(user);
        assertEquals(AuthenticationProvider.FACEBOOK, user.getAuthenticationProvider());
        assertEquals(user.getName(), providerUserProfile.getName());
        assertEquals("facebook:friend", user.getUsername());
        assertNotNull("User Id should be a positive number", user.getId());
        assertNotNull("MemberSince shouldn't be null", user.getMemberSince());
        assertNotNull("Version shouln't be null", user.getVersion());
        assertTrue("The user should be enabled", user.getEnabled());
        assertNull("Pasword should be forced to null", user.getPassword());
    }

    @Test(expected=UnsupportedOperationException.class)
    public void execute_withUnknownProvider_throwsException() {
        String username = "ideasagiles";
        when(providerUserProfile.getUsername()).thenReturn(username);
        when(providerConnectionData.getProviderId()).thenReturn("unknownProvider");

        accountConnectionSignUp.execute(providerConnection);

        fail("An exception should have been thrown.");
    }

    /* Future test scenarios:
     *   - UserProfile.getUsername() returns an email (Facebook?).
     *   - UserProfile.getUsername() returns null (is it possible?).
     *   - UserProfile.getUsername() returns a name and getEmail() returns an email (should we use the email?).
     *   - The user already exists in the database.
     */
}
