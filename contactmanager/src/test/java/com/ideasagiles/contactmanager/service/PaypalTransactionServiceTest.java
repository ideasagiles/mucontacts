/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.contactmanager.service;

import com.ideasagiles.contactmanager.AbstractDatabaseTest;
import com.ideasagiles.contactmanager.PaypalValidationMockServer;
import com.ideasagiles.contactmanager.dao.SubscriptionDao;
import com.ideasagiles.contactmanager.domain.PaypalSubscriptionPayment;
import com.ideasagiles.contactmanager.domain.Subscription;
import com.ideasagiles.contactmanager.service.exception.PaymentTransactionException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.jdbc.JdbcTestUtils;
import org.springframework.transaction.annotation.Transactional;

/**
 * Test cases for paypal transactions.
 */
@Transactional
public class PaypalTransactionServiceTest extends AbstractDatabaseTest {

    @Autowired
    private PaypalTransactionServiceImpl paypalTransactionService;
    @Autowired
    private SubscriptionDao subscriptionDao;

    /** Mock paypal server to replace remote server. */
    private static PaypalValidationMockServer paypalServer;


    @BeforeClass
    public static void startPaypalServer() throws IOException {
        paypalServer = new PaypalValidationMockServer();
        paypalServer.start(9876);
    }

    @Before
    public void resetPaypalServer() throws MalformedURLException {
        //change URL to service
        URL validationUrlMock = new URL("http://localhost:9876");
        paypalTransactionService.setValidationUrl(validationUrlMock);

        //set default response
        paypalServer.setResponseToVerified();
    }

    @AfterClass
    public static void stopServer() throws IOException {
        paypalServer.stop();
    }

    /** Returns a valid PaypalTransaction. */
    private PaypalSubscriptionPayment getValidPaypalTransaction() {
        PaypalSubscriptionPayment tx = new PaypalSubscriptionPayment();
        tx.setPaymentStatus("Completed");
        tx.setTransactionId("45544");
        tx.setReceiverEmail("paypal.receiver.email@gmail.com");
        tx.setItemNumber("PAYPAL TEST PLAN");
        tx.setCustomValue("username=test_user_with_restricted_plan");
        tx.setDateReceived(new Date());
        return tx;
    }

    /** Returns a valid set of request parameters for a Paypal transaction. */
    private Map<String, String[]> getValidRequestParams() {
        return new HashMap<>();
    }

    @Test(expected = PaymentTransactionException.class)
    public void processPayment_invalidPaypalTransactionStatus_throwsException() throws PaymentTransactionException {
        paypalServer.setResponseToInvalid();
        paypalTransactionService.processPayment(getValidPaypalTransaction(), getValidRequestParams());
        fail("An exception should have been thrown.");
    }

    @Test(expected = PaymentTransactionException.class)
    public void processPayment_unknownPaypalTransactionStatus_throwsException() throws PaymentTransactionException {
        paypalServer.setResponseToUnknownValue();
        paypalTransactionService.processPayment(getValidPaypalTransaction(), getValidRequestParams());
        fail("An exception should have been thrown.");
    }

    @Test(expected = PaymentTransactionException.class)
    public void processPayment_paymentStatusNotCompleted_throwsException() throws PaymentTransactionException {
        PaypalSubscriptionPayment tx = getValidPaypalTransaction();
        tx.setPaymentStatus("not completed");

        paypalTransactionService.processPayment(tx, getValidRequestParams());
        fail("An exception should have been thrown.");
    }

    @Test(expected = PaymentTransactionException.class)
    public void processPayment_paypalTransactionIdAlreadyExists_throwsException() throws PaymentTransactionException {
        PaypalSubscriptionPayment tx = getValidPaypalTransaction();
        tx.setTransactionId("1");

        paypalTransactionService.processPayment(tx, getValidRequestParams());
        fail("An exception should have been thrown.");
    }

    @Test(expected = PaymentTransactionException.class)
    public void processPayment_receiverEmailDoesNotMatch_throwsException() throws PaymentTransactionException {
        PaypalSubscriptionPayment tx = getValidPaypalTransaction();
        tx.setReceiverEmail("shouldnotmatch@ideasagiles.com");

        paypalTransactionService.processPayment(tx, getValidRequestParams());
        fail("An exception should have been thrown.");
    }

    @Test(expected = PaymentTransactionException.class)
    public void processPayment_itemNumberNotFoundInPlan_throwsException() throws PaymentTransactionException {
        PaypalSubscriptionPayment tx = getValidPaypalTransaction();
        tx.setItemNumber("SHOULD NOT EXIST");

        paypalTransactionService.processPayment(tx, getValidRequestParams());
        fail("An exception should have been thrown.");
    }

    @Test(expected = PaymentTransactionException.class)
    public void processPayment_transactionIdNull_throwsException() throws PaymentTransactionException {
        PaypalSubscriptionPayment tx = getValidPaypalTransaction();
        tx.setTransactionId(null);

        paypalTransactionService.processPayment(tx, getValidRequestParams());
        fail("An exception should have been thrown.");
    }

    @Test(expected = PaymentTransactionException.class)
    public void processPayment_usernameDoesNotExist_throwsException() throws PaymentTransactionException {
        PaypalSubscriptionPayment tx = getValidPaypalTransaction();
        tx.setCustomValue("username=does not exist;");

        paypalTransactionService.processPayment(tx, getValidRequestParams());
        fail("An exception should have been thrown.");
    }

    @Test(expected = PaymentTransactionException.class)
    public void processPayment_invalidCustomValueFormat_throwsException() throws PaymentTransactionException {
        PaypalSubscriptionPayment tx = getValidPaypalTransaction();
        tx.setCustomValue("invalidFormat");

        paypalTransactionService.processPayment(tx, getValidRequestParams());
        fail("An exception should have been thrown.");
    }

    @Test
    public void processPayment_validPaymentUserWithActivePaidSubscription_savesPaypalTransactionAndSubscription() throws PaymentTransactionException {
        int beforeCountSubscriptions = JdbcTestUtils.countRowsInTable(jdbcTemplate, "subscriptions");
        int beforeCountPaypalTransactions = JdbcTestUtils.countRowsInTable(jdbcTemplate, "paypal_subscription_payments");
        PaypalSubscriptionPayment paypalTransaction = getValidPaypalTransaction();

        paypalTransactionService.processPayment(paypalTransaction, getValidRequestParams());

        int afterCountSubscriptions = JdbcTestUtils.countRowsInTable(jdbcTemplate, "subscriptions");
        int afterCountPaypalTransactions = JdbcTestUtils.countRowsInTable(jdbcTemplate, "paypal_subscription_payments");

        assertNotNull(paypalTransaction.getId());
        assertNotNull(paypalTransaction.getSubscriptionId());
        assertEquals(beforeCountSubscriptions + 1, afterCountSubscriptions);
        assertEquals(beforeCountPaypalTransactions + 1, afterCountPaypalTransactions);
    }

    @Test
    public void processPayment_validPaymentUserWithFreePlan_savesPaypalTransactionAndSubscription() throws PaymentTransactionException {
        int beforeCountSubscriptions = JdbcTestUtils.countRowsInTable(jdbcTemplate, "subscriptions");
        int beforeCountPaypalTransactions = JdbcTestUtils.countRowsInTable(jdbcTemplate, "paypal_subscription_payments");

        PaypalSubscriptionPayment paypalTransaction = getValidPaypalTransaction();
        paypalTransaction.setCustomValue("username=leito");
        paypalTransactionService.processPayment(paypalTransaction, getValidRequestParams());

        int afterCountSubsriptions = JdbcTestUtils.countRowsInTable(jdbcTemplate, "subscriptions");
        int afterCountPaypalTransactions = JdbcTestUtils.countRowsInTable(jdbcTemplate, "paypal_subscription_payments");

        assertNotNull(paypalTransaction.getId());
        assertNotNull(paypalTransaction.getSubscriptionId());
        assertEquals(beforeCountSubscriptions + 1, afterCountSubsriptions);
        assertEquals(beforeCountPaypalTransactions + 1, afterCountPaypalTransactions);
    }

    @Test
    public void processPayment_validPendingTransactionUserWithActivePaidSubscription_savesPaypalTransactionAndSubscription() throws PaymentTransactionException {
        int beforeCountSubscriptions = JdbcTestUtils.countRowsInTable(jdbcTemplate, "subscriptions");
        int beforeCountPaypalTransactions = JdbcTestUtils.countRowsInTable(jdbcTemplate, "paypal_subscription_payments");

        PaypalSubscriptionPayment paypalTransaction = getValidPaypalTransaction();
        paypalTransaction.setPaymentStatus("Pending");
        paypalTransactionService.processPayment(paypalTransaction, getValidRequestParams());

        int afterCountSubsriptions = JdbcTestUtils.countRowsInTable(jdbcTemplate, "subscriptions");
        int afterCountPaypalTransactions = JdbcTestUtils.countRowsInTable(jdbcTemplate, "paypal_subscription_payments");

        assertNotNull(paypalTransaction.getId());
        assertNotNull(paypalTransaction.getSubscriptionId());
        assertEquals(beforeCountSubscriptions + 1, afterCountSubsriptions);
        assertEquals(beforeCountPaypalTransactions + 1, afterCountPaypalTransactions);
    }

    @Test(expected = PaymentTransactionException.class)
    public void processPayment_validPendingTransactionAlreadyRegistered_throwsException() throws PaymentTransactionException {
        PaypalSubscriptionPayment paypalTransaction = getValidPaypalTransaction();
        paypalTransaction.setTransactionId("300");
        paypalTransaction.setPaymentStatus("Pending");
        paypalTransactionService.processPayment(paypalTransaction, getValidRequestParams());
        fail("An exception should have been thrown.");
    }

    @Test
    public void processPayment_validCompletedTransactionAndPendingTransactionRegistered_savesPaypalTransaction() throws PaymentTransactionException {
        int beforeCountSubscriptions = JdbcTestUtils.countRowsInTable(jdbcTemplate, "subscriptions");
        int beforeCountPaypalTransactions = JdbcTestUtils.countRowsInTable(jdbcTemplate, "paypal_subscription_payments");

        PaypalSubscriptionPayment paypalTransaction = getValidPaypalTransaction();
        paypalTransaction.setTransactionId("300");
        paypalTransaction.setPaymentStatus("Completed");
        paypalTransactionService.processPayment(paypalTransaction, getValidRequestParams());

        int afterCountSubsriptions = JdbcTestUtils.countRowsInTable(jdbcTemplate, "subscriptions");
        int afterCountPaypalTransactions = JdbcTestUtils.countRowsInTable(jdbcTemplate, "paypal_subscription_payments");

        assertNotNull(paypalTransaction.getId());
        assertNotNull(paypalTransaction.getSubscriptionId());
        assertEquals("No subscriptions should have been created", beforeCountSubscriptions, afterCountSubsriptions);
        assertEquals(beforeCountPaypalTransactions + 1, afterCountPaypalTransactions);
    }





    //TODO: test user cases: users with current subscription active. (upgrade and downgrade)
    @Test
    public void processPayment_validPaymentUserWithPaidPlan_upgradesSubscriptionAndSavesPaypalTransaction() throws PaymentTransactionException {

        PaypalSubscriptionPayment paypalTransaction = getValidPaypalTransaction();
        paypalTransaction.setItemNumber("EXTENDED PAYPAL TEST PLAN");

        int beforeCountPaypalTransactions = JdbcTestUtils.countRowsInTable(jdbcTemplate, "paypal_subscription_payments");

        paypalTransactionService.processPayment(paypalTransaction, getValidRequestParams());

        int afterCountPaypalTransactions = JdbcTestUtils.countRowsInTable(jdbcTemplate, "paypal_subscription_payments");

        Subscription newSub = subscriptionDao.findLastSubscriptionByUserPlan(90001L, 6L);

        Calendar today = new GregorianCalendar();
        Calendar since = new GregorianCalendar();
        since.setTime(newSub.getSince());

        assertNotNull(paypalTransaction.getId());
        assertNotNull(paypalTransaction.getSubscriptionId());
        assertEquals(paypalTransaction.getSubscriptionId(), newSub.getId());
        assertEquals(beforeCountPaypalTransactions + 1, afterCountPaypalTransactions);
        assertTrue(newSub.getValidThrough().after(today.getTime()));

        assertEquals(today.get(Calendar.YEAR), since.get(Calendar.YEAR));
        assertEquals(today.get(Calendar.DAY_OF_YEAR), since.get(Calendar.DAY_OF_YEAR));
    }

    @Test
    public void processPayment_validPaymentUserWithFreePlan_upgradesSubscriptionAndSavesPaypalTransaction() throws PaymentTransactionException {
        int beforeCountPaypalTransactions = JdbcTestUtils.countRowsInTable(jdbcTemplate, "paypal_subscription_payments");

        PaypalSubscriptionPayment paypalTransaction = getValidPaypalTransaction();
        paypalTransaction.setCustomValue("username=leito");
        paypalTransaction.setItemNumber("PAYPAL TEST PLAN");

        paypalTransactionService.processPayment(paypalTransaction, getValidRequestParams());

        Subscription oldSub = subscriptionDao.findLastSubscriptionByUserPlan(1L, 2L);
        Subscription newSub = subscriptionDao.findLastSubscriptionByUserPlan(90000L, 2L);

        int afterCountPaypalTransactions = JdbcTestUtils.countRowsInTable(jdbcTemplate, "paypal_subscription_payments");

        Date today = new Date();

        assertNotNull(paypalTransaction.getId());
        assertNotNull(paypalTransaction.getSubscriptionId());
        assertEquals(beforeCountPaypalTransactions + 1, afterCountPaypalTransactions);
        assertTrue(oldSub.getValidThrough() == null);
        assertTrue(newSub.getValidThrough().after(today));
    }

    @Test
    public void processPayment_validPaymentUserWithPaidPlan_downgradeSubscriptionAndSavesPaypalTransaction() throws PaymentTransactionException {
        Subscription oldPreSub = subscriptionDao.findLastSubscriptionByUserPlan(90000L, 6L);
        PaypalSubscriptionPayment paypalTransaction = getValidPaypalTransaction();
        paypalTransaction.setItemNumber("FREE PLAN");

        int beforeCountPaypalTransactions = JdbcTestUtils.countRowsInTable(jdbcTemplate, "paypal_subscription_payments");

        paypalTransactionService.processPayment(paypalTransaction, getValidRequestParams());

        int afterCountPaypalTransactions = JdbcTestUtils.countRowsInTable(jdbcTemplate, "paypal_subscription_payments");

        Subscription oldPostSub = subscriptionDao.findLastSubscriptionByUserPlan(90000L, 6L);
        Subscription newSub = subscriptionDao.findLastSubscriptionByUserPlan(1L, 6L);

        assertNotNull(paypalTransaction.getId());
        assertNotNull(paypalTransaction.getSubscriptionId());
        assertEquals(beforeCountPaypalTransactions + 1, afterCountPaypalTransactions);
        assertTrue(oldPreSub.getValidThrough().equals(oldPostSub.getValidThrough()));
        assertTrue(oldPreSub.getSince().equals(oldPostSub.getSince()));
        assertTrue(newSub.getValidThrough() == null);
    }
}
