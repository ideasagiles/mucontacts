/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.contactmanager.service;

import com.ideasagiles.contactmanager.AbstractDatabaseTest;
import com.ideasagiles.contactmanager.DirtiesDatabase;
import com.ideasagiles.contactmanager.SecurityTestUtils;
import com.ideasagiles.contactmanager.domain.AddressBook;
import com.ideasagiles.contactmanager.domain.Contact;
import com.ideasagiles.contactmanager.domain.Tag;
import com.ideasagiles.contactmanager.service.exception.SubscriptionLimitException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Set;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.HibernateOptimisticLockingFailureException;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.test.jdbc.JdbcTestUtils;

/**
 * Tests for ContactService class. This test class is NOT transactional in order
 * to test the transactions of the service class.
 *
 * @author Leito
 */
public class ContactServiceTest extends AbstractDatabaseTest {

    @Autowired
    private ContactService contactService;

    @Before
    public void loginUserWithStandardRoles() {
        SecurityTestUtils.loginUserWithStandardRoles();
    }

    private AddressBook getAddressBookForTestUser() {
        AddressBook addressBook = new AddressBook();
        addressBook.setId(3L);
        return addressBook;
    }

    private AddressBook getAddressBookNotInvitedForTestUser() {
        AddressBook addressBook = new AddressBook();
        addressBook.setId(5L);
        return addressBook;
    }

    private AddressBook getAddressBookReadOnlyForTestUser() {
        AddressBook addressBook = new AddressBook();
        addressBook.setId(1L);
        return addressBook;
    }

    private AddressBook getAddressBookOneForTestUserWithRestrictedPlan() {
        AddressBook addressBook = new AddressBook();
        addressBook.setId(6L);
        return addressBook;
    }

    private AddressBook getAddressBookTwoForTestUserWithRestrictedPlan() {
        AddressBook addressBook = new AddressBook();
        addressBook.setId(601L);
        return addressBook;
    }

    private Contact getContactForTestUser() {
        Contact contact = new Contact();
        contact.setId(1L);
        contact.setAddressBook(getAddressBookForTestUser());
        contact.setName("Razputin Raz Aquato");
        contact.setEmail("raz@ideasagiles.com");
        contact.setVersion(0L);
        return contact;
    }

    @Test
    public void save_withNullId_assignsNewIdAndVersion() {
        Contact contact = new Contact();
        contact.setName("PRUEBA");
        contact.setEmail("raz@ideasagiles.com");
        contact.setAddressBook(getAddressBookForTestUser());

        int beforeCount = JdbcTestUtils.countRowsInTable(jdbcTemplate, "contacts");
        contactService.save(contact);
        int afterCount = JdbcTestUtils.countRowsInTable(jdbcTemplate, "contacts");

        assertNotNull(contact.getId());
        assertNotNull(contact.getCreationTime());
        assertEquals(beforeCount + 1, afterCount);
        assertNotNull(contact.getVersion());
        assertNotNull(contact.getAddressBook());
        assertNotNull(contact.getStatistics());
        assertEquals(0, contact.getStatistics().getHappyEmotionCount());
        assertEquals(0, contact.getStatistics().getMadEmotionCount());
        assertEquals(0, contact.getStatistics().getSadEmotionCount());
        assertEquals(0, contact.getStatistics().getScaredEmotionCount());
    }

    @Test(expected = AccessDeniedException.class)
    public void save_withInvalidAddressBook_throwsSecurityException() {
        Contact contact = new Contact();
        contact.setName("PRUEBA");
        contact.setAddressBook(getAddressBookNotInvitedForTestUser());

        contactService.save(contact);

        fail("An exception should have been thrown.");
    }

    @Test(expected = AccessDeniedException.class)
    public void save_withReadOnlyAddressBook_throwsSecurityException() {
        Contact contact = new Contact();
        contact.setName("PRUEBA");
        contact.setAddressBook(getAddressBookReadOnlyForTestUser());

        contactService.save(contact);

        fail("An exception should have been thrown.");
    }

    @Test
    public void save_withCreationAndModificationDates_overridesDates() {
        Contact contact = new Contact();
        contact.setName("PRUEBA");
        contact.setEmail("raz@ideasagiles.com");
        contact.setAddressBook(getAddressBookForTestUser());
        Date creationDate = new Date(System.currentTimeMillis() - 1000);
        contact.setCreationTime(creationDate);
        contact.setLastModificationTime(creationDate);

        int beforeCount = JdbcTestUtils.countRowsInTable(jdbcTemplate, "contacts");
        contactService.save(contact);
        int afterCount = JdbcTestUtils.countRowsInTable(jdbcTemplate, "contacts");

        assertNotNull(contact.getId());
        assertEquals(beforeCount + 1, afterCount);
        assertNull(contact.getLastModificationTime());
        assertTrue(contact.getCreationTime().after(creationDate));
    }

    @Test
    public void save_withNullMandatoryValues_throwsValidationException() {
        Contact contact = new Contact();
        contact.setAddressBook(getAddressBookForTestUser());
        try {
            contactService.save(contact);
            fail("An exception should have been thrown.");
        } catch (ConstraintViolationException ex) {
            Set<ConstraintViolation<?>> constraintViolations = ex.getConstraintViolations();
            assertEquals(1, constraintViolations.size());
        }
    }

    @Test
    public void save_withInvalidEmail_throwsValidationException() {
        String invalidEmail = "invalidemail.com";
        Contact contact = new Contact();
        contact.setName("a contact with an invalid email");
        contact.setAddressBook(getAddressBookForTestUser());
        contact.setEmail(invalidEmail);
        try {
            contactService.save(contact);
            fail("An exception should have been thrown.");
        } catch (ConstraintViolationException ex) {
            Set<ConstraintViolation<?>> constraintViolations = ex.getConstraintViolations();
            assertEquals(1, constraintViolations.size());

            String invalidValue = (String) constraintViolations.iterator().next().getInvalidValue();
            assertEquals(invalidEmail, invalidValue);
        }
    }

    @Test
    public void save_withNewAndExistingTags_createsAndAssignsTagsToContact() {
        Date creationDate = new Date(System.currentTimeMillis() - 1000);
        Contact contact = new Contact();
        contact.setName("PRUEBA");
        contact.setAddressBook(getAddressBookForTestUser());

        Collection<Tag> tags = new ArrayList<>();
        Tag tag1 = new Tag();
        tag1.setName("niceguy");
        Tag tag2 = new Tag();
        tag2.setName("people");
        Tag tag3 = new Tag();
        tag3.setName("this is a new tag");
        tags.add(tag1);
        tags.add(tag2);
        tags.add(tag3);
        contact.setTags(tags);

        int beforeContactsCount = JdbcTestUtils.countRowsInTable(jdbcTemplate, "contacts");
        int beforeTagsCount = JdbcTestUtils.countRowsInTable(jdbcTemplate, "tags");
        contactService.save(contact);
        int afterContactsCount = JdbcTestUtils.countRowsInTable(jdbcTemplate, "contacts");
        int afterTagsCount = JdbcTestUtils.countRowsInTable(jdbcTemplate, "tags");

        assertNotNull(contact.getId());
        assertNull(contact.getLastModificationTime());
        assertTrue(contact.getCreationTime().after(creationDate));
        assertNotNull(tag3.getId());
        assertEquals(beforeContactsCount + 1, afterContactsCount);
        assertEquals(beforeTagsCount + 3, afterTagsCount);
    }

    @Test
    public void save_withNewTags_createsAndAssignsTagsToContact() {
        Date creationDate = new Date(System.currentTimeMillis() - 1000);
        Contact contact = new Contact();
        contact.setName("PRUEBA");
        contact.setAddressBook(getAddressBookForTestUser());

        Collection<Tag> tags = new ArrayList<>();
        Tag tag1 = new Tag();
        tag1.setName("newtag1");
        Tag tag2 = new Tag();
        tag2.setName("newtag2");
        Tag tag3 = new Tag();
        tag3.setName("newtag3");
        tags.add(tag1);
        tags.add(tag2);
        tags.add(tag3);
        contact.setTags(tags);

        int beforeContactsCount = JdbcTestUtils.countRowsInTable(jdbcTemplate, "contacts");
        int beforeTagsCount = JdbcTestUtils.countRowsInTable(jdbcTemplate, "tags");
        contactService.save(contact);
        int afterContactsCount = JdbcTestUtils.countRowsInTable(jdbcTemplate, "contacts");
        int afterTagsCount = JdbcTestUtils.countRowsInTable(jdbcTemplate, "tags");

        assertNotNull(contact.getId());
        assertNull(contact.getLastModificationTime());
        assertTrue(contact.getCreationTime().after(creationDate));

        assertNotNull(tag1.getId());
        assertNotNull(tag1.getContact());
        assertEquals(contact.getId(), tag1.getContact().getId());

        assertNotNull(tag2.getId());
        assertNotNull(tag2.getContact());
        assertEquals(contact.getId(), tag2.getContact().getId());

        assertNotNull(tag3.getId());
        assertNotNull(tag3.getContact());
        assertEquals(contact.getId(), tag3.getContact().getId());

        for (Tag tag : contact.getTags()) {
            assertNotNull(tag.getId());
            assertNotNull(tag.getContact());
            assertEquals(contact.getId(), tag.getContact().getId());
        }

        assertEquals(beforeContactsCount + 1, afterContactsCount);
        assertEquals(beforeTagsCount + contact.getTags().size(), afterTagsCount);
    }

    @Test
    public void save_withExistingTags_assignsTagsToContact() {
        Date creationDate = new Date(System.currentTimeMillis() - 1000);
        Contact contact = new Contact();
        contact.setName("PRUEBA");
        contact.setAddressBook(getAddressBookForTestUser());

        Collection<Tag> tags = new ArrayList<>();
        Tag tagInUse1 = new Tag();
        tagInUse1.setName("friend");
        Tag tagInUse2 = new Tag();
        tagInUse2.setName("people");
        Tag tagInUse3 = new Tag();
        tagInUse3.setName("niceguy");
        tags.add(tagInUse1);
        tags.add(tagInUse2);
        tags.add(tagInUse3);
        contact.setTags(tags);

        int beforeContactsCount = JdbcTestUtils.countRowsInTable(jdbcTemplate, "contacts");
        int beforeTagsCount = JdbcTestUtils.countRowsInTable(jdbcTemplate, "tags");
        contactService.save(contact);
        int afterContactsCount = JdbcTestUtils.countRowsInTable(jdbcTemplate, "contacts");
        int afterTagsCount = JdbcTestUtils.countRowsInTable(jdbcTemplate, "tags");

        assertNotNull(contact.getId());
        assertNull(contact.getLastModificationTime());
        assertTrue(contact.getCreationTime().after(creationDate));
        assertEquals(beforeContactsCount + 1, afterContactsCount);
        assertEquals(beforeTagsCount + 3, afterTagsCount);
    }

    @Test(expected = SubscriptionLimitException.class)
    public void save_withRestrictedPlan_throwsException() {
        SecurityTestUtils.loginUserWithRestrictedPlan();

        Contact contact = new Contact();
        contact.setName("PRUEBA");
        contact.setEmail("raz@ideasagiles.com");
        contact.setAddressBook(getAddressBookOneForTestUserWithRestrictedPlan());

        contactService.save(contact);
        fail("An exception should have been thrown.");
    }

    @Test
    public void update_existingContact_updatesContact() {

        Contact contact = getContactForTestUser();
        Long id = contact.getId();

        contact.setName("The NEW name");
        //set an old las modification time
        Date modificationTimeBeforeUpdate = new Date(System.currentTimeMillis() - 1000);
        contact.setLastModificationTime(modificationTimeBeforeUpdate);
        Long oldVersion = contact.getVersion();
        Date creationTimeBeforeUpdate = contact.getCreationTime();

        contactService.update(contact);

        assertEquals(id, contact.getId());
        assertTrue(contact.getVersion() > oldVersion);
        assertTrue(modificationTimeBeforeUpdate.getTime() < contact.getLastModificationTime().getTime());
        assertEquals(creationTimeBeforeUpdate, contact.getCreationTime());
        assertNotNull(contact.getStatistics());
    }

    @Test(expected = AccessDeniedException.class)
    public void update_withNotInvitedAddressBook_throwsSecurityException() {
        Contact contact = getContactForTestUser();
        contact.setName("The NEW name");
        contact.setAddressBook(getAddressBookNotInvitedForTestUser());

        contactService.update(contact);

        fail("An exception should have been thrown.");
    }

    @Test(expected = AccessDeniedException.class)
    public void update_withReadOnlyAddressBook_throwsSecurityException() {
        Contact contact = getContactForTestUser();
        contact.setName("The NEW name");
        contact.setAddressBook(getAddressBookReadOnlyForTestUser());

        contactService.update(contact);

        fail("An exception should have been thrown.");
    }

    @Test
    public void update_contactWithNoTagAndAddExistingTags_assignsTagsToContact() {
        Long id = new Long(2L);

        Contact contact = contactService.findById(id);
        contact.setName("The NEW name");
        //set an old las modification time
        Date modificationTimeBeforeUpdate = new Date(System.currentTimeMillis() - 1000);
        contact.setLastModificationTime(modificationTimeBeforeUpdate);
        Long oldVersion = contact.getVersion();
        Date creationTimeBeforeUpdate = contact.getCreationTime();


        Collection<Tag> tags = new ArrayList<>();
        Tag tagInUse1 = new Tag();
        tagInUse1.setName("friend");
        Tag tagInUse2 = new Tag();
        tagInUse2.setName("people");
        Tag tagInUse3 = new Tag();
        tagInUse3.setName("niceguy");
        tags.add(tagInUse1);
        tags.add(tagInUse2);
        tags.add(tagInUse3);
        contact.setTags(tags);

        int beforeContactsCount = JdbcTestUtils.countRowsInTable(jdbcTemplate, "contacts");
        int beforeTagsCount = JdbcTestUtils.countRowsInTable(jdbcTemplate, "tags");
        contactService.update(contact);
        int afterContactsCount = JdbcTestUtils.countRowsInTable(jdbcTemplate, "contacts");
        int afterTagsCount = JdbcTestUtils.countRowsInTable(jdbcTemplate, "tags");

        assertEquals(id, contact.getId());
        assertTrue(contact.getVersion() > oldVersion);
        assertTrue(modificationTimeBeforeUpdate.getTime() < contact.getLastModificationTime().getTime());
        assertEquals(creationTimeBeforeUpdate, contact.getCreationTime());
        assertNotNull(tagInUse1.getId());
        assertNotNull(tagInUse2.getId());
        assertNotNull(tagInUse3.getId());

        for (Tag tag : contact.getTags()) {
            assertNotNull(tag.getId());
            assertNotNull(tag.getContact());
            assertEquals(contact.getId(), tag.getContact().getId());
        }

        assertEquals(beforeContactsCount, afterContactsCount);
        assertEquals(beforeTagsCount + 3, afterTagsCount);
    }

    @Test(expected = AccessDeniedException.class)
    public void update_nullId_throwsSecurityException() {
        Contact contact = new Contact();
        contact.setId(null);

        contactService.update(contact);

        fail("An exception should have been thrown.");
    }

    @Test(expected = SubscriptionLimitException.class)
    public void update_existingContactUpdateAddressBookToRestrictedUser_throwsException() {
        Contact contact = getContactForTestUser();
        contact.setName("The NEW name");
        contact.setAddressBook(getAddressBookOneForTestUserWithRestrictedPlan());

        contactService.update(contact);
        fail("An exception should have been thrown.");
    }

    @Test
    @DirtiesDatabase
    public void update_existingContactOfRestrictedUserDontChangeAddressBook_updatedContact() {
        Long id = new Long(600L);

        Contact contact = contactService.findById(id);
        contact.setName("The NEW name");
        //set an old las modification time
        Date modificationTimeBeforeUpdate = new Date(System.currentTimeMillis() - 1000);
        contact.setLastModificationTime(modificationTimeBeforeUpdate);
        Long oldVersion = contact.getVersion();
        Date creationTimeBeforeUpdate = contact.getCreationTime();

        contactService.update(contact);

        assertEquals(id, contact.getId());
        assertTrue(contact.getVersion() > oldVersion);
        assertTrue(modificationTimeBeforeUpdate.getTime() < contact.getLastModificationTime().getTime());
        assertEquals(creationTimeBeforeUpdate, contact.getCreationTime());
    }

    @Test
    @DirtiesDatabase
    public void update_existingContactUpdateAddressBookOfRestrictedToAnotherAddressBookOfSameUser_updatedContact() {
        SecurityTestUtils.loginUserWithRestrictedPlan();
        Long id = new Long(600L);

        Contact contact = contactService.findById(id);
        contact.setName("The NEW name");
        contact.setAddressBook(getAddressBookTwoForTestUserWithRestrictedPlan());
        //set an old las modification time
        Date modificationTimeBeforeUpdate = new Date(System.currentTimeMillis() - 1000);
        contact.setLastModificationTime(modificationTimeBeforeUpdate);
        Long oldVersion = contact.getVersion();
        Date creationTimeBeforeUpdate = contact.getCreationTime();

        contactService.update(contact);

        assertEquals(id, contact.getId());
        assertTrue(contact.getVersion() > oldVersion);
        assertTrue(modificationTimeBeforeUpdate.getTime() < contact.getLastModificationTime().getTime());
        assertEquals(creationTimeBeforeUpdate, contact.getCreationTime());
    }

    @Test(expected = HibernateOptimisticLockingFailureException.class)
    public void update_olderVersion_throwsException() {
        Contact contact = getContactForTestUser();
        contact.setVersion(-100L);

        contactService.update(contact);

        fail("An exception should have been thrown.");
    }

    @Test
    public void findById_withExistingId_returnsContact() {
        long id = 2L;

        Contact contact = contactService.findById(id);

        assertNotNull(contact);
        assertEquals(id, contact.getId().longValue());
    }

    @Test(expected = AccessDeniedException.class)
    public void findById_withNotExistingId_throwsSecurityException() {
        long id = -8888888L;

        Contact contact = contactService.findById(id);

        assertNull(contact);
    }

    @Test(expected = AccessDeniedException.class)
    public void findById_withNoPermissionOnContact_throwsSecurityException() {
        SecurityTestUtils.loginUserWithNoInvitations();
        contactService.findById(2L);
        fail("An exception should have been thrown.");
    }

    @Test
    public void findAll_loggedInUser_returnsAllContacts() {
        Collection<Contact> contacts = contactService.findAll();

        assertNotNull(contacts);
        assertTrue(contacts.size() > 0);
        for (Contact contact : contacts) {
            assertNotNull(contact.getId());
            assertNotNull(contact.getAddressBook());
            assertNotNull(contact.getTags());
        }
    }

    @Test
    @DirtiesDatabase
    public void delete_withExistingId_deletesContact() {
        Long id = 1L;

        int beforeCount = JdbcTestUtils.countRowsInTable(jdbcTemplate, "contacts");
        contactService.delete(id);
        int afterCount = JdbcTestUtils.countRowsInTable(jdbcTemplate, "contacts");

        assertEquals(beforeCount - 1, afterCount);
    }

    @Test(expected = AccessDeniedException.class)
    public void delete_withNoPermission_throwsSecurityException() {
        SecurityTestUtils.loginUserWithNoInvitations();
        Long id = 1L;

        contactService.delete(id);
        fail("An exception should have been thrown.");
    }
}
