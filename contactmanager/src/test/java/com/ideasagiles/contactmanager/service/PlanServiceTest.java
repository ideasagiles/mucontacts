/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.contactmanager.service;

import com.ideasagiles.contactmanager.AbstractDatabaseTest;
import com.ideasagiles.contactmanager.domain.Plan;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Tests for PlanService.
 */
public class PlanServiceTest extends AbstractDatabaseTest {

    @Autowired
    private PlanService planService;

    @Test
    public void findLevelZeroPlan_notLoggedInUser_returnsPlan() {
        Plan plan = planService.findLevelZeroPlan();

        assertNotNull(plan);
        assertEquals(1L, plan.getId().longValue());
    }

    @Test
    public void findLevelOnePlan_notLoggedInUser_returnsPlan() {
        Plan plan = planService.findLevelOnePlan();

        assertNotNull(plan);
        assertEquals(3L, plan.getId().longValue());
    }

    @Test
    public void findLevelTwoPlan_notLoggedInUser_returnsPlan() {
        Plan plan = planService.findLevelTwoPlan();

        assertNotNull(plan);
        assertEquals(4L, plan.getId().longValue());
    }

    @Test
    public void findLevelThreePlan_notLoggedInUser_returnsPlan() {
        Plan plan = planService.findLevelThreePlan();

        assertNotNull(plan);
        assertEquals(5L, plan.getId().longValue());
    }

    @Test
    public void findLevelFourPlan_notLoggedInUser_returnsPlan() {
        Plan plan = planService.findLevelFourPlan();

        assertNotNull(plan);
        assertEquals(6L, plan.getId().longValue());
    }

}
