/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.contactmanager.service;

import com.ideasagiles.contactmanager.AbstractDatabaseTest;
import com.ideasagiles.contactmanager.SecurityTestUtils;
import com.ideasagiles.contactmanager.domain.Comment;
import com.ideasagiles.contactmanager.domain.Emotion;
import java.util.Collection;
import java.util.Set;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import static org.junit.Assert.*;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;

/**
 *
 * @author ldeseta
 */
public class CommentServiceTest extends AbstractDatabaseTest {

    @Autowired
    private CommentService commentService;

    @Autowired
    private ContactService contactService;

    @Test
    public void findByContact_withExistingContact_returnsCollection() {
        SecurityTestUtils.loginUserWithStandardRoles();
        Collection<Comment> comments = commentService.findByContact(1L);

        assertNotNull(comments);
        assertTrue(comments.size() > 0);
    }

    @Test(expected=AccessDeniedException.class)
    public void findByContact_withNotExistingContact_throwsSecurityException() {
        SecurityTestUtils.loginUserWithStandardRoles();
        Collection<Comment> comments = commentService.findByContact(Long.MIN_VALUE);

        assertNotNull(comments);
        assertEquals(0, comments.size());
    }

    @Test(expected=AccessDeniedException.class)
    public void findByContact_withUnauthorizedUser_throwsSecurityException() {
        SecurityTestUtils.loginUserWithNoInvitations();
        commentService.findByContact(2L);
        fail("An exception should have been thrown.");
    }

    @Test
    public void save_withValidComment_assignsNewIdAndDate() {
        SecurityTestUtils.loginUserWithStandardRoles();
        long contactId = 1L;
        Comment comment = new Comment();
        comment.setText("Test data");
        comment.setContactId(contactId);
        comment.setEmotion(Emotion.HAPPY);

        long oldEmotionStat = contactService.findById(contactId).getStatistics().getHappyEmotionCount();
        commentService.save(comment);
        long newEmotionStat = contactService.findById(contactId).getStatistics().getHappyEmotionCount();

        assertNotNull(comment.getId());
        assertNotNull(comment.getCreationTime());
        assertNotNull(comment.getPostedBy());
        assertEquals(SecurityTestUtils.USER_WITH_STANDARD_ROLES_EMAIL, comment.getPostedBy().getEmail());
        assertEquals(oldEmotionStat + 1, newEmotionStat);
    }

    @Test(expected=AccessDeniedException.class)
    public void save_contactWithNoPermission_throwsSecurityException() {
        SecurityTestUtils.loginUserWithNoInvitations();
        Comment comment = new Comment();
        comment.setText("Test data");
        comment.setContactId(1L);
        comment.setEmotion(Emotion.HAPPY);

        commentService.save(comment);
        fail("An exception should have been thrown.");
    }

    @Test(expected=AccessDeniedException.class)
    public void save_notExistingContact_throwsSecurityException() {
        SecurityTestUtils.loginUserWithStandardRoles();
        Comment comment = new Comment();
        comment.setText("Test data");
        comment.setContactId(Long.MIN_VALUE);
        comment.setEmotion(Emotion.HAPPY);

        commentService.save(comment);
        fail("An exception should have been thrown.");
    }

    @Test(expected = ConstraintViolationException.class)
    public void save_withEmptyText_throwsValidationException() {
        SecurityTestUtils.loginUserWithStandardRoles();
        Comment comment = new Comment();
        comment.setText("");
        comment.setContactId(1L);
        comment.setEmotion(Emotion.HAPPY);

        commentService.save(comment);
        fail("An exception should have been thrown.");
    }

    @Test
    public void save_withNullMandatoryValues_throwsValidationException() {
        SecurityTestUtils.loginUserWithStandardRoles();
        Comment comment = new Comment();
        comment.setContactId(1L);
        comment.setText(null);
        comment.setEmotion(null);

        try {
            commentService.save(comment);
            fail("An exception should have been thrown.");
        } catch (ConstraintViolationException ex) {
            Set<ConstraintViolation<?>> constraintViolations = ex.getConstraintViolations();
            assertEquals(2, constraintViolations.size());
        }
    }
}
