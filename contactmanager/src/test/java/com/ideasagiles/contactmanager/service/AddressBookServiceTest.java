/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.contactmanager.service;

import com.ideasagiles.contactmanager.AbstractDatabaseAndSmtpServerTest;
import com.ideasagiles.contactmanager.DirtiesDatabase;
import com.ideasagiles.contactmanager.SecurityTestUtils;
import com.ideasagiles.contactmanager.controller.exception.DuplicateException;
import com.ideasagiles.contactmanager.domain.AddressBook;
import com.ideasagiles.contactmanager.domain.AddressBookInvitation;
import com.ideasagiles.contactmanager.domain.AddressBookPermission;
import com.ideasagiles.contactmanager.service.exception.SubscriptionLimitException;
import java.util.Collection;
import javax.validation.ConstraintViolationException;
import static org.junit.Assert.*;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.test.jdbc.JdbcTestUtils;
import org.springframework.transaction.annotation.Transactional;

/**
 * Test cases for AddressBookService.
 */
public class AddressBookServiceTest extends AbstractDatabaseAndSmtpServerTest {

    @Autowired
    private AddressBookService addressBookService;
    @Autowired
    private AddressBookPermissionService addressBookPermissionService;


    @Test
    public void findById_withExistingId_returnsAddressBook() {
        long id = 3L;
        AddressBook addressBook = addressBookService.findById(id);
        assertNotNull(addressBook);
        assertEquals(id, addressBook.getId().longValue());
    }

    @Test
    public void findById_withNotExistingId_returnsNull() {
        AddressBook addressBook = addressBookService.findById(Long.MIN_VALUE);
        assertNull(addressBook);
    }


    @Test
    @DirtiesDatabase
    public void save_withValidName_savesAddressBook() {
        SecurityTestUtils.loginUserWithStandardRoles();
        AddressBook addressBook = new AddressBook();
        addressBook.setName("   A tiny new AddressBook   "); //should trim

        Collection<AddressBookPermission> permissionsBefore = addressBookPermissionService.getAddressBookPermissionsForLoggedInUser();
        addressBookService.save(addressBook);
        Collection<AddressBookPermission> permissionsAfter = addressBookPermissionService.getAddressBookPermissionsForLoggedInUser();

        assertNotNull(addressBook.getId());
        assertNotNull(addressBook.getCreationTime());
        assertEquals(addressBook.getName().trim(), addressBook.getName());
        assertEquals(SecurityTestUtils.USER_WITH_STANDARD_ROLES_ID, addressBook.getOwner().getId());
        assertEquals(permissionsBefore.size() + 1, permissionsAfter.size());

        //verify that the new AddressBookPermission exists and it has rw permissions.
        boolean rwiPermissionAssigned = false;
        for (AddressBookPermission addressBookPermission : permissionsAfter) {
            if (addressBookPermission.getAddressBook().getId().equals(addressBook.getId())) {
                if (addressBookPermission.getPermission().indexOf("rwi") != -1) {
                    rwiPermissionAssigned = true;
                }
                break;
            }
        }
        assertTrue(rwiPermissionAssigned);
    }

    @Test(expected = ConstraintViolationException.class)
    public void save_withInvalidName_throwsValidationException() {
        SecurityTestUtils.loginUserWithStandardRoles();
        AddressBook addressBook = new AddressBook();
        addressBook.setName("    ");

        addressBookService.save(addressBook);
        fail("An exception should have been thrown.");
    }

    @Test(expected = AuthenticationCredentialsNotFoundException.class)
    public void save_withNotLoggedInUser_throwsSecurityException() {
        AddressBook addressBook = new AddressBook();
        addressBook.setName("something");

        addressBookService.save(addressBook);
        fail("An exception should have been thrown.");
    }

    @Test(expected = DuplicateException.class)
    public void save_withDuplicateAddressBookName_throwsException() {
        SecurityTestUtils.loginUserWithStandardRoles();
        AddressBook addressBook = new AddressBook();
        addressBook.setName("test_user@ideasagiles.com Address Book");

        addressBookService.save(addressBook);
        fail("An exception should have been thrown.");
    }

    @Test(expected = SubscriptionLimitException.class)
    public void save_withRestrictedPlan_throwsException() {
        SecurityTestUtils.loginUserWithRestrictedPlan();
        AddressBook addressBook = new AddressBook();
        addressBook.setName("test_user@ideasagiles.com Address Book");

        addressBookService.save(addressBook);
        fail("An exception should have been thrown.");
    }

    /*
     * This test MUST be transactional, because our CustomPermissionEvaluator
     * looks for this addressbook before executing the method, and Hibernate
     * will fail if there is another instance of this AddressBook.
     */
    @Test
    @Transactional
    @DirtiesDatabase
    public void update_userIsOwnerOfAddressBook_updatesTheAddressBook() {
        SecurityTestUtils.loginUserWithStandardRoles();
        AddressBook addressBook = addressBookService.findById(3L);
        addressBook.setName("A new name");

        addressBookService.update(addressBook);
        sessionFactory.getCurrentSession().flush();
    }

    /*
     * This test MUST be transactional, because our CustomPermissionEvaluator
     * looks for this addressbook before executing the method, and Hibernate
     * will fail if there is another instance of this AddressBook.
     */
    @Test
    @Transactional
    @DirtiesDatabase
    public void update_userHasRwiPermissionsOnAddressBook_updatesTheAddressBook() {
        SecurityTestUtils.loginUserWithUnlimitedPlan();
        AddressBook addressBook = addressBookService.findById(3L);
        addressBook.setName("A new name");

        addressBookService.update(addressBook);
        sessionFactory.getCurrentSession().flush();
    }

    @Test(expected = ConstraintViolationException.class)
    @Transactional
    public void update_emptyName_throwsException() {
        SecurityTestUtils.loginUserWithUnlimitedPlan();
        AddressBook addressBook = addressBookService.findById(3L);
        addressBook.setName("   ");

        addressBookService.update(addressBook);
        sessionFactory.getCurrentSession().flush();
    }

    @Test(expected = ConstraintViolationException.class)
    @Transactional
    public void update_nullName_throwsException() {
        SecurityTestUtils.loginUserWithUnlimitedPlan();
        AddressBook addressBook = addressBookService.findById(3L);
        addressBook.setName(null);

        addressBookService.update(addressBook);
        sessionFactory.getCurrentSession().flush();
    }

    @Test(expected=AuthenticationCredentialsNotFoundException.class)
    public void update_withNotLoggedInUser_throwsSecurityException() {
        AddressBook addressBook = addressBookService.findById(3L);
        addressBookService.update(addressBook);
    }

    @Test(expected=AccessDeniedException.class)
    public void update_userWithNoPermissionsOnAddressBook_throwsSecurityException() {
        SecurityTestUtils.loginUserWithRestrictedPlan();
        AddressBook addressBook = addressBookService.findById(3L);
        addressBookService.update(addressBook);
    }


    @Test
    @DirtiesDatabase
    public void inviteUser_withValidInvitation_savesInvitationAndSendsEmail() {
        SecurityTestUtils.loginUserWithStandardRoles();
        AddressBook addressBook = new AddressBook();
        addressBook.setId(3L);
        AddressBookInvitation invitation = new AddressBookInvitation();
        invitation.setAddressBook(addressBook);
        invitation.setInvitedUserEmail("notanuser@ideasagiles.com");
        invitation.setPermission("rw");

        addressBookService.inviteUser(invitation);

        assertNotNull(invitation.getId());
        assertNotNull(invitation.getCreationTime());
        assertEquals(1, smtpServer.getMessages().size());
        assertNotNull(invitation.getSentByUser());
        assertEquals(invitation.getSentByUser().getId(), SecurityTestUtils.USER_WITH_STANDARD_ROLES_ID);

        //Read-only should have been forced until Plans are implemented.
        //When Plans are implemented, remove this assert.
        assertEquals("r", invitation.getPermission());
    }

    @Test(expected = AuthenticationCredentialsNotFoundException.class)
    public void inviteUser_notLoggedIn_throwsSecurityException() {
        AddressBook addressBook = new AddressBook();
        addressBook.setId(3L);
        AddressBookInvitation invitation = new AddressBookInvitation();
        invitation.setAddressBook(addressBook);
        invitation.setInvitedUserEmail("notanuser@ideasagiles.com");
        invitation.setPermission("rw");

        addressBookService.inviteUser(invitation);
        fail("An exception should have been thrown.");
    }

    @Test(expected = AccessDeniedException.class)
    public void inviteUser_withNoPermissionOnAddressBook_throwsSecurityException() {
        SecurityTestUtils.loginUserWithStandardRoles();
        AddressBook addressBook = new AddressBook();
        addressBook.setId(1L);  //this user has no permission to invite on this addressbook
        AddressBookInvitation invitation = new AddressBookInvitation();
        invitation.setAddressBook(addressBook);
        invitation.setInvitedUserEmail("notanuser@ideasagiles.com");
        invitation.setPermission("rw");

        addressBookService.inviteUser(invitation);
        fail("An exception should have been thrown.");
    }

    @Test(expected = DuplicateException.class)
    public void inviteUser_alreadyInvited_throwsException() {
        SecurityTestUtils.loginUserWithStandardRoles();
        AddressBook addressBook = new AddressBook();
        addressBook.setId(3L);
        AddressBookInvitation invitation = new AddressBookInvitation();
        invitation.setAddressBook(addressBook);
        invitation.setInvitedUserEmail("notregistereduser@ideasagiles.com");
        invitation.setPermission("rw");

        addressBookService.inviteUser(invitation);
        fail("An exception should have been thrown.");
    }

    @Test(expected = DuplicateException.class)
    public void inviteUser_alreadyMember_throwsException() {
        SecurityTestUtils.loginUserWithStandardRoles();
        AddressBook addressBook = new AddressBook();
        addressBook.setId(3L);
        AddressBookInvitation invitation = new AddressBookInvitation();
        invitation.setAddressBook(addressBook);
        invitation.setInvitedUserEmail("leito@ideasagiles.com");
        invitation.setPermission("rw");

        addressBookService.inviteUser(invitation);
        fail("An exception should have been thrown.");
    }

    @Test
    public void getReceivedInvitationsForLoggedInUser_validEmail_returnsInvitations() {
        SecurityTestUtils.loginUserWithStandardRoles();
        String email = SecurityTestUtils.USER_WITH_STANDARD_ROLES_EMAIL;

        Collection<AddressBookInvitation> invitations = addressBookService.getReceivedInvitationsForLoggedInUser();

        assertNotNull(invitations);
        assertTrue(invitations.size() > 0);
        for (AddressBookInvitation invitation : invitations) {
            assertEquals(email, invitation.getInvitedUserEmail());
        }
    }

    @Test(expected=AuthenticationCredentialsNotFoundException.class)
    public void getReceivedInvitationsForLoggedInUser_notLoggedInUser_throwsSecurityException() {
        addressBookService.getReceivedInvitationsForLoggedInUser();
        fail("An exception should have been thrown.");
    }

    public void findInvitationByAddressBook_validAddressBook_returnsInvitations() {
        SecurityTestUtils.loginUserWithStandardRoles();
        Long addressBookId = SecurityTestUtils.USER_WITH_STANDARD_ROLES_ID;


        Collection<AddressBookInvitation> invitations = addressBookService.findInvitationByAddressBook(addressBookId);

        assertNotNull(invitations);
        assertTrue(invitations.size() > 0);
        for (AddressBookInvitation invitation : invitations) {
            assertEquals(addressBookId, invitation.getAddressBook().getId());
        }
    }

    @Test(expected=AuthenticationCredentialsNotFoundException.class)
    public void findInvitationByAddressBook_notLoggedInUser_throwsSecurityException() {
        addressBookService.findInvitationByAddressBook(3L);
        fail("An exception should have been thrown.");
    }

    @Test(expected=AccessDeniedException.class)
    public void findInvitationByAddressBook_noPermissionOnAddressBook_throwsSecurityException() {
        SecurityTestUtils.loginUserWithStandardRoles();
        addressBookService.findInvitationByAddressBook(5L);
        fail("An exception should have been thrown.");
    }


    @Test
    @DirtiesDatabase
    public void acceptInvitation_loggedInUser_addPermission() {
        SecurityTestUtils.loginUserWithStandardRoles();
        Long invitationId = 2L;

        int beforeCountInvitation = JdbcTestUtils.countRowsInTable(jdbcTemplate, "address_books_invitations");
        int beforeCountPermission = JdbcTestUtils.countRowsInTable(jdbcTemplate, "address_books_permissions");
        addressBookService.acceptInvitation(invitationId);
        int afterCountInvitation = JdbcTestUtils.countRowsInTable(jdbcTemplate, "address_books_invitations");
        int afterCountPermission = JdbcTestUtils.countRowsInTable(jdbcTemplate, "address_books_permissions");

        assertEquals(beforeCountInvitation - 1, afterCountInvitation);
        assertEquals(beforeCountPermission + 1, afterCountPermission);
    }

    @Test(expected = AuthenticationCredentialsNotFoundException.class)
    public void acceptInvitation_notLoggedInUser_throwsException() {
        Long invitationId = 2L;

        addressBookService.acceptInvitation(invitationId);

        fail("An exception should have been thrown");
    }

    @Test(expected = AccessDeniedException.class)
    public void acceptInvitation_loggedWithOtherUser_throwsException() {
        SecurityTestUtils.loginUserWithNoInvitations();
        Long invitationId = 2L;

        addressBookService.acceptInvitation(invitationId);

        fail("An exception should have been thrown");
    }

    @Test
    @DirtiesDatabase
    public void rejectInvitation_loggedInUser_rejectInvitation() {
        SecurityTestUtils.loginUserWithStandardRoles();
        Long invitationId = 2L;

        int beforeCountInvitation = JdbcTestUtils.countRowsInTable(jdbcTemplate, "address_books_invitations");
        int beforeCountPermission = JdbcTestUtils.countRowsInTable(jdbcTemplate, "address_books_permissions");
        addressBookService.rejectInvitation(invitationId);
        int afterCountInvitation = JdbcTestUtils.countRowsInTable(jdbcTemplate, "address_books_invitations");
        int afterCountPermissions = JdbcTestUtils.countRowsInTable(jdbcTemplate, "address_books_permissions");

        assertEquals(beforeCountInvitation - 1, afterCountInvitation);
        assertEquals(beforeCountPermission, afterCountPermissions);
    }

    @Test(expected = AuthenticationCredentialsNotFoundException.class)
    public void rejectInvitation_notLoggedInUser_throwsException() {
        Long invitationId = 2L;

        addressBookService.rejectInvitation(invitationId);

        fail("An exception should have been thrown");
    }

    @Test(expected = AccessDeniedException.class)
    public void rejectInvitation_loggedWithOtherUser_throwsException() {
        SecurityTestUtils.loginUserWithNoInvitations();
        Long invitationId = 2L;

        addressBookService.rejectInvitation(invitationId);

        fail("An exception should have been thrown");
    }
}
