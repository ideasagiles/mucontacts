/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.contactmanager.service;

import com.ideasagiles.contactmanager.AbstractDatabaseAndSmtpServerTest;
import com.ideasagiles.contactmanager.DirtiesDatabase;
import com.ideasagiles.contactmanager.SecurityTestUtils;
import com.ideasagiles.contactmanager.controller.exception.BadRequestException;
import com.ideasagiles.contactmanager.controller.exception.DuplicateException;
import com.ideasagiles.contactmanager.dao.ReferralDao;
import com.ideasagiles.contactmanager.dao.SubscriptionDao;
import com.ideasagiles.contactmanager.domain.*;
import com.ideasagiles.contactmanager.vo.UserSignupVo;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import static org.junit.Assert.*;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.test.jdbc.JdbcTestUtils;
import org.subethamail.wiser.WiserMessage;

/**
 * Tests for PrivateUserService class.
 *
 * @author gianu
 */
public class PrivateUserServiceTest extends AbstractDatabaseAndSmtpServerTest {

    @Autowired
    private PrivateUserService userService;
    @Autowired
    private UserInvitationService userInvitationService;
    @Autowired
    private ReferralDao referralDao;
    @Autowired
    private SubscriptionDao subscriptionDao;


    @Test
    @DirtiesDatabase
    public void createUserForLocalAuthentication_validUser_createsAccount() throws NoSuchAlgorithmException {
        String password = "raz";
        UserSignupVo user = new UserSignupVo();
        user.setUsername("userraz");
        user.setEmail("serviceRaz@ideasagiles.com");

        int beforeAddressBookPermissionCount = JdbcTestUtils.countRowsInTable(jdbcTemplate, "address_books_permissions");
        int beforeAddressBookCount = JdbcTestUtils.countRowsInTable(jdbcTemplate, "address_books");
        int beforeUserCount = JdbcTestUtils.countRowsInTable(jdbcTemplate, "users");
        int beforeSubscriptionCount = JdbcTestUtils.countRowsInTable(jdbcTemplate, "subscriptions");
        PrivateUser privateUser = userService.createUserForLocalAuthentication(user, password);
        int afterAddressBookPermissionCount = JdbcTestUtils.countRowsInTable(jdbcTemplate, "address_books_permissions");
        int afterUserCount = JdbcTestUtils.countRowsInTable(jdbcTemplate, "users");
        int afterAddressBookCount = JdbcTestUtils.countRowsInTable(jdbcTemplate, "address_books");
        int afterSubscriptionCount = JdbcTestUtils.countRowsInTable(jdbcTemplate, "subscriptions");

        assertNotNull("User Id should be a positive number", privateUser.getId());
        assertNotNull("MemberSince shouldn't be null", privateUser.getMemberSince());
        assertEquals("A PrivateUser should have been inserted into the database", beforeUserCount + 1, afterUserCount);
        assertNotNull("Version shouln't be null", privateUser.getVersion());
        assertNotSame("The password should have been encrypted", password, user.getPassword());

        assertNotNull("A Refer Code should have been created", privateUser.getReferralCode());
        assertTrue(privateUser.getReferralCode().length() > 3);
        assertEquals(AuthenticationProvider.LOCAL, privateUser.getAuthenticationProvider());

        assertEquals("An AddressBook should have been inserted into the database", beforeAddressBookCount + 1, afterAddressBookCount);
        assertEquals("An AddressBookPermission should have been inserted into the database", beforeAddressBookPermissionCount + 1, afterAddressBookPermissionCount);
        assertEquals("A Subscription should have been inserted into the database", beforeSubscriptionCount + 1, afterSubscriptionCount);
    }

    @Test
    @DirtiesDatabase
    public void createUserForLocalAuthentication_validUserWithNullEmail_createsAccount() throws NoSuchAlgorithmException {
        String password = "raz";
        UserSignupVo user = new UserSignupVo();
        user.setUsername("a-nice_USERname.2011");
        user.setEmail(null);

        int beforeAddressBookPermissionCount = JdbcTestUtils.countRowsInTable(jdbcTemplate, "address_books_permissions");
        int beforeAddressBookCount = JdbcTestUtils.countRowsInTable(jdbcTemplate, "address_books");
        int beforeUserCount = JdbcTestUtils.countRowsInTable(jdbcTemplate, "users");
        int beforeSubscriptionCount = JdbcTestUtils.countRowsInTable(jdbcTemplate, "subscriptions");
        PrivateUser privateUser = userService.createUserForLocalAuthentication(user, password);
        int afterAddressBookPermissionCount = JdbcTestUtils.countRowsInTable(jdbcTemplate, "address_books_permissions");
        int afterUserCount = JdbcTestUtils.countRowsInTable(jdbcTemplate, "users");
        int afterAddressBookCount = JdbcTestUtils.countRowsInTable(jdbcTemplate, "address_books");
        int afterSubscriptionCount = JdbcTestUtils.countRowsInTable(jdbcTemplate, "subscriptions");

        assertNotNull("User Id should be a positive number", privateUser.getId());
        assertNotNull("MemberSince shouldn't be null", privateUser.getMemberSince());
        assertEquals("A PrivateUser should have been inserted into the database", beforeUserCount + 1, afterUserCount);
        assertNotNull("Version shouln't be null", privateUser.getVersion());
        assertNotSame("The password should have been encrypted", password, user.getPassword());
        assertNotNull("A Refer Code should have been created", privateUser.getReferralCode());
        assertTrue(privateUser.getReferralCode().length() > 3);
        assertEquals(AuthenticationProvider.LOCAL, privateUser.getAuthenticationProvider());

        assertEquals("An AddressBook should have been inserted into the database", beforeAddressBookCount + 1, afterAddressBookCount);
        assertEquals("An AddressBookPermission should have been inserted into the database", beforeAddressBookPermissionCount + 1, afterAddressBookPermissionCount);
        assertEquals("A Subscription should have been inserted into the database", beforeSubscriptionCount + 1, afterSubscriptionCount);
    }

    @Test
    @DirtiesDatabase
    public void createUserForLocalAuthentication_validUserWithEmptyEmail_createsAccount() throws NoSuchAlgorithmException {
        String password = "raz";
        UserSignupVo user = new UserSignupVo();
        user.setUsername("a-nice_USERname.2011");
        user.setEmail("");

        PrivateUser privateUser = userService.createUserForLocalAuthentication(user, password);

        assertNotNull("User Id should be a positive number", privateUser.getId());
        assertNull("User email should be forced to null if empty", privateUser.getEmail());
        assertNotNull("MemberSince shouldn't be null", privateUser.getMemberSince());
        assertNotNull("Version shouln't be null", privateUser.getVersion());
        assertNotSame("The password should have been encrypted", password, privateUser.getPassword());
        assertNotNull("A Refer Code should have been created", privateUser.getReferralCode());
        assertTrue(privateUser.getReferralCode().length() > 3);
        assertEquals(AuthenticationProvider.LOCAL, privateUser.getAuthenticationProvider());
    }

    @Test(expected = DuplicateException.class)
    public void createUserForLocalAuthentication_alreadyExistingEmail_throwsException() {
        UserSignupVo user = new UserSignupVo();
        user.setUsername("userraz");
        user.setEmail("gianu@ideasagiles.com");

        userService.createUserForLocalAuthentication(user, "a password");

        fail("An Exception should have been thrown");
    }

    @Test(expected = IllegalArgumentException.class)
    public void createUserForLocalAuthentication_usernameLikeEmail_throwsException() {
        UserSignupVo user = new UserSignupVo();
        user.setUsername("leito@mail.com");

        userService.createUserForLocalAuthentication(user, "a password");

        fail("An Exception should have been thrown");
    }

    @Test(expected = IllegalArgumentException.class)
    public void createUserForLocalAuthentication_usernameWithSpaces_throwsException() {
        UserSignupVo user = new UserSignupVo();
        user.setUsername("leito ideas");

        userService.createUserForLocalAuthentication(user, "a password");

        fail("An Exception should have been thrown");
    }

    @Test(expected = IllegalArgumentException.class)
    public void createUserForLocalAuthentication_usernameTooShort_throwsException() {
        UserSignupVo user = new UserSignupVo();
        user.setUsername("  ab  ");

        userService.createUserForLocalAuthentication(user, "a password");

        fail("An Exception should have been thrown");
    }


    @Test(expected = DuplicateException.class)
    public void createUserForLocalAuthentication_alreadyExistingUsername_throwsException() {
        UserSignupVo user = new UserSignupVo();
        user.setUsername("leito");
        user.setEmail("anotheremailaddressforleito@ideasagiles.com");

        userService.createUserForLocalAuthentication(user, "a password");

        fail("An Exception should have been thrown");
    }

    @Test(expected = IllegalArgumentException.class)
    public void createUserForLocalAuthentication_nullPassword_throwsException() {
        UserSignupVo user = new UserSignupVo();
        user.setEmail("serviceRaz@ideasagiles.com");

        userService.createUserForLocalAuthentication(user, null);
        fail("An exception should have been thrown.");
    }

    @Test(expected = IllegalArgumentException.class)
    public void createUserForLocalAuthentication_emptyPassword_throwsException() {
        UserSignupVo user = new UserSignupVo();
        user.setEmail("serviceRaz@ideasagiles.com");

        userService.createUserForLocalAuthentication(user, "");
        fail("An exception should have been thrown.");
    }

    @Test
    @DirtiesDatabase
    public void createUserForLocalAuthentication_withRecommendationToken_createsAccountAndUpdateRecommendation() {
        String password = "raz";
        UserSignupVo user = new UserSignupVo();
        user.setUsername("userraz");
        user.setEmail("invasorzim@ideasagiles.com");
        user.setToken("23456789");

        int beforeAddressBookPermissionCount = JdbcTestUtils.countRowsInTable(jdbcTemplate, "address_books_permissions");
        int beforeAddressBookCount = JdbcTestUtils.countRowsInTable(jdbcTemplate, "address_books");
        int beforeUserCount = JdbcTestUtils.countRowsInTable(jdbcTemplate, "users");
        int beforeSubscriptionCount = JdbcTestUtils.countRowsInTable(jdbcTemplate, "subscriptions");
        PrivateUser privateUser = userService.createUserForLocalAuthentication(user, password);
        int afterAddressBookPermissionCount = JdbcTestUtils.countRowsInTable(jdbcTemplate, "address_books_permissions");
        int afterUserCount = JdbcTestUtils.countRowsInTable(jdbcTemplate, "users");
        int afterAddressBookCount = JdbcTestUtils.countRowsInTable(jdbcTemplate, "address_books");
        int afterSubscriptionCount = JdbcTestUtils.countRowsInTable(jdbcTemplate, "subscriptions");

        UserInvitation invitation = userInvitationService.findInvitationByToken("23456789");

        assertNotNull("User Id should be a positive number", privateUser.getId());
        assertNotNull("MemberSince shouldn't be null", privateUser.getMemberSince());
        assertEquals("A PrivateUser should have been inserted into the database", beforeUserCount + 1, afterUserCount);
        assertNotNull("Version shouln't be null", privateUser.getVersion());
        assertNotSame("The password should have been encrypted", password, user.getPassword());
        assertEquals(AuthenticationProvider.LOCAL, privateUser.getAuthenticationProvider());

        assertEquals("An AddressBook should have been inserted into the database", beforeAddressBookCount + 1, afterAddressBookCount);
        assertEquals("An AddressBookPermission should have been inserted into the database", beforeAddressBookPermissionCount + 1, afterAddressBookPermissionCount);
        assertEquals("A Subscription should have been inserted into the database", beforeSubscriptionCount + 1, afterSubscriptionCount);

        assertNotNull(invitation.getInvited());
        assertNotNull(invitation.getAcceptedTime());
    }

    @Test
    @DirtiesDatabase
    public void createUserForLocalAuthentication_withUnexistingRecommendationToken_createsAccount() {
        String password = "raz";
        UserSignupVo user = new UserSignupVo();
        user.setUsername("userraz");
        user.setEmail("invasorzim@ideasagiles.com");
        user.setToken("23456764");

        int beforeAddressBookPermissionCount = JdbcTestUtils.countRowsInTable(jdbcTemplate, "address_books_permissions");
        int beforeAddressBookCount = JdbcTestUtils.countRowsInTable(jdbcTemplate, "address_books");
        int beforeUserCount = JdbcTestUtils.countRowsInTable(jdbcTemplate, "users");
        int beforeSubscriptionCount = JdbcTestUtils.countRowsInTable(jdbcTemplate, "subscriptions");
        PrivateUser privateUser = userService.createUserForLocalAuthentication(user, password);
        int afterAddressBookPermissionCount = JdbcTestUtils.countRowsInTable(jdbcTemplate, "address_books_permissions");
        int afterUserCount = JdbcTestUtils.countRowsInTable(jdbcTemplate, "users");
        int afterAddressBookCount = JdbcTestUtils.countRowsInTable(jdbcTemplate, "address_books");
        int afterSubscriptionCount = JdbcTestUtils.countRowsInTable(jdbcTemplate, "subscriptions");

        UserInvitation invitation = userInvitationService.findInvitationByToken("23456764");

        assertNotNull("User Id should be a positive number", privateUser.getId());
        assertNotNull("MemberSince shouldn't be null", privateUser.getMemberSince());
        assertEquals("A PrivateUser should have been inserted into the database", beforeUserCount + 1, afterUserCount);
        assertNotNull("Version shouln't be null", privateUser.getVersion());
        assertNotSame("The password should have been encrypted", password, user.getPassword());
        assertEquals(AuthenticationProvider.LOCAL, privateUser.getAuthenticationProvider());

        assertEquals("An AddressBook should have been inserted into the database", beforeAddressBookCount + 1, afterAddressBookCount);
        assertEquals("An AddressBookPermission should have been inserted into the database", beforeAddressBookPermissionCount + 1, afterAddressBookPermissionCount);
        assertEquals("A Subscription should have been inserted into the database", beforeSubscriptionCount + 1, afterSubscriptionCount);

        assertNull(invitation);
        assertNull(invitation);
    }

    @Test
    @DirtiesDatabase
    public void createUserForExternalAuthentication_withNullId_assignsNewIdAndVersionAndAddressBook() {
        PrivateUser user = new PrivateUser();
        user.setUsername("serviceRaz");
        user.setEmail("serviceRaz@ideasagiles.com");
        user.setAuthenticationProvider(AuthenticationProvider.TWITTER);

        int beforeAddressBookPermissionCount = JdbcTestUtils.countRowsInTable(jdbcTemplate, "address_books_permissions");
        int beforeAddressBookCount = JdbcTestUtils.countRowsInTable(jdbcTemplate, "address_books");
        int beforeUserCount = JdbcTestUtils.countRowsInTable(jdbcTemplate, "users");
        int beforeSubscriptionCount = JdbcTestUtils.countRowsInTable(jdbcTemplate, "subscriptions");
        userService.createUserForExternalAuthentication(user);
        int afterAddressBookPermissionCount = JdbcTestUtils.countRowsInTable(jdbcTemplate, "address_books_permissions");
        int afterUserCount = JdbcTestUtils.countRowsInTable(jdbcTemplate, "users");
        int afterAddressBookCount = JdbcTestUtils.countRowsInTable(jdbcTemplate, "address_books");
        int afterSubscriptionCount = JdbcTestUtils.countRowsInTable(jdbcTemplate, "subscriptions");

        assertNotNull("User Id should be a positive number", user.getId());
        assertNotNull("MemberSince shouldn't be null", user.getMemberSince());
        assertEquals("A PrivateUser should have been inserted into the database", beforeUserCount + 1, afterUserCount);
        assertNotNull("Version shouln't be null", user.getVersion());
        assertTrue("The user should be enabled", user.getEnabled());
        assertNull("Pasword should be forced to null", user.getPassword());

        assertEquals("An AddressBook should have been inserted into the database", beforeAddressBookCount + 1, afterAddressBookCount);
        assertEquals("An AddressBookPermission should have been inserted into the database", beforeAddressBookPermissionCount + 1, afterAddressBookPermissionCount);
        assertEquals("A Subscription should have been inserted into the database", beforeSubscriptionCount + 1, afterSubscriptionCount);
    }

    @Test(expected = DuplicateException.class)
    public void createUserForExternalAuthentication_alreadyExistingUser_throwsException() {
        PrivateUser user = new PrivateUser();
        user.setUsername("serviceRaz");
        user.setEmail("gianu@ideasagiles.com");

        userService.createUserForExternalAuthentication(user);

        fail("An Exception should have been thrown");
    }

    @Test
    @DirtiesDatabase
    public void createUserForExternalAuthentication_notEmptyPassword_forcesPasswordToNull() {
        PrivateUser user = new PrivateUser();
        user.setUsername("serviceRaz");
        user.setEmail("serviceRaz@ideasagiles.com");
        user.setPassword("some password");
        user.setAuthenticationProvider(AuthenticationProvider.TWITTER);

        userService.createUserForExternalAuthentication(user);

        assertNull("Password should be forced to null", user.getPassword());
    }


    @Test
    @DirtiesDatabase
    public void update_loggedInUserWithNullPassword_updateUser() {
        SecurityTestUtils.loginUserWithStandardRoles();
        Long oldVersion = userService.getLoggedInUser().getVersion();

        PrivateUser user = new PrivateUser();
        user.setId(SecurityTestUtils.USER_WITH_STANDARD_ROLES_ID);

        //change the email and the name
        user.setEmail("updateduser@ideasagiles.com");
        user.setName("Updated user");

        userService.update(user, null);

        //Re-login the user
        SecurityTestUtils.login("updateduser@ideasagiles.com", SecurityTestUtils.DEFAULT_TEST_USERS_PASSWORD);

        user = userService.getLoggedInUser();

        assertTrue(user.getVersion() > oldVersion);
        assertEquals("Updated user", user.getName());
        assertEquals("updateduser@ideasagiles.com", user.getEmail());
    }

    @Test
    @DirtiesDatabase
    public void update_loggedInUserEmptyPassword_updateUser() {
        SecurityTestUtils.loginUserWithStandardRoles();
        Long oldVersion = userService.getLoggedInUser().getVersion();

        PrivateUser user = new PrivateUser();
        user.setId(SecurityTestUtils.USER_WITH_STANDARD_ROLES_ID);

        user.setEmail("updateduser@ideasagiles.com");
        user.setName("Updated user");

        userService.update(user, "");

        SecurityTestUtils.login("updateduser@ideasagiles.com", SecurityTestUtils.DEFAULT_TEST_USERS_PASSWORD);

        user = userService.getLoggedInUser();

        assertTrue(user.getVersion() > oldVersion);
        assertEquals("Updated user", user.getName());
        assertEquals("updateduser@ideasagiles.com", user.getEmail());
    }

    @Test
    @DirtiesDatabase
    public void update_loggedInUser_updateUser() {
        SecurityTestUtils.loginUserWithStandardRoles();
        Long oldVersion = userService.getLoggedInUser().getVersion();
        String oldPassword = userService.getLoggedInUser().getPassword();

        PrivateUser user = new PrivateUser();
        user.setId(SecurityTestUtils.USER_WITH_STANDARD_ROLES_ID);

        //change the name and the password
        user.setName("Updated User");

        userService.update(user, "modifiedPassword");

        SecurityTestUtils.login(SecurityTestUtils.USER_WITH_STANDARD_ROLES_EMAIL, "modifiedPassword");

        user = userService.getLoggedInUser();

        assertTrue(user.getVersion() > oldVersion);
        assertEquals("Updated User", user.getName());
        assertFalse(oldPassword.equals(user.getPassword()));
    }

    @Test(expected = Exception.class)
    public void update_nullId_throwsException() {
        PrivateUser user = new PrivateUser();
        user.setId(null);
        userService.update(user, null);
        fail("An exception should have been thrown.");
    }

    @Test(expected = AccessDeniedException.class)
    public void update_anotherUser_throwsException() {
        SecurityTestUtils.loginUserWithStandardRoles();
        PrivateUser user = new PrivateUser();
        user.setId(1L);
        user.setEmail("gianu@ideasagiles.com");
        user.setName("Raz");
        user.setVersion(0L);

        userService.update(user, null);

        fail("An exception should have been thrown.");
    }

    @Test(expected = AuthenticationCredentialsNotFoundException.class)
    public void update_notLoggedInUser_throwsException() {
        PrivateUser user = new PrivateUser();
        user.setId(1L);
        user.setName("Raz");
        userService.update(user, null);
        fail("An exception should have been thrown.");
    }

    @Test
    public void update_triesToChangeUsername_usernameDoesNotChange() {
        SecurityTestUtils.loginUserWithStandardRoles();
        PrivateUser user = new PrivateUser();
        user.setId(SecurityTestUtils.USER_WITH_STANDARD_ROLES_ID);
        user.setUsername("otherusername");
        user.setName("Raz");
        userService.update(user, null);

        assertEquals(SecurityTestUtils.USER_WITH_STANDARD_ROLES_USERNAME, user.getUsername());
    }

    @Test
    public void findByEmail_withExistingMail_returnsUser() {
        String userEmail = "gianu@ideasagiles.com";
        User user = userService.findByEmail(userEmail);

        assertNotNull(user);
        assertEquals(userEmail, user.getEmail());
    }

    @Test
    public void findByEmail_withNoExistingEmail_returnsNull() {
        String userEmail = "nonExistingUser@ideasagiles.com";
        User user = userService.findByEmail(userEmail);

        assertNull(user);
    }

    @Test
    public void getLoggedInUser_userIsLoggedIn_returnsUser() {
        SecurityTestUtils.loginUserWithStandardRoles();

        PrivateUser user = userService.getLoggedInUser();
        assertNotNull(user);
        assertEquals(SecurityTestUtils.USER_WITH_STANDARD_ROLES_USERNAME, user.getUsername());
    }

    @Test(expected = BadCredentialsException.class)
    public void getLoggedInUser_userIsNotLoggedIn_throwsException() {
        SecurityTestUtils.loginInvalidUser();
        userService.getLoggedInUser();
        fail("An exception should have been thrown.");
    }

    @Test
    public void login_withValidUser_logsInTheUser() {
        String email = "leito@ideasagiles.com";
        String password = "leito";
        userService.login(email, password);
        PrivateUser user = userService.getLoggedInUser();

        assertNotNull(user);
        assertEquals(email, user.getEmail());
    }

    @Test(expected = BadCredentialsException.class)
    public void login_withWrongPassword_throwsException() {
        String email = "leito@ideasagiles.com";
        String password = "wrong password";
        userService.login(email, password);
        fail("An exception should have been thrown.");
    }

    @Test(expected = BadCredentialsException.class)
    public void login_withNotExistingUser_throwsException() {
        String email = "foobarchu@ideasagiles.com";
        String password = "something";
        userService.login(email, password);
        fail("An exception should have been thrown.");
    }

    @Test
    @DirtiesDatabase
    public void createToken_existingUser_returnTokenAndSendEmail() {
        PasswordRecoveryToken userToken;

        userToken = userService.createToken("gianu@ideasagiles.com");

        assertEquals(1, smtpServer.getMessages().size());
        assertNotNull(userToken);
        assertNotNull(userToken.getToken());
    }

    @Test
    @DirtiesDatabase
    public void createToken_existingUserWithToken_returnSameTokenAndSendEmail() {
        String existingToken = "abcdefghijklmnopq";
        String userEmail = SecurityTestUtils.USER_WITH_STANDARD_ROLES_EMAIL;

        PasswordRecoveryToken userToken = userService.createToken(userEmail);

        assertEquals(1, smtpServer.getMessages().size());
        assertNotNull(userToken);
        assertNotNull(userToken.getToken());
        assertEquals(existingToken, userToken.getToken());
    }

    @Test
    public void createToken_UnexistingUser_returnNullAndDoesNotSendEmail() {
        String userEmail = "unexisting.user@ideasagiles.com";

        PasswordRecoveryToken userToken = userService.createToken(userEmail);

        assertEquals(0, smtpServer.getMessages().size());
        assertNull(userToken);
    }

    @Test
    @DirtiesDatabase
    public void resetPassword_WithExistingTokenAndUser_changesPassword() {
        String userEmail = "test_user@ideasagiles.com";
        String userToken = "abcdefghijklmnopq";
        String newPassword = "nuevoPassword";

        int rowsBeforeReset = JdbcTestUtils.countRowsInTable(jdbcTemplate, "password_recovery_tokens");
        userService.resetPassword(userEmail, userToken, newPassword);
        int rowsAfterReset = JdbcTestUtils.countRowsInTable(jdbcTemplate, "password_recovery_tokens");

        assertEquals(rowsBeforeReset - 1, rowsAfterReset);
    }

    @Test(expected = BadRequestException.class)
    public void resetPassword_WithExistingTokenAndUnexistingUser_throwsException() {
        String userEmail = "nonexisting@ideasagiles.com";
        String userToken = "abcdefghijklmnopq";

        userService.resetPassword(userEmail, userToken, "nuevoPassword");

        fail("An exception should have been thrown");
    }

    @Test(expected = BadRequestException.class)
    public void resetPassword_withExistingTokenAndDifferentUser_throwsException() {
        String userEmail = "test_user_with_no_invitations@ideasagiles.com";
        String userToken = "abcdefghijklmnopq";

        userService.resetPassword(userEmail, userToken, "nuevoPassword");

        fail("An exception should have been thrown");
    }

    @Test(expected = BadRequestException.class)
    public void resetPassword_WithUnExistingTokenAndExistingUser_throwsException() {
        String userEmail = "gianu@ideasagiles.com";
        String userToken = "noToken";

        userService.resetPassword(userEmail, userToken, "nuevoPassword");

        fail("An exception should have been thrown");
    }

    @Test(expected = BadRequestException.class)
    public void resetPassword_WithUnExistingTokenAndUnExistingUser_throwsException() {
        String userEmail = "nonexisting@ideasagiles.com";
        String userToken = "noToken";

        userService.resetPassword(userEmail, userToken, "nuevoPassword");

        fail("An exception should have been thrown");
    }

    @Test
    public void findUserByToken_existingToken_returnUser() {
        String token = "abcdefghijklmnopq";
        String userEmail = SecurityTestUtils.USER_WITH_STANDARD_ROLES_EMAIL;

        User user = userService.findByToken(token);

        assertNotNull("The user returned by the service is null", user);
        assertNotNull(user.getEmail());
        assertEquals(userEmail, user.getEmail());
    }

    @Test
    public void findUserByToken_unexistingToken_returnNull() {
        String token = "unexistingToken";

        User user = userService.findByToken(token);

        assertNull(user);
    }

    @Test(expected = AuthenticationCredentialsNotFoundException.class)
    public void cancelAccount_notLoggedInUser_throwsException() {
        userService.cancelAccount();
        fail("An exception should have been thrown.");
    }

    @Test
    public void cancelAccount_LoggedInUser_sendEmail() throws MessagingException, IOException {
        SecurityTestUtils.loginUserWithStandardRoles();
        userService.cancelAccount();
        assertEquals(1, smtpServer.getMessages().size());
        MimeMessage mail = ((WiserMessage) smtpServer.getMessages().get(0)).getMimeMessage();
        assertNotNull(mail.getSubject());
        assertTrue(mail.getContent().toString().indexOf(SecurityTestUtils.USER_WITH_STANDARD_ROLES_ID.toString()) != -1);
        assertTrue(mail.getContent().toString().indexOf(SecurityTestUtils.USER_WITH_STANDARD_ROLES_EMAIL) != -1);
        assertTrue(mail.getContent().toString().indexOf(SecurityTestUtils.USER_WITH_STANDARD_ROLES_USERNAME) != -1);
    }

    @Test
    public void findByReferralCode_withExistingCodeAndLoggedInUser_returnsUser() {
        SecurityTestUtils.loginUserWithStandardRoles();
        Long userId = 1L;
        String username = "gianu";
        User user = userService.findByReferralCode("refcode_1"); //gianu

        assertNotNull(user);
        assertEquals(userId, user.getId());
        assertEquals(username, user.getUsername());
    }

    @Test(expected=AuthenticationCredentialsNotFoundException.class)
    public void findByReferralCode_withExistingCodeAndNotLoggedInUser_throwsSecurityException() {
        userService.findByReferralCode("refcode_1");
        fail("An exception should have been thrown.");
    }

}
