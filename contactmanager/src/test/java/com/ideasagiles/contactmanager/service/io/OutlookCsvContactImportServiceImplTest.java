/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.contactmanager.service.io;

import com.ideasagiles.contactmanager.AbstractDatabaseTest;
import com.ideasagiles.contactmanager.DirtiesDatabase;
import com.ideasagiles.contactmanager.SecurityTestUtils;
import com.ideasagiles.contactmanager.domain.AddressBookPermission;
import com.ideasagiles.contactmanager.domain.Contact;
import com.ideasagiles.contactmanager.domain.PrivateUser;
import com.ideasagiles.contactmanager.service.AddressBookPermissionService;
import com.ideasagiles.contactmanager.service.PrivateUserService;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;
import org.apache.commons.io.IOUtils;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.jdbc.JdbcTestUtils;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author gianu
 */
@Transactional
public class OutlookCsvContactImportServiceImplTest extends AbstractDatabaseTest  {

    private static final String VALID_CSV_FILENAME = "/com/ideasagiles/contactmanager/service/io/contacts-from-google-format-outlook.csv";

    @Autowired
    private OutlookCsvContactImportServiceImpl googleCsvService;
    @Autowired
    private PrivateUserService privateUserService;
    @Autowired
    private AddressBookPermissionService addressBookPermissionService;

    private String validCsv;

    @Before
    public void setup() throws IOException {
        InputStream resource = getClass().getResourceAsStream(VALID_CSV_FILENAME);
        validCsv = IOUtils.toString(resource);
    }

    @Test
    public void contactsFromExternalData_validContacts_returnsContacts() throws IOException {
        Collection<Contact> contacts = googleCsvService.contactsFromExternalData(validCsv);

        assertNotNull(contacts);
        assertEquals("There should be 4 contacts.", 4, contacts.size());

        for (Contact contact : contacts) {
            assertNotNull(contact.getName());
        }

        Contact[] contactsArray = contacts.toArray(new Contact[4]);

        assertEquals("Invasor Zim", contactsArray[0].getName());
        assertEquals("invasor@zim.com.ar", contactsArray[0].getEmail());

        assertEquals("Sergio Gianazza", contactsArray[1].getName());
        assertEquals("contacto@ideasagiles.com", contactsArray[1].getEmail());

        assertEquals("Leonardo Sebastian De Seta", contactsArray[2].getName());
        assertEquals("contacto@ideasagiles.com", contactsArray[2].getEmail());

        assertEquals("Unknown", contactsArray[3].getName());
        assertEquals("contact_with_no_name@ideasagiles.com", contactsArray[3].getEmail());

    }

    @Test
    @DirtiesDatabase
    public void importContacts_validContacts_saveContacts() throws IOException {
        SecurityTestUtils.loginUserWithStandardRoles();
        PrivateUser user = privateUserService.getLoggedInUser();
        Collection<AddressBookPermission> permissions = addressBookPermissionService.getAddressBookPermissionsForLoggedInUser();
        AddressBookPermission permission = permissions.iterator().next();

        ContactImporterOptions options = new ContactImporterOptions();
        options.setOwner(user.toUser());
        options.setAddressBook(permission.getAddressBook());
        options.setCreationTime(new java.util.Date());

        int beforeCount = JdbcTestUtils.countRowsInTable(jdbcTemplate, "contacts");

        googleCsvService.importContacts(validCsv, options);

        int afterCount = JdbcTestUtils.countRowsInTable(jdbcTemplate, "contacts");

        assertEquals(beforeCount + 4, afterCount);
    }
}
