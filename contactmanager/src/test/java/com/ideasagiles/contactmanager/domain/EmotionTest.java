/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.contactmanager.domain;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

/**
 * Test class for Emotion domain object.
 * @author Leito
 */
public class EmotionTest {

    @Test
    public void parse_mixedCaseName_returnsEmotion() {
        Emotion emotion = Emotion.parse("haPPy");
        assertEquals(emotion, Emotion.HAPPY);
    }

    @Test(expected=IllegalArgumentException.class)
    public void parse_notExisting_throwsException() {
        Emotion.parse("not existing emotion");
    }

    @Test(expected=NullPointerException.class)
    public void parse_null_throwsException() {
        Emotion.parse(null);
    }
}
