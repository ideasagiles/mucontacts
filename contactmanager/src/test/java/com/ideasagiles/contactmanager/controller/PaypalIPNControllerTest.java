/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.contactmanager.controller;

import com.ideasagiles.contactmanager.AbstractDatabaseTest;
import com.ideasagiles.contactmanager.DirtiesDatabase;
import com.ideasagiles.contactmanager.PaypalValidationMockServer;
import com.ideasagiles.contactmanager.service.PaypalTransactionServiceImpl;
import com.ideasagiles.contactmanager.service.exception.PaymentTransactionException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.jdbc.JdbcTestUtils;

/**
 * Test cases for PaypalIPNController.
 */
public class PaypalIPNControllerTest extends AbstractDatabaseTest {

    @Autowired
    private PaypalIPNController paypalIPNController;

    @Autowired
    private PaypalTransactionServiceImpl paypalTransactionService;

    /** Mock paypal server */
    private static PaypalValidationMockServer paypalServer;


    @BeforeClass
    public static void startPaypalServer() throws IOException {
        paypalServer = new PaypalValidationMockServer();
        paypalServer.start(9876);
    }

    @AfterClass
    public static void stopPaypalServer() throws IOException {
        paypalServer.stop();
    }

    @Before
    public void preparePaypalServer() throws MalformedURLException, IOException {
        URL validationUrlMock = new URL("http://localhost:9876");
        paypalTransactionService.setValidationUrl(validationUrlMock);
        paypalServer.setResponseToVerified();
    }

    private MockHttpServletRequest getValidPaypalRequest() {
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.setParameter("item_name", "");
        request.setParameter("item_number", "PAYPAL TEST PLAN");
        request.setParameter("payment_status", "Completed");
        request.setParameter("mc_gross", "20.00");
        request.setParameter("mc_currency", "USD");
        request.setParameter("txn_id", "45544");
        request.setParameter("receiver_email", "paypal.receiver.email@gmail.com");
        request.setParameter("payer_email", "payer.email@gmail.com");
        request.setParameter("custom", "username=test_user_with_restricted_plan");
        request.setParameter("txn_type", "subscr_payment");

        request.setCharacterEncoding("UTF-8");

        return request;
    }

    @Test(expected=PaymentTransactionException.class)
    public void paypalUpdate_invalidTransactionType_throwsException() throws PaymentTransactionException {
        MockHttpServletRequest request = getValidPaypalRequest();
        request.setParameter("txn_type", "subscr_signup");
        paypalIPNController.paypalUpdate(request, new MockHttpServletResponse());
        fail("An exception should have been thrown.");
    }

    @Test(expected=PaymentTransactionException.class)
    public void paypalUpdate_transactionTypeIsNotSet_throwsException() throws PaymentTransactionException {
        MockHttpServletRequest request = getValidPaypalRequest();
        request.removeParameter("txn_type");
        paypalIPNController.paypalUpdate(request, new MockHttpServletResponse());
        fail("An exception should have been thrown.");
    }


    @Test(expected=PaymentTransactionException.class)
    public void paypalUpdate_invalidPaymentTransactionStatus_throwsException() throws PaymentTransactionException {
        paypalServer.setResponseToInvalid();

        paypalIPNController.paypalUpdate(getValidPaypalRequest(), new MockHttpServletResponse());

        fail("An exception should have been thrown");
    }

    @Test(expected=PaymentTransactionException.class)
    public void paypalUpdate_unknownPaypalTransactionStatus_throwsException() throws PaymentTransactionException {
        paypalServer.setResponseToUnknownValue();

        paypalIPNController.paypalUpdate(getValidPaypalRequest(), new MockHttpServletResponse());

        fail("An exception should have been thrown.");
    }

    @Test(expected=PaymentTransactionException.class)
    public void paypalUpdate_paymentStatusNotCompleted_throwsException() throws PaymentTransactionException {
        MockHttpServletRequest req = getValidPaypalRequest();
        req.setParameter("payment_status", "not completed");

        paypalIPNController.paypalUpdate(req, new MockHttpServletResponse());

        fail("An exception should have been thrown.");
    }

    @Test(expected=PaymentTransactionException.class)
    public void paypalUpdate_paypalTransactionIdAlreadyExists_throwsException() throws PaymentTransactionException {
        MockHttpServletRequest req = getValidPaypalRequest();
        req.setParameter("txn_id", "1");

        paypalIPNController.paypalUpdate(req, new MockHttpServletResponse());

        fail("An exception should have been thrown.");
    }

    @Test(expected=PaymentTransactionException.class)
    public void paypalUpdate_receiverEmailDoesNotMatch_throwsException() throws PaymentTransactionException {
        MockHttpServletRequest req = getValidPaypalRequest();
        req.setParameter("receiver_email", "shouldnotmatch@ideasagiles.com");

        paypalIPNController.paypalUpdate(req, new MockHttpServletResponse());

        fail("An exception should have been thrown.");
    }

    @Test(expected=PaymentTransactionException.class)
    public void paypalUpdate_itemNumberNotFoundInPlan_throwsException() throws PaymentTransactionException {
        MockHttpServletRequest req = getValidPaypalRequest();
        req.setParameter("item_number", "SHOULD NOT EXIST");

        paypalIPNController.paypalUpdate(req, new MockHttpServletResponse());

        fail("An exception should have been thrown.");
    }

    @Test(expected=PaymentTransactionException.class)
    public void paypalUpdate_transactionIdNull_throwsException() throws PaymentTransactionException {
        MockHttpServletRequest req = getValidPaypalRequest();
        req.removeParameter("txn_id");

        paypalIPNController.paypalUpdate(req, new MockHttpServletResponse());

        fail("An exception should have been thrown.");
    }

    @Test(expected=PaymentTransactionException.class)
    public void paypalUpdate_usernameDoesNotExist_throwsException() throws PaymentTransactionException {
        MockHttpServletRequest req = getValidPaypalRequest();
        req.setParameter("custom", "username=does not exist;");

        paypalIPNController.paypalUpdate(req, new MockHttpServletResponse());

        fail("An exception should have been thrown.");
    }

    @Test(expected=PaymentTransactionException.class)
    public void paypalUpdate_invalidCustomValueFormat_throwsException() throws PaymentTransactionException {
        MockHttpServletRequest req = getValidPaypalRequest();
        req.setParameter("custom", "invalidFormat");

        paypalIPNController.paypalUpdate(req, new MockHttpServletResponse());

        fail("An exception should have been thrown.");
    }

    @Test
    @DirtiesDatabase
    public void processPayment_validPaymentUserWithActivePaidSubscription_savesPaypalTransactionAndSubscription() throws PaymentTransactionException {
        int beforeCountSubscriptions = JdbcTestUtils.countRowsInTable(jdbcTemplate, "subscriptions");
        int beforeCountPaypalTransactions = JdbcTestUtils.countRowsInTable(jdbcTemplate, "paypal_subscription_payments");

        paypalIPNController.paypalUpdate(getValidPaypalRequest(), new MockHttpServletResponse());

        int afterCountSubscriptions = JdbcTestUtils.countRowsInTable(jdbcTemplate, "subscriptions");
        int afterCountPaypalTransactions = JdbcTestUtils.countRowsInTable(jdbcTemplate, "paypal_subscription_payments");

        assertEquals(beforeCountSubscriptions + 1, afterCountSubscriptions);
        assertEquals(beforeCountPaypalTransactions + 1, afterCountPaypalTransactions);
    }

    @Test
    @DirtiesDatabase
    public void processPayment_validPaymentUserWithFreePlan_savesPaypalTransactionAndSubscription() throws PaymentTransactionException {
        MockHttpServletRequest request = getValidPaypalRequest();
        request.setParameter("custom", "username=leito");

        int beforeCountSubscriptions = JdbcTestUtils.countRowsInTable(jdbcTemplate, "subscriptions");
        int beforeCountPaypalTransactions = JdbcTestUtils.countRowsInTable(jdbcTemplate, "paypal_subscription_payments");

        paypalIPNController.paypalUpdate(request, new MockHttpServletResponse());

        int afterCountSubscriptions = JdbcTestUtils.countRowsInTable(jdbcTemplate, "subscriptions");
        int afterCountPaypalTransactions = JdbcTestUtils.countRowsInTable(jdbcTemplate, "paypal_subscription_payments");

        assertEquals(beforeCountSubscriptions + 1, afterCountSubscriptions);
        assertEquals(beforeCountPaypalTransactions + 1, afterCountPaypalTransactions);
    }

}
