/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.contactmanager.controller;

import com.ideasagiles.contactmanager.AbstractDatabaseTest;
import com.ideasagiles.contactmanager.SecurityTestUtils;
import java.io.IOException;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;

/**
 * This test class tests the ContactController object "in memory", with no
 * deployment. It focus on security access restrictions.
 *
 */
public class ContactExportControllerTest extends AbstractDatabaseTest {

    @Autowired
    private ContactExportController contactExportController;
    private MockHttpServletResponse response;

    @Before
    public void createHttpServletResponse() {
        response = new MockHttpServletResponse();
    }


    @Test(expected = AuthenticationCredentialsNotFoundException.class)
    public void exportCSV_notLoggedInUser_throwException() throws IOException {
        contactExportController.exportContactsToCSV(response);
        fail ("An exception should have been thrown.");
    }

    @Test
    public void exportCSV_loggedInUser_writesCsv() throws IOException {
        SecurityTestUtils.loginUserWithStandardRoles();

        contactExportController.exportContactsToCSV(response);

        assertEquals("text/csv", response.getContentType());
        assertTrue(response.getHeader("Content-disposition").toString().indexOf("filename") != -1);
        String csv = response.getContentAsString();
        assertNotNull(csv);
        assertTrue(csv.length() > 0);
    }
}

